#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

class DrawDiamond
{
public:
    DrawDiamond(string content, int size)
    {
        int i, j;
        setDiamondContent(content);

        for (i = 1; i <= size; i++)
        {
            for (j = 1; j <= size; j++)
            {
                if ((i == 1) || (i == size))
                    cout << content;
                else if (j == (size - i) / 2 + 2)
                    cout << " ";
                else
                    cout << content;
            }

            cout << endl;
        }

    }

    void setDiamondContent(string content)
    {
        DiamondContent = content;
    }

    void drawTop()
    {

    }

    void drawDown()
    {

    }
private:
    string DiamondContent;
};

int main()
{
    int size = 999;
    string content;

    cout << "Please input the content of diamond: ";
    cin >> content;

    while(1)
    {
        cout << "Please input a postive odd number for the size of diamond: ";
        cin >> size;

        if (!(size % 2))
        {
            cout << "Please input a postive odd number!" << endl;
            continue;
        }
        else
        {
            DrawDiamond diamond(content, size);
            break;
        }
    }

    return 0;
}
