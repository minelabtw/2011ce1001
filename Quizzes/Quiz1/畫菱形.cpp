#include <iostream>
#include <cstdlib>

using namespace std;

int main()
{
    for(int i=1; i<=6;i++){
	   for(int j=0; j<=6-i; j++)
		  cout << " ";

		
	   for(int k=0; k<i*2; k++)
	       cout << "*";
	       
	    cout << endl;
    }
    
    for(int i=5; i>=1; i--){
        for(int j=1; j>=i-5; j--)
            cout << " ";
            
        for(int k=0; k<i*2; k++)
            cout << "*";
            
        cout << endl;
    }
		
	system("pause");
	return 0;
}
