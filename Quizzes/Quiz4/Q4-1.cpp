#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#define MAX 100 // define the max array size 10

using namespace std;

int main()
{
    int number[MAX]; // declare arrays to store data
    string name[MAX];
    int balance[MAX];

    ifstream inputFile("input-1.txt", ios::in); // open the input file
    int dataNumber; // declare a variable dataNumber to record the number of data set
    inputFile >> dataNumber;

    for(int i=0;i<dataNumber;i++) // use a for loop to read data from input file
    {
        inputFile >> number[i] >> name[i] >> balance[i]; // data set is number, name, balance
    }

    ifstream inputFile2("input-2.txt", ios::in); // open the second input file
    int newNumber; // declare variables to store new data
    string newName;
    int newBalance;
    while(inputFile2 >> newNumber >> newName >> newBalance) // while the second input file is not EOF
    {
        name[newNumber-1] = newName; // modify the original data with new data
        balance[newNumber-1] = newBalance;
    }

    ofstream outputFile("Q4-100522092-1.txt", ios::out); // open a new file to output result
    for(int i=0;i<dataNumber;i++) // use a for loop to output data
    {
        outputFile << "numebr = " << number[i] << "\t" << "name = " << name[i] << "\t" << "balance = " << balance[i] << endl;
    }

    system("pause");
    return 0;
}
