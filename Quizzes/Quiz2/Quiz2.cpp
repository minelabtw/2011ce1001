#include <iostream>
#include <cstdlib>

using namespace std;

class TEST
{
public:
    double Pi;//declare Pi
    double Exp;//declare exp
    
    void Display(int choice)//display function
    {
        switch(choice)
        {
            case 1:// if case 1, display Pi
                cout << Pi << endl;
                break;
            case 2:// if case 2, display Exp
                cout << Exp << endl;
                break;
            default:
                break;
        }
    }
};

double Pi(int term)// calculate Pi
{
    return 3.14;
}

double Exp(int term)// calculate Exp
{
    return 2.17;
}

int main()
{
    int choice;
    cout << "Please input what you want to present? 1.Pi 2.Exp" << endl;// let user make a choice
    cin >> choice;
    
    TEST test;
    
    int term = 0;
    switch(choice)
    {
        case 1:// cchoose Pi
            cout << "how many terms?" << endl;
            cin >> term;// determine term
            test.Pi = Pi(term);
            test.Display(choice);
            break;
        case 2:// choose Exp
            cout << "how many terms?" << endl;
            cin >> term;//determine term
            test.Exp = Exp(term);
            test.Display(choice);
            break;
        default:
            break;
    }
    
    system("pause");
    return 0;
}
