#include <iostream>
#include <cstdlib>

using namespace std;

// class is a important concept in C++
// please make sure you know the difference between public and private data member
// and how to get data member from private and how to call the member class in class
class TEST// declaure a class call TEST
{
public:
    
    void setPi(double pi)// setPi is going to set the private data member Pi
    {
        Pi = pi;
    }
    
    void setExp(double exp)// setExp is going to set the private data member Exp
    {
        Exp = exp;
    }
    
    void Display(int choice)//display function
    {
        switch(choice)
        {
            case 1:// if case 1, display Pi
                cout << Pi << endl;
                break;
            case 2:// if case 2, display Exp
                cout << Exp << endl;
                break;
            default:
                break;
        }
    }

private:
    double Pi;//declare Pi
    double Exp;//declare exp
};

// [return type][function name]([argument if it is necessary])
// it is the structure of fuunction
// remember how to declare a function and how to call it
double Pi(int term)// calculate Pi
{
    return 3.14;
}

double Exp(int term)// calculate Exp
{
    return 2.17;
}

int main()
{
    int choice;
    cout << "Please input what you want to present? 1.Pi 2.Exp" << endl;// let user make a choice
    cin >> choice;
    
    TEST test;
    
    int term = 0;
    switch(choice)
    {
        case 1:// cchoose Pi
            cout << "how many terms?" << endl;
            cin >> term;// determine term
            test.setPi(Pi(term));
            test.Display(choice);
            break;
        case 2:// choose Exp
            cout << "how many terms?" << endl;
            cin >> term;//determine term
            test.setExp(Exp(term));
            test.Display(choice);
            break;
        default:
            break;
    }
    
    system("pause"); // remember to include cstdlib
    return 0;
}
