#include<iostream>
#include<string>
using namespace std;

class Hourglass
{
	int h,i,n;
	char m;
public:
	Hourglass(int num,char pic)
	{
		setnumber(num);
		setpicture(pic);
	}
	void setnumber(int num)
	{
		number=num;
	}
	void setpicture(char pic)
	{
		picture=pic;
	}
	void displaymessage()
	{
		for(int h=1;h<((number-1)/2)+1;h=h+1)
		{
			for(int i=1;i<h+1;i=i+1)
			{
				cout<<picture;
			}
			for(int i=((number-1)/2)+1-h;i>0;i=i-1)
			{
				cout<<" ";
			}
			for(int i=((number-1)/2)-h;i>0;i=i-1)
			{
				cout<<" ";
			}
			for(int i=1;i<h+1;i=i+1)
			{
				cout<<picture;
			}
			cout<<endl;
		}
		for(h=1;h<number+1;h=h+1)
		{
			cout<<picture;
		}
		cout<<endl;
		for(int h=1;h<((number-1)/2)+1;h=h+1)
		{
			for(int i=((number-1)/2)+1-h;i>0;i=i-1)
			{
				cout<<picture;
			}
			for(int i=1;i<h+1;i=i+1)
			{
				cout<<" ";
			}
			for(int i=1;i<h;i=i+1)
			{
				cout<<" ";
			}
			for(int i=((number-1)/2)+1-h;i>0;i=i-1)
			{
				cout<<picture;
			}
			cout<<endl;
		}
	}
private:
	int number;
	char picture;
};
int main()
{
	int a;
	for(a=1;a>0;a=a+1)
	{
		int n;
		char m;
		cout<<"請輸入菱形高度:";
		cin>>n;
		cout<<"請輸入圖案:";
		cin>>m;

		Hourglass hourglass(n,m);
		hourglass.displaymessage();
	}
	system("pause");
	return 0;
}