#include <iostream>
using namespace std;

class Hourglass
{
	public:
		Hourglass(int high,char p){
			int a,b,c,d,e,f,g;
			for(a=1;a<=high/2;a++){
				for(b=1;b<=a;b++){
					cout <<p;
				}
				cout<<" ";
				for(c=a;c<high/2;c++){
					cout << "  ";
				}
				for(d=1;d<=a;d++){
					cout <<p;
				}
				cout << endl;
			}
			for(e=1;e<=high;e++){
				cout <<p;
			}
			cout<<endl;
			for(f=1;f<=high/2;f++){
				for(g=f;g<=high/2;g++){
					cout <<p;
				}
				cout << " ";
				for(g=1;g<f;g++){
					cout << "  ";
				}
				for(g=f;g<=high/2;g++){
					cout <<p;
				}
				cout << endl;
			}
		}
};

int main(){
	int hight;
	char p1;
	while(true){
		cout << "請輸入高度: ";
		cin >> hight;
		cout <<"\n"<<"請輸入圖形";
		cin >> p1;
		Hourglass abc(hight,p1);
		continue;
	}
	return 0;
}
