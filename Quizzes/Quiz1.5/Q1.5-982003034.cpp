#include <iostream>
#include <string>
#include <sstream>
using namespace std;

class hourglass {

    public:
    hourglass (int in_height, char in_pic){
            draw_hourglass(in_height, in_pic);
    }

    private:

    void draw_hourglass (int height, char pic){
        if (height%2 == 0) cout << "oops, you entered an even number. input must be odd, retry\n";
        else {

            for (int uh=0; uh < (height-1)/2; uh++) {
                for (int l=0; l <= uh ; l++) cout << pic;
                for (int m=2; m<height-2*uh; m++) cout << ' ';
                for (int r=0; r<=uh; r++) cout << pic;
                cout << "\n";
            }

            for (int ml=0; ml < height; ml++) cout << pic;
            cout << "\n";

            for (int lh=0; lh < (height-1)/2; lh++) {
                for (int l=0; l <= ((height-1)/2)-lh-1; l++) cout << pic;
                for (int m=0; m<2*lh+1; m++) cout << ' ';
                for (int r=0; r <= ((height-1)/2)-lh-1; r++) cout << pic;
                cout << "\n";
            }

        }
    }

};

int main()
{
    int in_height = 0;
    char in_pic = ' ';
    string input = "";

    while(true){

        string input = "";

        cout << "insert the height of the hourglass: ";
        getline(cin, input);
        stringstream ss1(input);
        if (ss1>>in_height){
            cout << "insert the picture of the hourglass: ";
            getline(cin, input);
            stringstream ss2(input);
            if (ss2>>in_pic) hourglass hg = hourglass (in_height, in_pic);
            else cout << "error wrong input, input must be a char\n";
        }
        else cout << "error wrong input, input must be an integer\n";


    }
    return 0;
}
