#include <iostream>
#include <string>
using namespace std;

class Hourglass
{
public:
	Hourglass(string Graph,int Height)
	{
		setgraph(Graph);
		setheight(Height);
		displayresult();
	}
	void setgraph(string Graph)
	{
		graph = Graph;
	}
	void setheight(int Height)
	{
		height = Height;
	}
	void displayresult()
	{
		int a = (height-1) / 2;
		int i;
		int j;
		int k;

		for (i=1;i<=a;i++)
		{
			for (j=a-i;j<a;j++)
			{
				cout << graph;
			}
			for (k=i;k<a;k++)
			{
				cout << " ";
			}
			cout << " ";
			for (k=i;k<a;k++)
			{
				cout << " ";
			}
			for (j=a-i;j<a;j++)
			{
				cout << graph;
			}
			cout << endl;
		}
		for (i=1;i<=height;i++)
		{
			cout << graph;
		}
		cout << endl;
		for (i=1;i<=a;i++)
		{
			for (j=i-1;j<a;j++)
			{
				cout << graph;
			}
			for (k=a-i+1;k<a;k++)
			{
				cout << " ";
			}
			cout << " ";
			for (k=a-i+1;k<a;k++)
			{
				cout << " ";
			}
			for (j=i-1;j<a;j++)
			{
				cout << graph;
			}
			cout <<endl;
		}

	}
private:
	string graph;
	int height;
};

int main()
{
	string Graph;
	int Height;
	int z=1;

	while (z!=0)
	{
		cout << "Please input the graph : ";
		cin >> Graph;
		cout << "Please input the height : ";
		cin >> Height;

		if ( Height % 2 == 0)
		{
			cout << "This operation is invalid." << endl;
		}
		else
		{
			Hourglass(Graph,Height);
				
		}
	}

	system("pause");
	return 0;
}