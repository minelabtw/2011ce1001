#include <cstdlib>
#include <iostream>
using namespace std;

class Hourglass{
	public:
		Hourglass(int h,char p){
			height = h;
			pic = p;
			output(height);
		}
		void output(int h){
			for(int i=1;i<=h/2;i++){
				for(int j=0;j<i;j++){
					cout << pic;
				}
				for(int j=h-2*i;j>0;j--){
					cout << " ";
				}
				for(int j=0;j<i;j++){
					cout << pic;
				}
				cout << endl;
			}
			for(int i=0;i<h;i++){
				cout << pic;
			}
			cout << endl;
			for(int i=1;i<=h/2;i++){
				for(int j=h/2;j>=i;j--){
					cout << pic;
				}
				for(int j=0;j<i*2-1;j++){
					cout << " ";
				}
				for(int j=h/2;j>=i;j--){
					cout << pic;
				}
				cout << endl;
			}
		}
	private:
		int height;
		char pic;
};

int main(){
	int height;
	char pic;
	
	while(1){
		cout << "Please enter a number of height: ";
		cin >> height;
		cout << "Please enter a pic you want: ";
		cin >> pic;
		Hourglass hourglass(height,pic);
		cout << endl;
	}
	
	system("pause");
	return 0;
}
