#include <iostream>
using namespace std;

class Hourglass
{
public:
	Hourglass(int height,char graph)
	{
		for (int x=height;x>=1;x--)
		{
			for (int a=height;a>=x;a--)
			{
				cout << graph;
			}
			for (int b=1;b<=x;b++)
			{
				cout << " ";
			}
			for (int d=2;d<=x;d++)
			{
				cout << " ";
			}
			for (int c=height;c>=x;c--)
			{
				cout << graph;
			}
			cout << endl;
		}
		for(int e=2*height+1;e>=1;e--)
		{
            cout << graph;
		}
		cout << endl;
		for (int f=height;f>=1;f--)
		{
			for (int g=1;g<=f;g++)
			{
				cout << graph;
			}
			for (int h=height;h>=f;h--)
			{
				cout << " ";
			}
			for (int i=height-1;i>=f;i--)
			{
				cout << " ";
			}
			for (int j=1;j<=f;j++)
			{
				cout << graph;
			}
			cout << endl;
		}
	}
private:
	int height;
	char grpah;
};

int h;
char g;
int main()
{
	for (int i=1;i>=1;i++)
	{
		cout << "請輸入一個奇數當作高度:";
		cin >> h;
		if (h<0 || h%2==0)
			cout << "Error!!" << endl;
		else
		{
		h =h/2;
		cout << "請輸入一個字元當作圖案:";
		cin >> g;
		Hourglass (h,g);
		}
	}
	system ("pause");
	return 0;
}
