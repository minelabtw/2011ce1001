#include <iostream>
#include <string>
using namespace std;

class Hourglass
{
public:
	Hourglass( int height, string graph )
	{
		h = height;
		g = graph;
		start();
	}

	void start()
	{
		for( int i=1; i<(h+1)/2; i++ )
		{
			for( int j=i; j>0; j-- )
			{
				cout << g;
			}
			for( int j=h-2*i; j>0; j-- )
			{
				cout << " ";
			}
			for( int j=i; j>0; j-- )
			{
				cout << g;
			}
			cout << endl;
		}
		
		for( int i=h; i>0; i-- )
		{
			cout << g;
		}
		cout << endl;
		
		for( int i=1; i<(h+1)/2; i++ )
		{
			for( int j=(h+1)/2-i; j>0; j-- )
			{
				cout << g;
			}
			for( int j=2*i-1; j>0; j-- )
			{
				cout << " ";
			}
			for( int j=(h+1)/2-i; j>0; j-- )
			{
				cout << g;
			}
			cout << endl;
		}
	}
private:
	int h;
	string g;
};

int main()
{
	int he;
	string gr;
	for( int i=0; i>=0; i++ )
	{
	cout << "Decide height of hourglass: ";
	cin >> he;
	cout << "Decide graphic of hourglass: ";
	cin >> gr;
	Hourglass myHourglass( he, gr );
	}
	return 0;
}