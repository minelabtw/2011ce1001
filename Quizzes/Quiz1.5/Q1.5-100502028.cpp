#include<iostream>
#include<string>
using namespace std;

class Hourglass
{
public:
	Hourglass(string g , int h)
	{
		x = h / 2;
		for(int i=0;i<x;i++)
		{
			for(int j=0;j<=i;j++)
			{
				cout << g;
			}
			for(int j=x;j>i;j--)
			{
				cout << " ";
			}
			for(int j=x-1;j>i;j--)
			{
				cout << " ";
			}
			for(int j=0;j<=i;j++)
			{
				cout << g;
			}
			cout << endl;
		}
		for(int i=1;i<=h;i++)
		{
			cout << g;
		}
		cout << endl;
		for(int i=0;i<x;i++)
		{
			for(int j=x;j>i;j--)
			{
				cout << g;
			}
			for(int j=0;j<i+1;j++)
			{
				cout << " ";
			}
			for(int j=0;j<i;j++)
			{
				cout << " ";
			}
			for(int j=x;j>i;j--)
			{
				cout << g;
			}
			cout << endl;
		}
	}
private:
	string g;
	int h , x;
};

int main()
{	
	string pattern;
	int height;

	while(true)
	{
		cout << "Please enter the pattern you want: ";
		cin  >> pattern;
		cout << "please enter the height(odd integer) you want: ";
		cin >> height;

		Hourglass hourglass(pattern , height);
	}
	system ("pause");
	return 0;
}