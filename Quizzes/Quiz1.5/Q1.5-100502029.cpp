#include <iostream>
#include <string>
using namespace std;

class Hourglass
{
public:
	Hourglass(int c, string b)
	{	
		int a;
		height = c;
		figure = b;
		
		a = height / 2;
		for(int i=1;i<=a;i++)
		{
			for(int j=1;j<=i;j++)
			{
				cout << b;
			}
			for(int j=a;j>=i;j--)
			{
				cout << " ";
			}
			for(int j=a;j>i;j--)
			{
				cout << " ";
			}
			for(int j=1;j<=i;j++)
			{
				cout << b;
			}
			cout << endl;		
		}
		for(int i=1;i<=height;i++)
		{
			cout << b;
		}
		cout << endl;
		for(int i=1;i<=a;i++)
		{
			for(int j=i;j<=a;j++)
			{
				cout << b;
			}
			for(int j=i;j>=1;j--)
			{
				cout << " ";
			}
			for(int j=i;j>1;j--)
			{
				cout << " ";
			}
			for(int j=i;j<=a;j++)
			{
				cout << b;
			}
			cout << endl;
		}
		cout << endl;
	}
private:
	int height;
	string figure;
};

int main()
{
	int x;
	string y;

	while(true)
	{
		cout << "請輸入沙漏的高度(奇數): ";
		cin >> x;
		cout << "請輸入想要的圖案: ";
		cin >> y;

		Hourglass hourglass(x,y);
	}

	system("pause");
	return 0;
}