#include <iostream>
using namespace std;

class Hourglass {
private:
   int height;
   string sign;
   
public:
   Hourglass(int i, string s) {
      height = i;
      sign = s;
   } 
   void show() {
      for(int i=0; i<height/2; i++) {
         for(int j=0; j<=i; j++) {
            cout<<sign;
         }
         for(int j=0; j<height-(i+1)*2; j++) {
            cout<<" ";
         }
         for(int j=0; j<=i; j++) {
            cout<<sign;
         }
         cout<<endl;
      }
      for(int i=0; i<height; i++) {
         cout<<sign;
      }
      cout<<endl;
      for(int i=height/2; i>0; i--) {
         for(int j=0; j<i; j++) {
            cout<<sign;
         }
         for(int j=0; j<height-(i)*2; j++) {
            cout<<" ";
         }
         for(int j=0; j<i; j++) {
            cout<<sign;
         }
         cout<<endl;
      }
   }
};

int main() {
   int i;
   string s;
   while(true) {
      while(true) {
         cout<<"請輸入高度(奇數且大於等於3)跟圖案"<<endl; 
         cin>>i>>s;
         if(i>2&&i%2==1) {
            break;
         }
         else {
            cout<<"輸入非奇數或輸入小於3，請重新輸入"<<endl; 
         }
      }
      Hourglass h = Hourglass(i,s);
      h.show();
      system("pause");
   }
   return 0;
}
