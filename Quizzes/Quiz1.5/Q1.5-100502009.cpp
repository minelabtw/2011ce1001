#include<iostream>
using namespace std;

class Hourglass
{
public:
	Hourglass(int num,char pic)
	{
		number = num;
		picture = pic;
	}
	void displayMessage()
	{
		int x=(number-1)/2;
		for(int y=1;y<x+1;y++)
		{
			for(int z=y;z>0;z--)
			{
				cout<<picture;
			}
			for(int p=number-2*y;p>0;p--)
			{
				cout<<" ";
			}
			for(int q=y;q>0;q--)
			{
				cout<<picture;
			}
			cout<<endl;
		}
		for(int r=number;r>0;r--)
		{
			cout<<picture;
		}
		cout<<endl;
		for(int w=x;w>0;w--)
		{
			for(int l=w;l>0;l--)
			{
				cout<<picture;
			}
			for(int n=number-2*w;n>0;n--)
			{
				cout<<" ";
			}
			for(int k=w;k>0;k--)
			{
				cout<<picture;
			}
			cout<<endl;
		}
	}
private:
	int number;
	char picture;
};

int main()
{
	for(int v=1;v++;)
	{
		int a;
		char b;
		cout<<"Please enter an integer to descide the height:";
		cin>>a;
		cout<<"Please enter a graph to descide your picture:";
		cin>>b;
		Hourglass graph(a,b);
		graph.displayMessage();
	}
	system("pause");
	return 0;
}