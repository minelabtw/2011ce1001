#include<iostream>
#include<cstdlib>
#include<fstream>
#include<string>
using namespace std;

int gcd(int p,int q)//最大公因數 
{
    if(p==0)//如果p=0 q就是最後的餘數 
    {
        return q;    
    }else if(q==0)//如果q=0 q就是最後的餘數
    {
        return p;       
    }    
    if(p>q)
    {
        int a = p%q;
        return gcd(q,a);//輾轉相除法    
    }else if(p<q)
    {
        int a = q%p;    
        return gcd(p,a);//輾轉相除法 
    }else if(p=q){
        return p;//如果相等 最大公因數就是p和q自己    
    }
}

int lcm(int p,int q)//最大公倍數 
{
    return (p*q)/gcd(p,q);   
}


int main()
{
    ifstream inputfile("input.txt",ios::in);//ifstream constructor opens the file 
    if( !inputfile )//如果不能開啟就離開程式 
    {
        cerr << "File could not be opened" << endl;
        exit(1);    
    }
    
    ofstream outputfile("A12-100502505-1.txt",ios::out);//ofstream constructor opens the file 
    if( !outputfile )//如果不能創造就離開程式 
    {
        cerr << "File could not be opened" << endl;
        exit(1);
    }
    
    int number1;//測資 
    int number2;//測資 
    int i=1;
    
    while(inputfile >> number1 >> number2)//讀取測資 
    {
        outputfile << "第" << i << "組測資 = " << number1 << ' ' << number2 << endl;
        outputfile << "第" << i << "組測資 GCD = " << gcd(number1,number2) << endl;
        outputfile << "第" << i << "組測資 LCM = " << lcm(number1,number2) << endl;
        outputfile << endl << endl;
        i++;
    }
    return 0;
}//完成 
