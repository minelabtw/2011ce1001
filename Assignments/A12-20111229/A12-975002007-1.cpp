#include <iostream>
#include <fstream>
#include <cstdlib>
using namespace std;

int GCD(int num1,int num2){
    int temp;
    temp = num1 % num2;
    if (temp==0)
        return num2;
    else
        return GCD(num2,temp);
}

int LCM(int num1,int num2){
    return (num1 * num2 / GCD(num1,num2));
}

int main(){
    int num1,num2,row=1,temp;
    ifstream inFile("input.txt");
    ofstream outFile("A12-975002007-1.txt",ios::out);
    
    while (!inFile.eof()){
        inFile >> num1 >> num2;
        if (num1<num2){
            temp = num1;
            num1 = num2;
            num2 = temp;
        }
        outFile << "第" << row << "組測資 = " << num1 << " " << num2 << endl;
        outFile << "第" << row << "組測資 GCD = " << GCD(num1,num2) << endl;
        outFile << "第" << row << "組測資 LCM = " << LCM(num1,num2) << endl << endl;
        row++;
    }
    
    inFile.close();
    outFile.close();
    system ("pause");
    return 0;
}
