#include<iostream>
#include<cstdlib>
#include<fstream>
using namespace std;

bool x = true;
int hold=0,result1=0,result2=0;
int count=1;

//function gcd to calculate the Greatest Common Divisor number of two integers
int gcd(int big,int small)
{
    while(x=true) //while loop to do the recursion
    {
        hold = big % small;
        if(hold != 0) //recursion step
        {
            big = small;
            small = hold;
            gcd(big,small);
        }
        else //base case
        {
            result1 = small;
            x = false;
            break; //exit while loop
        }
    } //end while loop
    return result1;
} //end function gcd

//function lcm to calculate the Least Common Multiple number of two integers
int lcm(int number1,int number2)
{
    result2 = number1 * number2 / gcd(number1,number2);
    return result2;
} //end function lcm

int main()
{
    ifstream inClientFile("input.txt",ios::in); //ifstream constructor opens the file which has existed
    if (!inClientFile) //exit program if ifstream could not open file
    {
        cerr << "File could not be opened" << endl;
        exit(1);
    } //end if statement
    ofstream outClientFile("A12-100502028-1.txt",ios::out); //ofstream constructor opens a new file
    if (!outClientFile) //exit program if outstream could not open file
    {
        cerr << "File could not be opened" << endl;
        exit(1);
    } //end if statement
    int number1,number2,hold;
    if(number1 < number2) //if statement to exchange the numbers if number1 < number2
    {
        hold = number1;
        number1 = number2;
        number2 = hold;
    } //end if statement
    while (inClientFile >> number1 >> number2) //while loop to display each record in file
    {
        outClientFile << "第" << count << "組測資 = " << number1 << ' ' << number2 << endl
        << "第" << count << "組測資 GCD = " << gcd(number1,number2) << endl
        << "第" << count << "組測資 LCM = " << lcm(number1,number2) << endl << endl;
        count++;
    } //end while loop
    cout << "The program has done!!!\n"
        << "Go to see the A12-100502028-1.txt now!!!" << endl;
    system("pause");
    return 0;
}
