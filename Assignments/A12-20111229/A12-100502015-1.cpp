#include<iostream>
#include<cstdlib>
#include<fstream>
#include<string>
#include<iomanip>
using namespace std;
int gcd(int numgcd1,int numgcd2)
{
    int gcdnumber;   //處存餘數
    if (numgcd1==1||numgcd2==1)
    {
         return 1 ;
    }

    else if(numgcd1>numgcd2)//當地一個數字大於第2個,第一個除第2個
    {
         if(numgcd1%numgcd2!=0)
         {
          gcdnumber= numgcd1%numgcd2;
          return gcd(gcdnumber,numgcd2);//在傳回去一次,直到算出最大公因式
         }
         else if(numgcd1%numgcd2==0)//當餘數等於0，即答案
         {

          return numgcd2;
         }
    }
    else if(numgcd1<numgcd2)
    {
         if(numgcd2%numgcd1!=0)
         {
          gcdnumber= numgcd2%numgcd1;
          return gcd(numgcd1,gcdnumber);
         }
         else if(numgcd2%numgcd1==0)
         {

          return numgcd1;
         }
    }

}
int lcm(int numlcm1,int numlcm2)
{
    return numlcm1*numlcm2/gcd(numlcm1,numlcm2);//最小公倍數算式
}
int main()
{
    ifstream infile("input.txt",ios::in);//寫入檔案
    ofstream outfile("A12-100502015-1.txt",ios::out);//輸出檔案
    if(!infile)
    {
        cerr<<"could not opened"<<endl;
        exit(1);
    }
    int num1,num2,counttime=1;

    while(infile>>num1>>num2)//當輸入檔案時，輸出文字到另一個TXT中
    {
        outfile<<"第"<<counttime<<"組測字 = "<<num1<<" "<<num2<<endl<<
        "第"<<counttime<<"組測字 "<<"GCD"<<" "<<"="<<" "<<gcd(num1,num2)<<endl<<
        "第"<<counttime<<"組測字 "<<"LCM"<<" "<<"="<<" "<<lcm(num1,num2)<<endl<<endl;
        counttime+=1;
    }
    system("pause");
    return 0;
}
