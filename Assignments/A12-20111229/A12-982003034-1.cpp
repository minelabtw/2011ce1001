#include <iostream>
#include <fstream> // for file I/O

using namespace std;

// copied from TA's code - as exactly as possible

int gcd (int p, int q){
    if((p%q) == 0) return q;
    else return gcd(q, p%q);
}

int lcm (int p, int q){
    return (p*q)/gcd(p,q);
}

// QQ

int main()
{
    int n0;
    int n1;

    int temp;
    int count = 1;

    string infilename = "input.txt";
    string outfilename = "A12-982003034-1.txt";

    ifstream ifs (infilename.c_str());
    ofstream ofs (outfilename.c_str());

    // failure cases
    if (!ifs){
        cout << "cannot find file!\n";
        return -1;
    }

    if (!ofs){
        cout << "cannot open file!\n";
        return -2;
    }

    while (ifs >> n0 >> n1){

        ofs << "Number set " << count << ": " << n0 << ", " << n1 << endl;

        // swap the integers in order to match n0 > n1 before doing gcd & lcm
        if (n0 < n1){
            temp = n1;
            n1 = n0;
            n0 = temp;
        }

        ofs << "Set " << count << " gcd: " << gcd (n0, n1) << endl;
        ofs << "Set " << count << " lcm: " << lcm (n0, n1) << endl;
        count ++;

    }
}
