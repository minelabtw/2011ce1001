#include <iostream>
#include <cstdlib>
#include <fstream>
using namespace std;

int GCD(int number1,int number2)  // 最大公因數 
{
	int storenum=0;
	if (number1<number2)  // 運算時讓 number1 大於 number2 
	{
		storenum=number2;
		number2=number1;
		number1=storenum;
	}	
	int remainder=0;  // remainder 為餘數	
	remainder = number1%number2;
	if (remainder==0)
	{
		return number2;	
	}
	else if (remainder==1)
	{
		return 1;	
	}
	else if (remainder>1)
	{
		return GCD(number2,remainder);	
	}
}

int LCM(int number1,int number2)  // 最小公倍數  
{
	return (number1*number2)/GCD(number1,number2);
}

int main()
{
    ifstream indataFile("input.txt",ios::in);  // ifstream constructor opens the file
    
    if (!indataFile)
    {
                      cerr << "File could not be opened!!!" << endl;
                      system("pause");
                      return 0;
                      exit(1);                      
    }
    
    ofstream outdataFile("A12-100502511-1.txt",ios::out);  // ofstream constructor opens the file
    
    if (!outdataFile)
    {
                      cerr << "File could not be opened!!!" << endl;
                      system("pause");
                      return 0;
                      exit(1);               
    }    
    
    int num1,num2;
    int item=1;
    
    while(indataFile >> num1 >> num2)  // display in file 
    {          
              outdataFile << "第" << item << "組測資 = " << num1 << ' ' << num2 << endl;
              outdataFile << "第" << item << "組測資 GCD = " << GCD(num1,num2) << endl;
              outdataFile << "第" << item << "組測資 LCM = " << LCM(num1,num2) << endl << endl;
              item++;  // 上面執行完後，將item累加上去 
    }
    cout << "A12-100502511-1.txt 已創建!!!\n請打開 A12-100502511-1.txt 查看最後結果!!!\n";
    system("pause");
    return 0;
}
