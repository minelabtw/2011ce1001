#include<iostream>
#include<cstdlib>
#include<fstream>
using namespace std;

int gcd(int p, int q)//gcd的公式
{
	if(p%q==0)
	{
		return q;
	}
	else
	{
		int store;
		store=q;
		q=p%q;
		p=store;
		return gcd(p,q);
	}
}

int lcm(int p, int q, int gcd)//lcm的公式
{
	int a;
	a=p*q/gcd;
	return a;
}

int main()
{
	ifstream inputfile("input.txt",ios::in);//讀input

	if(!inputfile)//錯誤訊息
	{
		cerr<<"File could not be opened"<<endl;
		exit(1);
	}

	int p,q;

	ofstream outputfile("A12-100502518-1.txt",ios::out);//把檔案傳出去

	if(!outputfile)
	{
		cerr<<"File could not be opened"<<endl;
		exit(1);
	}

	while(inputfile>>p>>q)//每次讀一行後就把他帶進公式傳出去
	{
		int count=1;
		outputfile<<"第"<<count<<"組測資 = "<<p<<" "<<q<<endl
			<<"第"<<count<<"組測試 GCD = "<<gcd(p,q)<<endl
			<<"第"<<count<<"組測試 LCM = "<<lcm(p,q,gcd(p,q))<<"\n"<<endl;
		count++;
	}

	inputfile.close();
	outputfile.close();
	return 0;
}