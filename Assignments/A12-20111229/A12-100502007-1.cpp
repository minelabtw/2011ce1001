#include<iostream>
#include<fstream>
#include<cstdlib>
using namespace std;

unsigned long gcd(int,int);
unsigned long lcm(int,int);

int main()
{
    ifstream inclientfile("input.txt",ios::in);
    ofstream outclientfile("A12-100502007-1.txt",ios::out);
    if(!inclientfile)
    {
        cerr<<"file could not be opened"<<endl;
        exit(1);
    }
    int first,second,i=1;
    while(inclientfile>>first>>second)
    {
        outclientfile<<"第"<<i<<"組測資 = "<<first<<' '<<second<<endl;
        outclientfile<<"第"<<i<<"組測資 GCD = "<<gcd(first,second)<<endl;
        outclientfile<<"第"<<i<<"組測資 LCM = "<<lcm(first,second)<<endl<<endl;
        i=i+1;
    }
}
unsigned long gcd(int first,int second)
{
    if (first>second)//若第一輸入數較大
    {
        first=first-second;
        if(second%first==0)
        {
            return first;
        }
    }
    if (first<second)//若第二輸入數較大
    {
        second=second-first;
        if(first%second==0)
        {
            return second;
        }
    }
    if(first==second)//若兩數相同
    {
        return first;
    }
    return gcd(first,second);
}
unsigned long lcm(int first,int second)//最小公倍數
{
    double finalfirst,finalsecond;
    finalfirst=first/gcd(first,second);
    finalsecond=second/gcd(first,second);
    return finalfirst*finalsecond*gcd(first,second);
}

