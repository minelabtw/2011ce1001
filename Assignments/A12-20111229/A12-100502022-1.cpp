#include<iostream>
#include<cstdlib>
#include<fstream>
#include<string>
using namespace std;
int gcd(int a,int b){//use recursive funtion to cal the gcd
    int c;//initiailize the 餘數
    if(a>b){//輸入的前數比後數大
        c=a%b;
        if(a%b==0){
            return b;//輾轉結束找到GCD
        }
        else return gcd(b,c);//使除數榆樹繼續做輾轉

    }
    //假設後數大於前數的計算
    else if(b>a){
        c=b%a;
        if(b%a==0){
            return a;
        }
        else return gcd(a,c);
    }
    else return a;//前數後數依樣return a or b
}
int lcm(int a,int b){

    return (a*b)/gcd(a,b);//cal lcm
}


int main(){
    ifstream inClientFile( "input.txt",ios::in );//讀檔
    if(!inClientFile)//沒讀到的情形
    {
        cerr<<"File could not be opende"<<endl;//顯示錯誤訊息
        exit(1);//回到原始狀態
    }
    ofstream outClientFile( "A12-100502022-1.txt",ios::out );//寫出一個檔名我的學號的TXT
    if(!outClientFile)//沒產生檔案的情形
    {
        cerr<<"File could not be opende"<<endl;//回報錯誤訊息
        exit(1);//回到原始狀態
    }
    //宣告兩個變數來對應資料中的前數後數
    int number1;
    int number2;
    int n=1;//從第一筆資料開始
    while(inClientFile>>number1>>number2){//continuously reading number of data members
        outClientFile <<"第"<<n<<"組測資 = "<<number1<<" "<<number2<<endl
    <<"第"<<n<<"組測資 GCD = "<<gcd(number1,number2)<<endl
    <<"第"<<n<<"組測資 LCM = "<<lcm(number1,number2)<<endl<<endl;//table type cout
    n++;//依次寫入
    }//end while
    return 0;
}
