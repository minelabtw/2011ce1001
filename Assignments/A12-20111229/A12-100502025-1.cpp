#include<iostream>
#include<fstream>
#include<cstdlib>

using namespace std;

int gcd( int , int );  //最大公因數的函式
int lcm( int , int );  //最小公倍數的函式

int main()
{
	int number1,number2,number3 = 1;  //宣告三個變數，並令number3的初始值為1
	ifstream inClientFile ( "Input.txt" , ios::in );  //讀入一個檔案，名稱為Input.txt
	ofstream outClientFile ( "A12-100502025-1.txt" , ios::out );  //讀出一個檔案，名稱為A12-100502025-1.txt

	if (!inClientFile)  //若讀不到Input.txt
	{
		cerr << "File could not be opend" << endl;  //則顯示出:無法開啟檔案
		exit(1);  //代表程式出錯
	}

	while(inClientFile >> number1 >> number2 )  //從Input.txt讀入一組測資，共兩個數
	{
		outClientFile << "第" << number3 << "組測資 = " << number1 << " " << number2 << endl;
		outClientFile << "第" << number3 << "組測資 GCD = " << gcd( number1 , number2 ) << endl;  //算出最大公因數
		outClientFile << "第" << number3 << "組測資 LCM = " << lcm( number1 , number2 ) << endl;  //算出最小公倍數
		outClientFile << endl;  //空一行
		number3++;  //每次都把number3加1
	}

	outClientFile.close();  //關掉檔案

	system("pause");
	return 0;
}

int gcd(int p , int q)  //最大公因數的函式
{
	if(p > q)  
	{
		p = p % q;  
		if(p != 0)  
		{
			return gcd(p,q);  
		}
		if(p == 0)  
		{
			return q;  
		}
	}
	else if(q>p > 0) 
	{
		q = q % p;  
		if(q != 0)  
		{
			return gcd(p,q);  
		}
		if(q == 0)  
		{
			return p;  
		}
	}
}

int lcm(int p , int q)  //最小公倍數的函式
{
	return  p * q / gcd(p , q);  
}