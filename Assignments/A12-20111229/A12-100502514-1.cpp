/******************************************************************************
 * Assignment 12-1
 * School/Year: National Central University, Taiwan / Freshman
 * Name:        江衍霖 ; Chiang,Yen-lin
 * E-mail:      flashcity123@kimo.com
 *              100502514@cc.ncu.edu.tw
 * Course:      NCU1001-CE1001A
 * Course Name: Introduction to Computer Science
 * Class:       CSIE 1A
 * Submitted:   Dec.29,2011
 *
 * Description:
 * Calculate GCD and LCM numbers using recursion;
 * and put the results in a file "A12-100502514.txt" in a specified format.
 * Functions GCD and LCM are referenced by Quiz #3.
 *
 * NOTICE: From now and then, I will usually include my own header.
 * If the compiling passed but there's a function never seen,
   it may be defined in my own header.
 *
 ******************************************************************************/

/****************************************
 *       Include my Custom Header       *
 ****************************************/
#include "A12-100502514-1.h"
#include <fstream>
using namespace FlashTeens;
using namespace std;


/**********************************
 *  Global Variables & Functions  *
 **********************************/

//Line Constant Arrangement
const int MESSAGE_LINE = 1;
const int LAST_CURSOR = 10;

//Calculating Greatest Common Divisor
long long GCD(long long p, long long q){
    if(p%q==0)return q;
    else return GCD(q, p%q);
}

//Calculating Least Common Multiple
long long LCM(long long p, long long q){
    return p*q / GCD(p, q);
}

//Main Function
int main(){
    //Load the testing data file.
    ifstream fin("input.txt", ios::in);
    if(!fin){
        //If file is not found, the following message will be shown.
        cout << gotoXY(1, MESSAGE_LINE)
             << toColorString("<yellow>找不到檔案\"input.txt\"。")
             << gotoXY(0, LAST_CURSOR);
        //Exit Program
        return 1;
    }

    //Output a file of calculated results.
    ofstream fout("A12-100502514-1.txt", ios::out);

    //Declare variables x and y for file input.
    int x, y;
    //Declare a number for counter.
    int count = 0;

    while(fin >> x >> y){
        //Declare long-long int of x, y
        //In order to match the data type for the functions.
        long long x_long = x, y_long = y;

        //Calculate and Output
        fout << "第" << (++count) << "組測資 = "
             << x << ' ' << y << '\n';
        fout << "第" << count << "組測資 GCD = "
             << GCD(x_long, y_long) << '\n';
        fout << "第" << count << "組測資 LCM = "
             << LCM(x_long, y_long) << '\n';

        //Output the seperation character.
        fout << endl;

    }

    //Close both input and output files.
    fin.close();
    fout.close();

    //Output Success Message
    cout << gotoXY(1, MESSAGE_LINE)
         << toColorString("<green>測試結果檔案\"A12-100502514-1.txt\"已產生。")
         << gotoXY(0, LAST_CURSOR);
    //Pause after the file generated.
    pause();
    return 0;
}

