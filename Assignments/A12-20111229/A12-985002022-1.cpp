#include<iostream>
#include<cstdlib>
#include<fstream>

using namespace std;

static int gcd(int a, int b) {
   if(a%b==0) {      //a%b整除 b最大公因數 
      return b;
   }
   else if(b%a==0) { //b%a整除 a最大公因數 
      return a;
   }
   else if(a>b) {    //a大於b a%b 再繼續gcd(a,b) 
      return gcd(a%b,b);
   }
   else if(b>a) {    //b大於a b%a 再繼續gcd(a,b) 
      return gcd(a,b%a);
   }
}

int lsm(int a, int b) {
   return a*b/gcd(a,b);     //a*b/gcd(a,b) 
}

int main() {
   string s1, s2, s3, s4, s5, s6;
   int num1, num2, num3, num4, num5, num6; 
   char buffer[10];
   ifstream fin("input.txt");
   if(fin) {            //從檔案中讀入字串存進s1-s6 
      fin>>s1;
      fin>>s2;
      fin>>s3;
      fin>>s4;
      fin>>s5;
      fin>>s6;
      fin.close();      //關閉檔案 
   }
   else {
      cout<<"error!"<<endl;   //若沒成功讀檔，顯示ERROR 
   }
   strcpy(buffer,s1.c_str()); //string 轉 int 
   sscanf(buffer,"%d",&num1);
   strcpy(buffer,s2.c_str());
   sscanf(buffer,"%d",&num2);
   strcpy(buffer,s3.c_str());
   sscanf(buffer,"%d",&num3);
   strcpy(buffer,s4.c_str());
   sscanf(buffer,"%d",&num4);
   strcpy(buffer,s5.c_str());
   sscanf(buffer,"%d",&num5);
   strcpy(buffer,s6.c_str());
   sscanf(buffer,"%d",&num6);
   cout<<"第1組測資 = "<<num1<<" "<<num2<<endl;    //輸出 
   cout<<"第1組測資 GCD = "<<gcd(num1, num2)<<endl;
   cout<<"第1組測試 LCM = "<<lsm(num1, num2)<<endl;
   cout<<"第2組測資 = "<<num3<<" "<<num4<<endl;
   cout<<"第2組測資 GCD = "<<gcd(num3, num4)<<endl;
   cout<<"第2組測試 LCM = "<<lsm(num3, num4)<<endl;
   cout<<"第3組測資 = "<<num5<<" "<<num6<<endl;
   cout<<"第3組測資 GCD = "<<gcd(num5, num6)<<endl;
   cout<<"第3組測試 LCM = "<<lsm(num5, num6)<<endl;
   system("pause");
   return 0;
}
