#include<iostream>
#include<cstdlib>
#include<fstream>

using namespace std;

int gcd(int,int);//function prototype

int lcm(int,int);//function prototype

int main()
{
    int num1,num2;//宣告兩個整數

    ifstream in_client_file("input.txt",ios::in);

    if(!in_client_file)//處理讀取失敗的情況,以cerr顯示錯誤訊息並結束程式
    {
        cerr<<"檔案讀取發生錯誤"<<endl;
        exit(1);
    }
    ofstream out_client_file("A12-100502017-1.txt",ios::out);

    if(!out_client_file)//處理建立失敗的情況,以cerr顯示錯誤訊息並結束程式
    {
        cerr<<"檔案建立發生錯誤"<<endl;
        exit(1);
    }

    for(int counter=1;in_client_file>>num1>>num2;counter++)
    {
        int gcd_value=(num1>=num2)?gcd(num1,num2):gcd(num2,num1);//判斷num1與num2誰大,決定參數的順序
        int lcm_value=(num1>=num2)?lcm(num1,num2):lcm(num2,num1);
        out_client_file<<"第"<<counter<<"組測資 = "<<num1<<" "<<num2<<endl;
        out_client_file<<"第"<<counter<<"組測資 GCD = "<<gcd_value<<endl;
        out_client_file<<"第"<<counter<<"組測資 LCM = "<<lcm_value<<endl;
        out_client_file<<endl;
    }
    out_client_file.close();//關閉檔案
    cout<<"程式執行完畢"<<endl;
    system("pause");
    return 0;//程式結束
}

int gcd(int p,int q)//return two numbers gcd
{
    int r=p%q;//取pq的餘數
    if (r==0)//處理兩數本身就相等或本身就有倍數關係
        return q;
    else if(q%r==0)//如r值為q的因數，此值即為最大公因數
        return r;
    else//已遞迴的方式根據輾轉相除法
        return gcd(q,r);
}

int lcm(int p,int q)//return two numbers lcm
{
    return (p*q)/gcd(p,q);
}
