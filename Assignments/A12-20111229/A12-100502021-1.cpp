#include<iostream>
#include<cstdlib>
#include<fstream>
#include<iomanip>
#include<string>
using namespace std;

int gcd(int,int);
int lcm(int,int,int);
int main()
{
	int a=1;
	ifstream inqua("input.txt",ios::in);
	ofstream outqua("A12-100502021-1.txt",ios::out);
	if(!inqua)
	{
		cerr<<"file could not be found"<<endl;
		exit(1);	
	}
	if(!outqua)
	{
		cerr<<"file could not be opened"<<endl;
		exit(1);	
	}
	int num1,num2;
	while(inqua>>num1>>num2)
	{
		int c;
		outqua<<"第"<<a<<"組測資 = "<<num1<<" "<<num2<<endl;
		if (num1<num2)
		{
			c=num1;
			num1=num2;
			num2=c;
		}
		
		outqua<<"第"<<a<<"組測資 "<<"GCD"<<" = "<<gcd(num1,num2)<<endl;
		outqua<<"第"<<a<<"組測資 "<<"LCM"<<" = "<<lcm(num1,num2,gcd(num1,num2))<<endl<<endl;

		a++;
	}
	
	return 0;
}
int gcd(int num1,int num2)
{
	if(num1%num2==0)
	{
		return num2;	
	}
	return gcd(num2,num1%num2);
}
int lcm(int num1,int num2,int gcd)
{
	return num1*num2/gcd;
}
