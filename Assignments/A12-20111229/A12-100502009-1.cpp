#include<iostream>
#include<cstdlib>
#include<fstream>
using namespace std;

int gcd(int,int);
int lcm(int,int);

int main()
{
    ifstream inputfile("input.txt",ios::in);//ifstream constructor opens the file
    if(!inputfile)//exit program if ifstream could not open file
    {
        cerr<<"File could not be opened"<<endl;
        exit(1);
    }
    ofstream outputfile("A12-100502009-1.txt",ios::out);//ofstream constructor opens the file
    if(!outputfile)//exit program if ofstream could not open file
    {
        cerr<<"File could not be opened"<<endl;
        exit(1);
    }

    int num1,num2,a,b,count=0;
    while(inputfile>>num1>>num2)//set a loop to take in the data in the file
    {
        count+=1;
        if(num1>=num2)//set conditions to tell the bigger from the two inputs
        {
            a=num1;
            b=num2;
        }
        else
        {
            a=num2;
            b=num1;
        }
        outputfile<<"第"<<count<<"組測資 = "<<num1<<' '<<num2<<endl;//put the result into the file
        outputfile<<"第"<<count<<"組測資 GCD = "<<gcd(a,b)<<endl;
        outputfile<<"第"<<count<<"組測資 LCM = "<<lcm(a,b)<<endl;
        outputfile<<endl;
    }
    system("pause");
    return 0;
}

int gcd(int n1,int n2)//function to find the greatest common divisor number of the inputs
{
    int hold=n1%n2;
    if(hold==0)
    {
        return n2;
    }
    else
        return gcd(n2,hold);
}

int lcm(int test1,int test2)//function to find the lease common multiple number of the inputs
{
    return (test1*test2)/gcd(test1,test2);
}

