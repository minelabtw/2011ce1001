#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
using namespace std ;
int gcd(int p ,int q)//利用遞迴計算最大公因數 
{
    int n=0;
    n=p%q ;
    if(n==1)
    {   
      return 1 ;
    }
    else if(n==0)
    {
      return q ;  
    } 
    else if(n>1)
    {
      return gcd(q,n) ; 
    } 
}

int lcm(int p ,int q)//利用最大公因數計算最小公倍數 
{
   return (p*q)/gcd(p,q) ;       
}


int main()//參考課本8-4 
{
    ifstream ininputfile("input.txt",ios::in);//讀取input.txt這個檔案 
    if(!ininputfile)//如果不是input這個檔名則跳出 
    {
        cerr<<"File could not be opened"<<endl;
        exit(1);
    }
    ofstream outA12file("A12-100502512.txt",ios::out);//輸出A12-100502512這個檔案 
    if(!outA12file)
    {
        cerr<<"File could not be opened"<<endl;
        exit(1);
    }
    
    int number1,number2,count=1,GCD,LCM ;
    while(ininputfile>>number1>>number2)//當讀取到檔案的number1與number2,則計算其最大公因數與最小公倍數並輸出A12-100502512的檔案 
    {
        GCD=gcd(number1,number2);
        LCM=lcm(number1,number2);
        outA12file<<"第"<<count<<"組測資 = "<<number1<<"  "<<number2<<endl;
        outA12file<<"第"<<count<<"組測資 GCD = "<<GCD<<endl;
        outA12file<<"第"<<count<<"組測資 LCM = "<<LCM<<endl;
        outA12file<<endl ;
        count ++ ;
    }
    
    return 0 ;
}
