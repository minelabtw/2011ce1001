#include<iostream>
#include<cstdlib>
#include<fstream>			// file stream
using namespace std;

int gcd(int,int);			// 宣告function (最大公因數)
int lcm(int,int);			// 宣告function (最小公倍數)

int main()
{
	ifstream read( "input.txt" , ios::in );			// open input.txt for input
	ofstream show( "A12-100502519-1.txt" , ios::out );			// open or create A2-100502519-1.txt for output

	if( !read )			// 若無法開啟 則顯示錯誤訊息 並exit
	{
		cerr << "File could not be opened" << endl;
		exit(1);
	}

	int num1;
	int num2;
	int count = 0;			// 用來跑是第幾組
	
	while( read >> num1 >> num2 )			// 讀每行的兩個數字
	{
		count++;			// 第幾組

		// 以下是輸字到A2-100502519-1.txt
		show << "第" << count << "組測資 = " << num1 << ' ' << num2 << endl;

		if(num1 >= num2)
		{
			show << "第" << count << "組測資 GCD = " << gcd(num1,num2) << endl;			// call function gcd
			show << "第" << count << "組測資 LCM = " << lcm(num1,num2) << endl << endl;			// call function lcm
		}
		else			//若前數小於後數 則換個位置再傳
		{
			show << "第" << count << "組測資 GCD = " << gcd(num2,num1) << endl;
			show << "第" << count << "組測資 LCM = " << lcm(num2,num1) << endl << endl;
		}
	}

	system("pause");
	return 0;
}




int gcd(int n1 , int n2)
{
    if((n1%n2) == 0)			//餘0 表示轉轉相除法結束了 gcd是最後的n2
        return n2;
    else			//遞迴 還沒到餘0 就做輾轉相除
        return gcd(n2, n1%n2);			//n2成了新的n1 而新的n2是原先的n1除n2的餘數
}

int lcm(int n1 , int n2)
{
    return (n1*n2)/gcd(n1,n2);			//lcm 即 兩數相乘除以他們的gcd
}