#include<iostream>
#include<cstdlib>
#include<fstream>
using namespace std;

int gcd(int p,int q) // gcd function
{
    if(p<q)
    {
        int u=q%p;
        if(u==0)
            return p;
        else
            return gcd(p,u); //call the gcd function recursively
    }
    else
    {
        int u=p%q;
        if(u==0)
            return q;
        else
            return gcd(u,q); //call the gcd function recursively
    }
}

int lcm(int p,int q) // lcm function
{
    return (p*q)/gcd(p,q);
}

int main()
{
	int number1,number2,k=1;
    //ofstream contructor opens file
	ifstream inClientFile("input.txt",ios::in);
	if(!inClientFile) //exit if ifstream can't open the file
	{
		cerr<<"Error file!"<<endl;
		exit(1);
	}
    //ofstream contructor opens file
	ofstream outClientFile("A12-100502501-1.txt",ios::out);
	if(!outClientFile) //exit program if ofstream can't create the file
	{
		cerr<<"Error file!"<<endl;
		exit(1);
	}
    //intput the number into the file
	while(inClientFile>>number1>>number2)
	{
	    //output the result
		outClientFile<<"第"<<k<<"組測資 = "<<number1<<" "<<number2<<endl;
		outClientFile<<"第"<<k<<"組測資 GCD = "<<gcd(number1,number2)<<endl;
		outClientFile<<"第"<<k<<"組測資 LCM = "<<lcm(number1,number2)<<endl;
		outClientFile<<endl;
		k++;
	}
	system("pause");
	return 0;
}
