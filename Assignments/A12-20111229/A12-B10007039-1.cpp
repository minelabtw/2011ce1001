#include <cstdlib>
#include <iostream>
#include <fstream>

using namespace std;

int gcd(int,int);
int lcm(int,int);

int main()
{
    char* InputFileName = "input.txt";
    char* OutputFileName = "A12-B10007039-1.txt";
    ifstream TextFileInput;
    ofstream TextFileOutput;
    
    TextFileInput.open(InputFileName,ios::in);//開啟讀取的檔案
    
    if(TextFileInput.fail()){//check file open
        cout << "檔案(" << InputFileName << ")開啟失敗" << endl;
        system("PAUSE");
        return 0;
    }

	int Number[20][2] = {};
	int Counter = 0;
	while(!TextFileInput.eof()){//從資料流將資料放入陣列
		TextFileInput >> Number[Counter][0] >> Number[Counter][1];
		Counter++;
	}

	TextFileInput.close();
	TextFileOutput.open(OutputFileName,ios::out);//開啟寫入的檔案

	for(int ForCounter=0;ForCounter < Counter;ForCounter++){
		if(Number[ForCounter][0] != 0 && Number[ForCounter][1] != 0){//如果兩個數字皆不為0才寫入，防止空白字元產生多組資料
			TextFileOutput << "第" << (ForCounter+1) <<"組測資 = " << Number[ForCounter][0] << " " << Number[ForCounter][1] << endl;
			TextFileOutput << "第" << (ForCounter+1) <<"組測資 GCD = " << gcd(Number[ForCounter][0],Number[ForCounter][1]) << endl;
			TextFileOutput << "第" << (ForCounter+1) <<"組測資 LCM = " << lcm(Number[ForCounter][0],Number[ForCounter][1]) << endl;
			TextFileOutput << endl;
		}
	}
	cout << "檔案(" << OutputFileName << ")寫入完成" << endl;
    system("PAUSE");
	TextFileOutput.close();
    return 0;
}

int gcd(int Number1,int Number2){//最小公因數，參數(int,int)

	if(Number1 < Number2){
		int Number3 = Number1;
		Number1 = Number2;
		Number2 = Number3;
	}
	if(Number2 == 0)return 0;

	int Answer=Number2;
	int gcd_number_next = Number1%Number2;

	if(gcd_number_next){
		gcd_number_next = gcd(Number2,gcd_number_next);
        return gcd_number_next;
	}else{
		return Answer;
	}
}

int lcm(int Number1,int Number2){//最小公倍數，參數(int,int)
	if(Number1 != 0 && Number2 != 0){
		return((Number1*Number2)/gcd(Number1,Number2));
	}else{
		return 0;
	}
}
