#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>
using namespace std;

void read();//初始化
void write();
int gcd(int,int);
int lcm(int,int);

int main()
{
	read();//call 讀取函數
	write();//cal 寫入函數
    system("pause");
    return 0;
}
void read()
{
	ifstream in("input.txt",ios::in);//讀檔
	if (!in)//防錯
	{
		cerr << "Flie could not be opened" << endl;
		exit(1);
	}
	int element[6];

	in >> element[0] >> element[1] >> element[2] >> element[3] >> element[4] >> element[5] ;//存進陣列
	cout << element[0] << " " << element[1] << endl;//排列輸出
	cout << element[2] << " " <<element[3] << endl;
	cout << element[4] << " " <<element[5] << endl;
};
void write()
{
	ofstream out("A12-996001021-1.txt",ios::out);//創檔
	if (!out)//防錯
	{
		cerr << "Flie could not be opened" << endl;
		exit(1);
	}
	int num1,num2,hold;
    cout << "Enter two number in one time:\n";
	cout <<"Enter end-of-flie to end input.\n";
	int i=1;
	while(cin >> num1 >> num2)
	{
	    if (num1<=0||num2<=0)
	    {
	        cout << "Wrong! stop the cpp.";
	        break;
	    }

		out << "第" << i << "組測資 = " << num1 << " " << num2 << endl; //寫入
		if (num1 < num2) // 讓num1大於num2
		{
			hold=num2;
			num2=num1;
			num1=hold;
		}
		out << "第" << i << "組測資 GCD = " << gcd(num1,num2) << endl
			<< "第" << i << "組測試 LCM = " << lcm(num1,num2) << endl <<endl;
		i++;
		cout << "The next data:\n";
	}

}

int gcd(int pgcd , int qgcd)//最大公因數
{
    if(qgcd==0)
    {
        return pgcd;
    }
    else
    {
		pgcd=pgcd%qgcd;
        return gcd(qgcd , pgcd);
    }
}
int lcm(int qlcm , int plcm )//最小公倍數
{
    int anwserlcm = (plcm*qlcm)/gcd(qlcm,plcm);//兩數相乘=最大公因數乘以最小公倍數
    return anwserlcm;
}
