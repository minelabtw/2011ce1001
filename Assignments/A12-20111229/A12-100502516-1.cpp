#include<iostream>
#include<fstream>
#include<cstdlib>
using namespace std;

int gcd(int,int);//計算最大公因數
int lcm(int,int);//計算最小公倍數

int main()
{
    int num1;//測資數字1
    int num2;//測資數字2
    int times=0;//第幾組測資

    ifstream getinput("input.txt",ios::in);

    ofstream outAnswer("A12-100502516-1.txt",ios::out);

    if(!getinput)//失敗訊息並跳出程式
    {
        cerr << "檔案無法讀取" << endl;
        exit(1);
    }

    if(!outAnswer)
    {
        cerr << "檔案創建失敗" << endl;
        exit(1);
    }

    while(getinput >> num1 >> num2)
    {
        times++;

        if(num1<num2)//要是num1較小，則互相調換
        {
            int hold=num1;

            num1=num2;

            num2=hold;
        }

        outAnswer << "第" << times << "組測資 = " << num1 << " " << num2 << endl
                  << "第" << times << "組測資 GCD = " << gcd(num1,num2) << endl
                  << "第" << times << "組測資 LCM = " << lcm(num1,num2) << "\n\n";
    }

    cout << "檔案已建立" << endl;

    getinput.close();//關閉檔案

    outAnswer.close();

    system("pause");

    return 0;
}

int gcd(int num1,int num2)
{
    if(num2==0)
        return num1;
    else
        return gcd(num2,num1%num2);
}

int lcm(int num1,int num2)
{
    return num1*num2/gcd(num1,num2);
}
