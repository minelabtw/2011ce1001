#include<iostream>
#include<fstream>
#include<iomanip>
#include<cstdlib>
using namespace std;

int gcd(int p,int q)//最大公因數(輾轉相除法)  
{
   if(p>q)//p大於q時 
   {
      if( (p%q)==0 )//p除以q的餘數如果等於0時，最大公因數即為q 
         return q;
      if( (p%q)==1 )//p除以q的餘數如果等於1時，最大公因數即為1   
         return 1;
      return gcd(p%q,q);//call自己並把p除以q的餘數繼續做運算(輾轉相除法) 
   }      
   else//q大於p時 
   {
      if( (q%p)==0 )//q除以p的餘數如果等於0時，最大公因數即為p 
         return p;
      if( (q%p)==1 )//q除以p的餘數如果等於1時，最大公因數即為1     
         return 1;
      return gcd(q,q%p);//call自己並把q除以p的餘數繼續做運算(輾轉相除法) 
   }    
}

int lcm(int p,int q)//最小公倍數 
{
   return (p*q)/gcd(p,q);//公式 
} 

int main()
{
   ifstream inputfile("input.txt",ios::in);
   ofstream outputfile("A12-100502513-1.txt",ios::out);
   if(!inputfile)//如果找不到檔案時，離開程式 
   {
      cerr<<"File could not be opened!!"<<endl;
      system("pause");
      exit(1);//離開程式 
   }
   if(!outputfile)//如果找不到檔案時，離開程式 
   {
      cerr<<"File could not be opened!!"<<endl;
      system("pause");
      exit(1);//離開程式 
   }
   
   int number1,number2,count=1;//測資數字1.2，計數器 
   
   while(inputfile>>number1>>number2)//讀入TXT檔的數字 
   {
      outputfile<<"第"<<count<<"組測資 = "<<number1<<' '<<number2<<endl//把這一拖拉庫的東西cout到創立的檔案(txt檔) 
                <<"第"<<count<<"組測資 GCD = "<<gcd(number1,number2)<<endl
                <<"第"<<count<<"組測資 LCM = "<<lcm(number1,number2)<<endl<<endl; 
      count++;
   }
   cout<<"檔案建立完成!!"<<endl;//創立成功 
   
   system("pause");
   return 0;
}      
