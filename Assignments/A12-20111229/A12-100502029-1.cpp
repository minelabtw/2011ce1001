#include <iostream>
#include <fstream>
#include <cstdlib>
using namespace std;

// function prototype
int gcd( int, int );
int lcm( int, int );

int main()
{
    ifstream infile( "input.txt", ios::in ); // ifstream constructor opens "input.txt"
    ofstream outfile( "A12-100502029-1.txt", ios::out ); // ofstream constructor created "A12-100502029-1.txt"

    if ( !infile ) // exit program if unable to create file
    {
        cerr << "File could not be opened" << endl;
        exit( 1 );
    }

    int firstnum, secondnum, set = 1; // variable declaration

    // read the set of numbers from "input.txt", calculate the gcd and lcm, set the result in "A12-100502029-1.txt"
    while ( infile >> firstnum >> secondnum )
    {
        outfile << "第" << set <<"組測資 = " << firstnum << ' ' << secondnum << endl;
        if ( firstnum < secondnum ) // determine which number is larger, set the larger number to firstnum and the smaller to secondnum
        {
            int hold = firstnum;
            firstnum = secondnum;
            secondnum = hold;
        }
        outfile << "第" << set << "組測資 GCD = " << gcd( firstnum, secondnum )
            << "\n第" << set << "組測資 LCM = " << lcm( firstnum, secondnum )
            << "\n" << endl;
        set++;
    }

    system("pause");
    return 0;
}

// calculate the Greatest Common Divisor number
int gcd( int number1, int number2 )
{
    if ( number1 % number2 == 0 )
        return number2;
    else
        return gcd( number2, number1 % number2 );
}

// calculate the Least Common Multiple number
int lcm( int number1, int number2 )
{
    return number1 * number2 / gcd( number1, number2 );
}
