#include <fstream>
#include <cstdlib>
#include <iomanip>
#include<iostream>
#include<string>

using namespace std;

int gcd(int p, int q) // gcd function
{    if((p%q) == 0) // 如果p%q = 0, q 就是 p 和 q 的最大公因數        
          return q;    
     else // 如果 p%q != 0,代表尚未找到 p 和 q 的最大公因數        
          return gcd(q, p%q); //所以我們再次呼叫此function並用修正過後的varible代入 
}

int lcm(int p, int q) // lcm function
{
    return (p*q)/gcd(p,q);
}



int main() //主程式開始
{
    ifstream input("input.txt",ios::in);//讀input.txt 
     
    
    if(!input)//如果讀取發生問題就顯示錯誤訊息 
    {
        cerr<<"Can not open the file!"<<endl;
        system("pause"); 
        exit(1);//結束程式 
    }

    ofstream output("A12-100502005-1.txt",ios::out);//寫入input.txt
    
    if(!output)//如果輸出發生問題就顯示錯誤訊息 
    {
        cerr<<"file could not be opened"<<endl;
        exit(1);
    }
    
    int a,b,i=1;
    
    while(input>>a>>b)
    {
        gcd(a,b);
        lcm(a,b);
        output<<"第"<<i<<"組測資 = "<<" "<<a<<" "<<b<<endl;
        output<<"第"<<i<<"組測資 gcd = "<<" "<<gcd(a,b)<<endl;
        output<<"第"<<i<<"組測資 lcm = "<<" "<<lcm(a,b)<<endl;
        output<<endl;
        i++;
    }



system("pause");

return 0;

}
