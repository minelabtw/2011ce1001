//Assignment12-100502507-汪子超
#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
using namespace std;

int gcd( int, int  );
int lcm( int, int  );

int main()//Function main begins execution
{
	ifstream inFile( "input.txt", ios::in );//Ifstream constructor inputs file
	ofstream outFile( "A12-100502507-1.txt", ios::out );//Ofstream constructor opens file
	//Exit program if unable to open or creat file
	if( !inFile || !outFile )//Overloaded ! operator
	{
		cerr << "File couldn't be opened!" << endl;
		exit( 1 );
	}//End if

	int number01, number02, counter01;//Two numbers for GCD or LCM and counter01 to search position
	string counter02;//Variable to output "一",  "二", or "三"
	counter01 = 0;//Initialize position to 0

	while( counter01 < 16 )//Start loading and storing
	{
		if( counter01 == 0 )//Operate "一",  "二", or "三" should be stored
		{
			counter02 = "一";
		}
		else if( counter01 == 5 )
		{
			counter02 = "二";
		}
		else
		{
			counter02 = "三";
		}
		inFile.seekg( counter01 );//Search position
		inFile >> number01 >> number02;//Load data from input.txt
		outFile << "第" << counter02 << "組測資 = " << number01 << ' ' << number02 << endl//Store data to A12-100502507-1.txt
			<< "第" << counter02 << "組測資 GCD = " << gcd( number01, number02 ) << endl
			<< "第" << counter02 << "組測資 LCM = " << lcm( number01, number02 ) << endl << endl;
		if( counter01 == 5 )//Avoid error
		{
			counter01 = counter01 + 4;
		}
		counter01 = counter01 + 5;
	}
	return 0;
}//End function main

int gcd( int p, int q ) //gcd function
{
    if( (p%q) == 0 ) //If p%q = 0, it means q is the gcd for original p and q
        return q;
    else //If p%q != 0, it means we still have not found the gcd of original p and q
        return gcd( q, p%q ); //So we recursively call the gcd function, and take q and (p%q) as argument to calculate again
}

int lcm( int p, int q )//Function to calculate the least common multiple number of these two numbers
	{
		int result;//Integer to store the result
		result = (p*q)/gcd( p, q );
		if( result < 0 )//Avoid negative result
		{
			result = result * (-1);
		}
		return result;
	}