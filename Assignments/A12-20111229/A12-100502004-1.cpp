#include<iostream>
#include<cstdlib>
#include<fstream>
using namespace std;

int Functiongcd(int p,int q)//用以計算最大公因數的function 
{
  int less;
  
  
  if(p==0||q==0)//結束條件，當其中一項餘數為0 ，傳回最大公因數 
  {  
    int gcd;
    
    if(p>q)
    {
      gcd=p;
      return gcd;  
    } 
    
    if(p<q)
    {
      gcd=q;
      return gcd;         
    }
            
  }
  
  
  
  if(p>q)//下面兩個if用以判斷是要p/q還是q/p並且取餘數 
  {
    less=p%q;
    
    p=less;
    
      
    return Functiongcd(p,q) ;    
         
         
  }     
     
   
  else
  {
    less=q%p;    
    q=less;  
    return Functiongcd(q,p);  
      
      
  }   
  
  
     
     
     
}


int Functionlcm(int p,int q, int gcd)//把公式和數值輸入進來求最小公倍數 
{
  int lcm=p*q/gcd;  
     
  return lcm;   
     
     
}










int main()
{
  ifstream FileInput;
  ofstream FileOutput ;
  FileInput.open("input.txt",ios :: in);  //讀入要測試的測資 
  FileOutput.open("A12-100502004-1.txt",ios :: out);//想要輸出的檔名為 
    
  if(!FileInput)  
    {
      cerr<<"File could not be opened "<<endl;//檢查是否讀的到檔案 
      exit(1);              
                      
    }  
  
  int number1;//宣告兩變數 
  int number2;
  int title=1;//設定要顯示之標題 
  
  while(FileInput>>number1>>number2)//讀入測資 
  {
    Functiongcd(number1,number2);
    Functionlcm(number1,number2,Functiongcd(number1,number2));                                          
    
    
    
                              
    FileOutput<<"第"<<title<<"組測資= "<<number1<<"  "<<number2<<endl;
    FileOutput<<"第"<<title<<"組測資 GCD = "<<Functiongcd(number1,number2)<<endl;
    FileOutput<<"第"<<title<<"組測試 LCM = "<<Functionlcm(number1,number2,Functiongcd(number1,number2))<<endl<<endl;
    
    title++;
                                      
  }
  FileInput.close();
  FileOutput.close();

  return 0;
  
  
  
      
}
