#include<iostream>
#include<cstdlib>
#include<fstream>
#include<iomanip>
using namespace std;

int GCD(int x,int y)
{
	 int answer=x%y;
	 if(answer==x)
	 {
		 int swap=x;
		 x=y;
		 y=swap;
		 return GCD (y,swap);
	 }
	 else if(answer==0)
	 {
		 return y;
	 }
	 else if(answer!=0)
	 {
		 return GCD(y,x%y);
	 }
	 
}
int LCM(int x,int y)
{
	return (x*y)/(GCD(x,y));
}

int main()
{
	ifstream ininput("input.txt",ios::in);
	if(!ininput)
	{
		cerr<<"Error!"<<endl;
		exit(1);
	}
	
	ofstream outinput("A12-100502510.txt",ios::out);
	if(!outinput)
	{
		cerr<<"Error!"<<endl;
		exit(1);
	}

	int first,second,gcd,lcm;
	int times=1;
	
	while(ininput>>first>>second)
	{
		
		gcd=GCD(first,second);
		lcm=LCM(first,second);
		outinput<<"第"<<times<<"組測資 = "<<first<<" "<<second<<endl<<"第"<<times<<"組測資 GCD = "<<gcd<<endl<<"第"<<times<<"組測資 LCM = "<<lcm<<endl<<endl;
		times++;
		while(times>3)
		{
			break;
		}
	}
	return 0;

}