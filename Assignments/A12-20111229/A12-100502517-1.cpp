#include<iostream>
#include<cstdlib>
#include<fstream>
using namespace std;

int gcd(int p, int q)//The Greaetest Common Divisor number of p and q
{
    if(q>p)
    {
        int hold = p;
        p = q;
        q = hold;
    }
    int remainer;

    remainer = p%q;

    if(remainer == 0)
        return q;
    if(remainer != 0)
        return gcd(q,remainer);

}

int lcm(int p, int q)//The Least Common Multiple Number of p and q
{
    int remain;

    remain = (p*q)/gcd(p,q);
    return remain;

}

int main()
{
    int num1 = 0;
    int num2 = 0;
    int number=1;

    ifstream inputFile("input.txt", ios::in);//read the input.txt

    if(!inputFile)//if there isn't input.txt, it can't read it
    {
        cerr<<"File could not be opened~!!"<<endl;
        exit(1);
    }

    ofstream outputFile("A12-100502517-1.txt", ios::out);//write the A12-100502517-1.txt

    if(!outputFile)
    {
        cerr<<"File could not be opened~!!"<<endl;
        exit(1);
    }

    while(inputFile>>num1>>num2)//read the input.txt, and then write the answers on the A12-100502517-1.txt
    {
        outputFile << "第" << number << "組測資" << " = " << num1 << " " << num2 << endl
                   << "第" << number << "組測資" << " GCD = " << gcd( num1, num2 ) << endl
                   << "第" << number << "組測資" << " LCM = " << lcm( num1, num2 ) << endl << endl;
        number++;
    }

}
