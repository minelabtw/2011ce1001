#include<iostream>
#include<cstdlib>
#include<fstream>
using namespace std;
	
	int gcd(int number1,int number2)//declare the function to calculate Greatest commom divisor number of two integer
    {
        int gcdnumber;
        
        if(number1>number2)//compare two number to decide which one should be divided
        {
            gcdnumber=number1%number2;
            if(gcdnumber==0)
            {
                return number2;// return the last 餘數   
                
            }
            return gcd(number2,gcdnumber);//call gcd function to remain dividing
        }
        
        else//compare two number to decide which one should be divided
        {
            gcdnumber=number2%number1;
            if(gcdnumber==0)
            {
                return number1;// return the last 餘數   
            }
            return gcd(number1,gcdnumber);//call gcd function to remain dividing            
        }    
    }
    
    int lcm(int number1, int number2, int gcdnumber)//declare the function to calculate the Multiple number of two integer
    {
        
        return number1*number2/gcdnumber; //return the lcm number by using the formula   
    }
	
	
	
	
	int main()
	{
		
		ifstream inputData;//宣告寫入 
		ofstream outputData;//宣告寫出 
		inputData.open("input.txt", ios :: in);
		outputData.open("A12-100502509-1.txt", ios :: out);
		if(!inputData)//判斷失敗 
		{
			cerr<<"File could not be opened"<<endl;
			exit(1);	
		}
		int num1,num2,count=0;
		while(inputData>>num1>>num2)//判斷 
		{
			outputData<<"第"<<count+1<<"測試組"<<" = "<<num1<<" "<<num2<<endl;
			
			outputData<<"第"<<count+1<<"測試組"<<" GCD = "<<gcd(num1,num2)<<endl;	
			
			outputData<<"第"<<count+1<<"測試組"<<" LCM = "<<lcm(num1,num2,gcd(num1,num2))<<endl<<endl;
			count++;		
			
		}
		inputData.close();//關閉 
		outputData.close();//關閉 
	}
