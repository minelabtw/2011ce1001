#include <iostream>
#include <cstdlib>
#include <fstream>
using namespace std;

int gcd (int,int);
int lcm (int,int);
int main ()
{
	int counter = 1; // 計數第幾組測資
	ifstream readFile ("input.txt",ios::in);  // read "input.txt" file
	ofstream writeFile ("A12-100502024-1.txt",ios::out);  // write "A12-100502024-1.txt" file
	if (!readFile)  // 若錯誤顯示錯誤訊息
	{
		cerr << "File could not be opened!" << endl;
		exit (1);
	}

	if (!writeFile) // 若錯誤顯示錯誤訊息
	{
		cerr << "File could not be writed!" << endl;
		exit (1);
	}

	int number1,number2;
	while (readFile >> number1 >> number2) // 讀入檔案
	{
		writeFile << "第" << counter << "組測資 = " << number1 << " " << number2 << endl;  // 設定輸出時檔案的內容及排版
		writeFile << "第" << counter << "組測資 GCD = " << gcd (number1,number2) << endl;
		writeFile << "第" << counter << "組測資 LCM = " << lcm (number1,number2) << endl;
		writeFile << endl;
		counter++;  // 增加計數
	}
	writeFile.close ();
	return 0;
}

int gcd (int a, int b)  // 計算最大公因數的 function 
{
	int k1,k2=b;
	k1 = a%b;
	if (k1 > k2)
	{
		if (k1 == 0)
		{
			return k2;
		}
		else if (k2 == 0)
		{
			return k1;
		}
		else
		{
			k1 = gcd (k1,k2);
		}	
	}
	else if  (k1 < k2)
	{
		if (k1 == 0)
		{
			return k2;
		}
		else if (k2 == 0)
		{
			return k1;
		}
		else
		{
			k2 = gcd (k2,k1);
		}	
	}
}

int lcm (int p, int q)  // 計算最小公倍數的 function
{
	int i;
	i = p*q /gcd (p,q);
	return i;
}