#include<iostream>
#include<cstdlib>
#include<fstream>//file stream
using namespace std;
int gcd(int,int);//prototype
int lcm(int,int);//prototype
int main()//main function execute
{
    int first;
    int second;
    int counter=1;
    ifstream inClientFile("input.txt",ios::in);//ifstream constructor opens the file
    if(!inClientFile)//exit program if ifstream could not open file
    {
        cerr<<"File could not be opened"<<endl;
        exit(1);
    }//end if
    ofstream outClientFile("A12-100502508-1.txt",ios::out);
    if(!outClientFile)//exit program if ifstream could not open file
    {
        cerr<<"File could not be opened"<<endl;
        exit(1);
    }//end if
    while(inClientFile>>first>>second)
    {
        
        outClientFile<<"第"<<counter<<"組測資 = "<<first<<" "<<second<<endl;
        outClientFile<<"第"<<counter<<"組測資 GCD = "<<gcd(first,second)<<endl;//call gcd function
        outClientFile<<"第"<<counter<<"組測資 LCM = "<<lcm(first,second)<<endl;//call lcm function
        outClientFile<<endl;
        counter++;
    }//end while 
    inClientFile.close();
    outClientFile.close();
    return 0;
}//end main
int gcd(int firstnumber,int secondnumber)//Greatest Common Divisor number
{
    int answer;
    if(firstnumber>secondnumber)
    {
        answer=firstnumber%secondnumber;
        if(answer==0)//surplus number is equal zero
        {
            return secondnumber;
        }
        else if(answer!=0)//surplus number is not equal zero
        {
            firstnumber=secondnumber;//exchange
            secondnumber=answer;
            return gcd(firstnumber,secondnumber);//recursive
        }
    }
    else if(firstnumber<secondnumber)
    {
        answer=secondnumber%firstnumber;
        if(answer==0)//surplus number is equal zero
        {
            return firstnumber;
        }
        else if(answer!=0)//surplus number is not equal zero
        {
            secondnumber=firstnumber;//exchange
            firstnumber=answer;
            return gcd(firstnumber,secondnumber);//recursive
        }
    }
    else//firstnumber is equal secondnumber
    {
        return firstnumber;
    }
}//end gcd function
int lcm(int firstnumber2,int secondnumber2)//Least Common Multiple number
{
    return (firstnumber2*secondnumber2)/gcd(firstnumber2,secondnumber2);//call gcd function
}//end lcm function
