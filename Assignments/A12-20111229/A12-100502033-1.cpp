#include <iostream>
#include <fstream>
#include <cstdlib>
using namespace std;
int GCD(int , int);
int LCM(int , int);
void outputline(int , int);
int main()
{
    ifstream File("input.txt" , ios::in);
    if(!File)//如果文字檔不存在時運行
    {
        cerr << "File could not be opened" << endl;
        exit(1);
    }
    int number1 , number2 , number3 ,number4=0;
    while(File>>number1>>number2)//載入文字檔
    {
        number4++;
        cout <<"第" << number4<<"組數據 " ;
        outputline(number1,number2);//呼叫函數
        if(number1 < number2)
        {
            number3=number1; //使得第一個數比第二個數大
            number1=number2;
            number2=number3;
        }
        cout <<"第"<< number4<<"組數據 " << "GCD = " << GCD(number1,number2)<<endl;//呼叫函數
        cout <<"第"<< number4<<"組數據 " <<"LCM = "  << LCM(number1,number2)<<endl<<endl;//呼叫函數
    }
    system("pause");
}
void outputline(int number1, int number2)
{
    cout << number1 << " " << number2 << endl;//印出文字檔的資料
}

int GCD(int integer1,int integer2)
{
    if (integer1 % integer2 == 0)
    {
        return integer2 ;
    }
    else
    {
        return GCD(integer2,integer1 % integer2); //計算GCD的值
    }
}
int LCM(int integer1 , int integer2)
{
    return (integer1*integer2)/GCD(integer1,integer2) ; //計算LCM的值
}
