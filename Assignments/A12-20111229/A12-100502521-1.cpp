#include <iostream>
#include <fstream>
#include <cstdlib>
using namespace std;
int gcd(int p,int q)
{
	if(p==0)				//輾轉相除法 如果一項餘數為0 則另一項是最大公因數
	{
		return q; 
	}
	if(q==0)
	{
		return p;
	}
	if(p>q)					//第一次進來時會判斷   判斷兩者誰大
	{
		return gcd(p%q,q);
	}
	else
	{
		return gcd(p,q%p);
	}
}
int lcm(int p,int q)
{
	return abs(p*q)/gcd(p,q);			//套用公式
}
int main()
{
	int I_N1,I_N2;							//儲存兩個數字
	ifstream ReadFile("input.txt",ios::in);	//開啟要讀入的檔案
	if(!ReadFile)
	{
		cerr<<"檔案無法開啟!"<<endl;
		system("pause");
		exit(1);
	}
	ofstream OutputFile("A12-100502521-1.txt",ios::out);	//開啟要輸出的檔案
	if(!OutputFile)
	{
		cerr<<"檔案無法開啟!"<<endl;
		system("pause");
		exit(1);
	}
	for(int i=1;i<=3;i++)											//輸出3次的測資
	{
		ReadFile>>I_N1>>I_N2;
		OutputFile<<"第"<<i<<"組測資 = "<<I_N1<<" "<<I_N2<<endl;
		OutputFile<<"第"<<i<<"組測資 GCD = "<<gcd(I_N1,I_N2)<<endl;
		OutputFile<<"第"<<i<<"組測資 LCM = "<<lcm(I_N1,I_N2)<<endl;
		OutputFile<<endl;
	}
	ReadFile.close();			//關閉檔案
	OutputFile.close();
	return 0;
}