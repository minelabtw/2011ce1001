#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>

using namespace std;

int caculate_GCD( int, int );
int caculate_LCM( int, int );

int main()
{
    ifstream inClientFile( "input.txt", ios::in );// 選擇檔案讀取來源
    ofstream outClientFile( "A12-100502503-1.txt", ios::out );//建立或尋找檔案輸出目的

    if( !inClientFile )// 發生錯誤時顯示的訊息
    {
        cerr << "The file could not be opened!" << endl;
        exit(1);
    }

    else if( !outClientFile )
    {
        cerr << "The file could not be opened!" << endl;
        exit(1);
    }

    int num1, num2;

    for( int th=1; th<=3; th++ )// 顯示第幾組測資
    {
         while( inClientFile >> num1 >> num2 )// 讀取一組測資即輸出該組測資的數字，及運算結果至目的檔案
        {
            outClientFile << "\n\n第" << th << "組測資 = " << num1 << ' ' << num2
                 << "\n第" << th << "組測資 GCD = " << caculate_GCD( num1, num2 )
                 << "\n第" << th <<"組測資 LCM =" << caculate_LCM( num1, num2 ) << endl;
        }
    }
    inClientFile.close();
    outClientFile.close();
}


int caculate_GCD( int num1, int num2 )//輾轉相除法計算最大公因數
{
    if( num1%num2 == 0)//若兩數相除的餘數為零,則可得知其最大公因數為數字本身
        return num2;

    else
        return caculate_GCD( num2, num1%num2 );//若不為零，則繼續運算直至為零
}

int caculate_LCM( int num1, int num2 )//以公式計算最小公倍數
{
    return( (num1*num2)/caculate_GCD( num1, num2 ) );
}



