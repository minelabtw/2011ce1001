#include<iostream>
#include<cstdlib>
#include<iomanip>
#include<fstream>//file stream
#include<string>

using namespace std;

int gcd(int p, int q)//function gcd 
{
    if((p%q) == 0)
        return q;
    else 
        return gcd(q, p%q);
}

int lcm(int p, int q)//function lcm 
{
    return (p*q)/gcd(p,q);
}

int main()
{
    ofstream outClientFile("A12-100502031-1.txt",ios::out); //ofstream construcyor opens the file
    ifstream inClientFile("input.txt",ios::in); //ifstream construcyor opens the file
    if(!inClientFile)  
    {
     cerr<<"I Error"<<endl;
     system("pause");
     exit(1);
    }
    if(!outClientFile)
    {
     cerr<<"O Error"<<endl;
     system("pause");
     exit(1);
    }
    
    int input1,input2; //定義input1,input2 來代表測資的兩個數字 
    
    gcd(input1,input2); //caculate the gcd of tow numbers 
    lcm(input1,input2); //caculate the lcm of tow numbers
    
    int i=1; //用來改變輸出的第i次測資 
    //display each record in file
    while(inClientFile>>input1>>input2)
    {
     outClientFile<<"第"<<i<<"組測資 = "<<input1<<" "<<input2<<endl; 
     outClientFile<<"第"<<i<<"組測資 GCD ="<<gcd(input1,input2)<<endl;
     outClientFile<<"第"<<i<<"組測試 LCM ="<<lcm(input1,input2)<<endl;
     outClientFile<<endl; 
     i++;//用來改變輸出的第i次測資 ，每當完成一次時就加一。 
    }
    cout<<"完成您的指示"<<endl;
    system("pause");
    return 0;
}


     

