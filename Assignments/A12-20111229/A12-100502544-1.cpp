#include<iostream>
#include<fstream>//file stream
#include<cstdlib>
using namespace std;
int GCD(int,int);
int LCM(int,int);
int main()
{
	ifstream inputfile("input.txt", ios::in);//ifstream constructor opens the file
	if(!inputfile)
	{
		cerr<<"file could not be opened!" << endl;
		exit(1);//exit program if ifstream could not be open file
	}
	int num1,num2;
	int num=0;
	ofstream outinputfile("A12-100502544-1.txt",ios::out);//ofstream constructor opens the file
	if(!outinputfile)
	{
	    cerr<<"file could not be opened!" << endl;
		exit(1);//end if
	}
    while(inputfile>>num1>>num2)//show each record in file
	 {
	     num++;
        outinputfile <<"第"<<num<<"組測資 = "<< num1 << " " << num2 << " " << endl;
        outinputfile <<"第"<<num<<"組測資 GCD = "<< GCD(num1,num2) << endl;
        outinputfile <<"第"<<num<<"組測資 LCM = "<<LCM(num1,num2) << endl;
        outinputfile << endl;//show in the file
        cout <<"第"<<num<<"組測資 = "<< num1 << " " << num2 << " " << endl;
        cout <<"第"<<num<<"組測資 GCD = "<< GCD(num1,num2) << endl;
        cout <<"第"<<num<<"組測資 LCM = "<<LCM(num1,num2) << endl;//show in the small black
        cout << endl;
	 }
	system("pause");
	return 0;
}
int GCD(int a,int b)
{
	if(a%b==0)
		return b;
	else
		return GCD(b,a%b);
}
int LCM(int c,int d)
{
	return c*d/GCD(c,d);
}
