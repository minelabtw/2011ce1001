//A12-100502502-1.cpp
#include<iostream>
#include<fstream>
#include<cstdlib>
using namespace std;
int gcd( int p, int q )//計算最大公因數
{
    if ( p%q == 0 )
        return q;
    if ( p%q != 0 )
        return gcd( q, p%q );
}
int lcm( int p, int q )//計算最小公倍數
{
    return p * q / gcd( p, q );
}
int main()//function main 開始
{
    int num1;
    int num2;
    int i = 1;//從第一組測資開始
    ifstream inClientFile( "input.txt", ios::in );//讀檔
    if ( !inClientFile )
    {
        cerr << "Error" << endl;
        exit ( 1 );
    }
    ofstream outClientFile( "A12-100502502-1.txt", ios::out );//輸出檔案
    if ( !outClientFile )
    {
        cerr << "Error" << endl;
        exit ( 1 );
    }
    while ( inClientFile >> num1 >> num2 )//讀檔案的資料
    {
        outClientFile << "第" << i << "組測資" << " = " << num1 << " " << num2 << endl;
        if ( num2 > num1 )//若第二個數大於第一個數，就交換數值
        {
            int item = num1;
            num1 = num2;
            num2 = item;
        }
        outClientFile << "第" << i << "組測資" << " GCD = " << gcd( num1, num2 ) << endl
                      << "第" << i << "組測資" << " LCM = " << lcm( num1, num2 ) << endl << endl;
        i++;
    }
    system( "pause" );//按任一鍵繼續
    return 0;//結束
}//function main 結束
