#include <iostream>
#include <fstream>
#include <cstdlib>
using namespace std;

int gcd(int p,int q) //calculate the greatest common divisor number
{
    if(p%q==0)
    {
        return q;
    }
    else
    {
        return gcd(q,p%q);
    }
}

int lcm(int p,int q) //calculate the lowest common multiple number
{
    return (p*q)/gcd(p,q);
}

int main()
{
	ifstream inClientFile;
	inClientFile.open("input.txt" , ios::in); //open the file input
	if(!inClientFile) //error message
	{
		cerr << "The file can't be open.\n";
		exit(1);
	}
	int number1,number2;
	int countnumber=1; // being used to count how many times it should be
	while(inClientFile >> number1 >> number2)
	{
		cout << "第" << countnumber << "組測資 = " << number1 << " " << number2 << endl;
		cout << "第" << countnumber << "組測資 GCD = " << gcd(number1,number2) << endl;
		cout << "第" << countnumber << "組測資 LCM = " << lcm(number1,number2) << endl;
		countnumber++;
		cout << endl;
	}
	inClientFile.close(); //close the file
	system("pause");
	return 0;
}
