#include<iostream>
#include<fstream>
#include<string>

using namespace std;

int GCD(int,int);
int LCM(int,int);

int main()
{
    ifstream InputFile("input.txt");//read the data from file
    ofstream OutputFile("A12-993002517-1.txt");//palce the data in file
    if(!InputFile)
    {//check if the file could be opened
        cerr<<"file could not be opened"<<endl;
        exit(1);
    }
    int num1,num2,count=1;
    while(InputFile>>num1>>num2)//read the data from file
    {
        OutputFile<<"第"<<count<<"組測資 = "<<num1<<" "<<num2<<endl//place data in the file
                  <<"第"<<count<<"組測資 GCD = "<<GCD(num1,num2)<<endl
                  <<"第"<<count<<"組測資 LCM = "<<LCM(num1,num2)<<endl<<endl;
        count++;
    }
    return 0;
}
int GCD(int num1,int num2)//calculate GCD
{
    if(num1%num2==0)
        return abs(num2);
    else if(num2%num1!=0)
        return abs(GCD(num2,num1%num2));
}
int LCM(int num1,int num2)//calculate LCM
{
    return abs(num1*num2/GCD(num1,num2));
}
