#include <cstdlib>
#include <iostream>
#include <fstream>
using namespace std;

int gcd(int,int);
int lcm(int,int);

int main(void){
	ifstream test("input.txt", ios::in); // open input.txt
	if(!test){ //test the file is opened or not
		cerr << "File could not be opened" << endl;
		exit(1);
	}
	ofstream answer("A12-993002004-1.txt", ios::out); // create a file
	if(!answer){ // test the file is opened or not
		cerr << "File could not be opened" << endl;
		exit(1);
	}

	int num1, num2;

	for(int i = 1; i <= 3; i++){
		test >> num1 >> num2; // get data, calculate and output in the file
		answer << "第" << i << "組測資" << " = " << num1 << " " << num2 << endl;
		answer << "第" << i << "組測資 GCD" << " = " << gcd(num1, num2) << endl;
		answer << "第" << i << "組測資 LCM" << " = " << lcm(num1, num2) << endl;
		answer << endl;
	}

	test.close(); // close files
	answer.close();

	return 0;
}

int gcd(int p, int q){ // use the functions in Q3
	if(q > p){ // let p > q
		int hold = p;
		p = q;
		q = hold;
	}
	p = p % q; // 輾轉相除法
	if(p == 0)
		return q;
	else
		return gcd(q,p);
}

int lcm(int p, int q){
	return p * q / gcd(p,q);
}
