#include <iostream>
#include <cstdlib>
#include <fstream>
using namespace std;

char* filein ="input.txt";
char* fileout="A12-100502520-1.txt";
int gcd(int numb1,int numb2)
{
	int transfer;
	if(numb2>numb1)  //確認前面大於後面 
	{
		transfer=numb1;
		numb1=numb2;
		numb2=transfer;
	}
	if(numb1%numb2==0)  //遞迴的停止條件 
	{
		return numb2;
	}
	else
	{
		return gcd(numb2,(numb1%numb2));
	}
}

int lcm(int numb1,int numb2)
{
	return (numb1*numb2/gcd(numb1,numb2));
}

int main()
{
    fstream fileinput;
	fstream fileoutput;
	int number=1;
	int store[2]={};
    fileinput.open(filein,ios::in);
    if(fileinput.fail())  //判斷讀檔是否失敗 
    {
		cout<<"開啟失敗"<<endl;
		system ("pause");
		return 0;

    }

	fileoutput.open("A12-100502520-1.txt",ios::out);

	while(!fileinput.eof())
	{
		fileinput>>store[0]>>store[1];  //讀取資料 
		
		//計算並寫入資料 
		fileoutput<<"第"<<number<<"組測資  "<<store[0]<<"  "<<store[1]<<endl;
		fileoutput<<"第"<<number<<"組測資 gcd  "<<gcd(store[0],store[1])<<endl;
		fileoutput<<"第"<<number<<"組測資 lcm  "<<lcm(store[0],store[1])<<endl<<endl;

		number++;
	}
	fileinput.close();  //關閉檔案 
    fileoutput.close();
    system ("pause");
    return 0;
}
