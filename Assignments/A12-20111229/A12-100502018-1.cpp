#include <iostream>
#include <cstdlib>
#include <fstream>
using namespace std;

int lcm( int, int );
int	gcd( int, int );

int main()
{
	ifstream inClientFile( "input.txt", ios::in ); //open a file named input
	ofstream outClientFile( "A12-100502018-1.txt", ios::out ); // create a file named A12-100502018
	
	if ( !inClientFile ) //if the opening failed
	{
		cerr << "File could not be opened" << endl; //opening error message
		system("pause");
		exit( 1 ); //quit from the execution
	}

	int number1,number2,count=1;

	while ( inClientFile >> number1 >> number2 ) //if the opening successful
	{
		outClientFile << "第" << count << "組測資 = " << number1 << ' ' << number2 << endl
			<< "第" << count << "組測資 GCD = " << gcd( number1, number2 ) << endl
			<< "第" << count << "組測資 LCM = " << lcm( number1, number2 ) << endl << endl;
		count++;
	}

	if( !outClientFile ) //if the creating failed
	{
		cerr << "File could not be created" << endl; //creating erro message
		system("pause");
		exit( 1 ); //quit from the execution
	}

	return 0;
}

int lcm( int x, int y )
{
	return x * y / gcd( x, y );
}
int gcd( int x, int y) //use the recursion to calculate the gcd of x and y
{
	int remainder;
	if( x == y )
		return x;
	else if( x >= y )
	{
		remainder = x % y;
		if( remainder != 0 )
			gcd( y, remainder );
		else
			return y;
	}
	else
	{
		remainder = y % x;
		if( remainder != 0 )
			gcd( x, remainder );
		else
			return x;
	}
}