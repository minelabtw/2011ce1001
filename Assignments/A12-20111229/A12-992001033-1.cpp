#include<iostream>
#include<cstdlib>//cerr需要使用
#include<fstream>
using namespace std;

int gcd(int p,int q);
int lcm(int p,int q);

int main()
{
    ifstream inInput("input.txt", ios::in );
    ofstream outInput("A12-992001033-1.txt", ios::out);
    if(!inInput||!outInput)//如果不能讀檔或建檔則發生錯誤
    {
        cerr<<"File could not be opened"<<endl;
        exit(1);
    }
    int first=0;//第一個數字
    int second=0;//第二個數字
    int count=1;//第幾組測資
    while(inInput>>first>>second)
    {
        outInput<<"第"<<count<<"組測資 = "<<first<<' '<<second<<endl;
        outInput<<"第"<<count<<"組測資 GCD = "<<gcd(first,second)<<endl;
        outInput<<"第"<<count<<"組測資 LCM = "<<lcm(first,second)<<endl<<endl;
        count++;//每讀完一行加一
    }
    inInput.close();
    outInput.close();
    return 0;
}

int gcd(int p,int q)//利用輾轉相除得最大公因數
{
    if(p%q==0 || q%p==0)//當其中有一個被整除，則結束
    {
        if(p>q)
            return q;
        else
            return p;
    }
    if (p>q)
        return gcd(p%q,q);
    else
        return gcd(p,q%p);
}
int lcm(int p,int q)
{
    return  (p*q)/gcd(p,q);//根據公式
}
