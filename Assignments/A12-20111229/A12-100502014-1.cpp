#include<iostream>
#include<cstdlib>
#include<fstream>
using namespace std;
int gcd(int p, int q)//最大公因數 
{
    if(p==0)
        return q;
    else if(q==0)
        return p;
    else if(p>=q)
        return gcd(p%=q,q);
    else
        return gcd(p,q%=p);
}
int lcm(int p, int q)//最小公倍數 
{
    return (p*q)/gcd(p,q);
}
int main()
{
    ifstream inFile("input.txt",ios::in);//讀檔 
    if(!inFile)//開啟檔案錯誤 
    {
        cerr << "Error!!" << endl;
        exit(1);
    }
    ofstream outFile("A12-100502014-1.txt",ios::app);//開新檔案 
    if(!outFile)//開新檔案錯誤 
    {
        cerr << "Error!!" << endl;
        exit(1);
    }
    int num1, num2, i=1;
    while(inFile >> num1 >> num2)//寫入
    {
        outFile << "第" << i << "組測資 = " << num1 << " " << num2 << endl;
        outFile << "第" << i << "組測資 GCD = " << gcd(num1,num2) << endl;
        outFile << "第" << i << "組測資 LCM = " << lcm(num1,num2) << "\n" << endl;
        i++;
    }
    cout << "Successfully write in!!!" << endl; 
    inFile.close();//關掉input.txt 
    outFile.close();//關掉A12-10050214-1.txt 
    system("pause");
    return 0;
}
