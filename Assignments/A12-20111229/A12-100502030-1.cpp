#include<iostream>
#include<cstdlib>
#include<fstream>
using namespace std;

int gcd(int p,int q)
{
    if(p<q)
    {
        int x;
        x=p;
        p=q;
        q=x;
    }
    if(p%q==0)
        return q;
    else
        return gcd(p%q,q);
}// 計算出兩數之公因數

int lcm(int p,int q)
{
    return p*q/gcd(p,q);
}// 計算出兩數之公倍數


int main()
{
    int number1,number2;

    ifstream inFile("input.txt",ios::in);// ifstream constructor opens the file
    if(!inFile)
    {
        cerr<<"File could not opened"<<endl;
        exit(1);
    }// exit program if could not open file

    ofstream outFile("A12-100502030-1.txt",ios::out);// ofstream constructor opens file
    if(!outFile)
    {
        cerr<<"File could not opened"<<endl;
        exit(1);
    }// exit program if could creat file

    int x=1;
    while(inFile>>number1>>number2)
    {
        outFile<<"第"<<x<<"組測資 = "<<number1<<" "<<number2<<endl
               <<"第"<<x<<"組測資 GCD = "<<gcd(number1,number2)<<endl
               <<"第"<<x<<"組測資 LCM = "<<lcm(number1,number2)<<endl<<endl;
        x++;
    }// 將讀取的資料和其公因數、公倍數的結果寫入另一檔案
}
