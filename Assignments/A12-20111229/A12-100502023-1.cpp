#include <iostream>
#include <cstdlib>
#include <fstream>
using namespace std;

int remaining;//餘數

int gcd(int p,int q)  //calculate the greatest common divisor number of p and q
{
	if (p>q)
	{
		remaining=p%q;

		if (remaining==0)
		{
			return q;
		}

		else
		{
			return gcd(q,remaining);
		}
	}

	else if (q>p)
	{
		remaining=q%p;

		if (remaining==0)
		{
			return p;
		}

		else
		{
			return gcd(p,remaining);
		}
	}
	
}

int lcm(int p,int q)  //calculate the least common multiple number of p and q
{
	int answer;

	if (p*q>0)
	{
		answer=p*q/gcd(p,q);
		return answer;
	}
	
}

int main()
{
	ifstream inNumberFile("input.txt",ios::in);
	ofstream outNumberFile("A12-100502023-1.txt",ios::out);

	if (!inNumberFile)
	{
		cerr<<"File could not be open.";
		exit(1);
	}

	if (!outNumberFile)
	{
		cerr<<"File could not be open.";
		exit(1);
	}

	int firstNumber;
	int secondNumber;
	int i=1;  //counter

	while(inNumberFile>>firstNumber>>secondNumber)
	{
		outNumberFile<<"第"<<i<<"組測資"<<" = "<<firstNumber<<" "<<secondNumber<<"\n";
		outNumberFile<<"第"<<i<<"組測資"<<" GCD = "<<gcd(firstNumber,secondNumber)<<"\n";
		outNumberFile<<"第"<<i<<"組測資"<<" LCM = "<<lcm(firstNumber,secondNumber)<<"\n\n";
		i+=1;
	}

	outNumberFile.close();

	system("pause");
	return 0;
}