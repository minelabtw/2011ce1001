#include <iostream>
#include <fstream>
#include <cstdlib>
using namespace std;

// prototype for functions
int GCD(int, int);
int LCM(int, int);
void swap(int * const, int * const);

int main()
{
    // constructor of ifstream & ofstream open files
    ifstream inInputFile("input.txt", ios::in);
    ofstream outOutputFile("A12-100502003-1.txt", ios::out);
    if(!inInputFile) // can't find the file
    {
        cerr << "File couldn't be opened!!" << endl;
        exit(1);
    }

    if(!outOutputFile) // can't save the file
    {
        cerr << "File couldn't be created!!" << endl;
        exit(1);
    }

    // declare variables that is going to be used
    int first_num;
    int second_num;
    int order=1;

    while(inInputFile >> first_num >> second_num) // read values from the inInputFile
    {
        if(first_num<second_num) // consider which number is bigger and will be used as the first number in function GCD
        {
            swap(&first_num, &second_num); // change the order to correspond to functon GCD's condition
        }
        outOutputFile << "第" << order << "組測資 = " << first_num << " " << second_num << endl;
        outOutputFile << "第" << order << "組測資 " << "GCD = " << GCD(first_num, second_num) << endl;
        outOutputFile << "第" << order << "組測資 " << "LCM = " << LCM(first_num, second_num) << endl;
        outOutputFile << endl;
        order++;
    }

    system("pause");
    return 0;
}

int GCD(int num1, int num2)
{
    if(num1%num2==0) // if num1 is a multiple of num2, then GCD is num2
        return num2;
    else
        return GCD(num2, num1%num2); // use recursive way to count
}

int LCM(int value1, int value2)
{
    return value1*value2/GCD(value1, value2);
}

void swap(int * const num1ptr, int * const num2ptr) // change two numbers
{
    int helper;
    helper = *num1ptr;
    *num1ptr = *num2ptr;
    *num2ptr = helper;
}
