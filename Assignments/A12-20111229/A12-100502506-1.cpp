#include<iostream>
#include<fstream>
#include<iomanip>
#include<string>
#include<cstdlib>
using namespace std;
int gcd(int p,int q)//最大公因數函式 
{
	if(p==0)
    {
		return q;
	}
	if(q==0)
	{
		return p;
	}
	if(p>q)
	{
		return gcd(p%q,q);
	}
	else if(q>p)
	{ 
		return gcd(p,q%p);
	}
}
int lcm(int p,int q)//最小公倍數函式 
{
	return abs(p*q)/gcd(p,q);	
}
int main()
{      
	int test1,test2; //兩組數字 
	int times=1; 
	 
	ifstream inFile("input.txt",ios::in);//讀input.txt檔 
	ofstream outFile("A12-100502506-1.txt",ios::out);//輸出A12-100502506-1.txt 
	if(!inFile)//無法讀檔情況 
	{
        cerr<<"File could not be opened"<<endl;
        system("pause");
        exit(1); 
	}
	if(!outFile)//無法輸出情況 
	{
		cerr<<"File could not be opened"<<endl;
		system("pause");
		exit(1); 
 	} 	 
	while(inFile>>test1>>test2)//讀 input.txt檔中 兩個數字  
	{
		//輸出在A12-100502506-1.txt中 
	    outFile<<"第"<<times<<"組測資 = "<<test1<<" "<<test2<<endl;
        outFile<<"第"<<times<<"組測資 GCD = "<<gcd(test1,test2)<<endl;
        outFile<<"第"<<times<<"組測資 LCM = "<<lcm(test1,test2)<<endl<<endl; 
        times+=1;
	}	
	inFile.close();//關檔 
	outFile.close();//關檔 
    return 0;
	
}
