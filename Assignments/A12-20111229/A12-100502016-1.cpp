#include<iostream>
#include<cstdlib>
#include<fstream>
using namespace std;

int gcd(int,int);
int lcm(int,int);

int main()
{
	ifstream inputfile("input.txt",ios::in); //讀取名為 input 的文字檔案 
	if(!inputfile)	//例外處理 當找不到檔案或無法開啟則出現錯誤訊息 
	{
		cerr << "File cannot be opened" << endl;
        exit(1);	
	}
	
	ofstream outputfile("A12-100502016.txt",ios::out); //產生名為 A12-100502016 的文字檔案 
	if(!outputfile)  
	{
		cerr << "File cannot be created." << endl;
        exit(1);	
	}
	
	int data1,data2;  
	int i=1;   
	
	while(inputfile>>data1>>data2)
	{
		outputfile<<"第"<<i<<"組測資 = "<<data1<<' '<<data2<<endl    //input讀取的資料經函數 
		    	  <<"第"<<i<<"組測資 GCD = "<<gcd(data1,data2)<<endl //後放入A12-100502016檔案內 
		    	  <<"第"<<i<<"組測資 LCM = "<<lcm(data1,data2)<<endl
				  <<endl;
		i++;
	}	 
	
	cout<<"檔案已產生..."<<endl;
    system("pause");
    return 0;
}
int gcd(int n1,int n2)  //recursive gcd function
{
	if(n1%n2!=0)
		return gcd(n2,n1%n2);
	else 
	    return n2;
}
int lcm(int n1,int n2)
{
	return n1*n2/gcd(n1,n2);
}
