#include<iostream>
#include<cstdlib>
#include<fstream>
using namespace std;

int gcd(int,int);//計算最大公因數之FUNCTION
int lcm(int,int);//計算最小公倍數之FUNCTIOM
int number1,number2;//兩個比較的數
int count=1;//計數用的variable

int main()
{
    ifstream input("input.txt",ios::in); //開啟測資檔案
    ofstream output("A12-100502011-1.txt",ios::out);//寫入結果檔案
    if(!input)
    {
        cerr << "This file  can not be opened!";
        exit(1);
    }

    while(input>> number1 >> number2)//將值傳入結果檔案
    {

        output<< "第" <<count << "組測資 = " << number1 << " " << number2<<endl
            <<"第" << count <<"組測資 GCD = " << gcd(number1,number2)<<endl
            <<"第" << count <<"組測資 LCM = " << lcm(number1,number2)<<endl<<endl;
        count++;
    }
    output.close(); //關閉檔案
    return 0;
}

int gcd(int number1,int number2)//計算最大公因數之FUNCTION
{
    if(number1%number2==0)
        return number2;
    else
        return gcd(number2,number1%number2);
}

int lcm(int number1,int number2)//計算最小公倍數之FUNCTIOM
{
    return number1*number2/gcd(number1,number2);
}
