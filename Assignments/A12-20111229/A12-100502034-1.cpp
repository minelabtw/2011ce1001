#include <iostream>
#include <fstream>
#include <cstdlib>
using namespace std;
int gcd (int ,int);//計算gcd
int lcm (int ,int);//計算lcm
void displayM(int);//每行都會出現的對白:第?組測資為
int main()
{
    int number=0;//用來放第?組的?
    int firstNo=0,secondNo=0;//就是第一跟第二個數字嘛
    //int hold;<~ 本來以為要用
    ifstream inputFile("input.txt",ios::in);//inputfunction
    if (!inputFile)//"只要關係到檔案的就要判斷錯誤"
    {
        cerr << "檔案不能開啟"<<endl; //第一次按F9我就看到這個訊息(因為還沒有開一個TXT出來)
        exit(1);
    }
    while (inputFile >> firstNo >> secondNo)
    {
        /*if (firstNo<secondNo)
        {
            hole = firstNo;
            firstNo = second;
            second = hold;
        }*///這裡哦...我發現比較少的數餘除比較大的數的時候...在funtion裡面做的跟這裡做的一樣(例如5%6=5 然後return gcm(6,5) (就是多餘嘛
        number++;
        if (firstNo>0&&secondNo>0)//判斷是否成立
        {
            displayM(number); //每行都會出現的訊息
            cout << " = "<<firstNo<<" "<<secondNo<<endl;
            displayM(number);
            cout << " GCD = "<<gcd(firstNo, secondNo)<<endl;//gcd
            displayM(number);
            cout << " LCM = "<<lcm(firstNo, secondNo)<<endl;//lcm
        }
        else
        {
            cout << "ERROR\n";
            if (firstNo<=0)
            {
                displayM(number);
                cout << "含非正數: "<<firstNo<<endl;//顯示哪個數字錯誤
            }
            if (secondNo<=0)
            {
                displayM(number);
                cout << "含非正數: "<<secondNo<<endl;//顯示哪個數字錯誤
            }
        }
        cout << endl;//依照題目, 每組測資之間會空一行
    }
    return 0;
}
int gcd ( int no1, int no2)//公式啦
{
    if ( no1 %no2 == 0)
        return no2;
    else
        return gcd (no2, no1%no2);
}
int lcm (int lcmNo1,int lcmNo2)//也是公式啦
{
    return lcmNo1*lcmNo2/gcd(lcmNo1,lcmNo2);
}
void displayM(int disnumber)//每行都會出現的cout
{
    cout << "第"<< disnumber<<"組測資 ";
}
