#include <iostream>
#include <cstdlib>
#include <fstream> //the file stream
#include <string>

using namespace std;

int gcd ( int, int ); //function gcd prototype.
int lcm ( int, int ); //function lcm prototype.

int main( )
{
    ifstream ininputfile ( "input.txt", ios::in );

    if ( !ininputfile )
    {
        cerr << "File cannot be opened or does not exist." << endl;
        exit ( 1 );//Error message if the file cannot be opened or does not exist.
    }

    ofstream outa12file ( "A12-100502515.txt", ios::out );

        if ( !outa12file )
        {
            cerr << "File cannot be created." << endl;
            exit ( 1 );//Error message if the file cannot be created.
        }

    int no1, no2, LCM, GCD, count = 1;

    while ( ininputfile >> no1 >> no2 )
    {
        GCD = gcd ( no1, no2 );//GCD is calculated here.
        LCM = lcm ( no1, no2 );//LCM is calculated here.

        outa12file << "第" << count << "組測資 = "    << no1 << ' ' << no2 << endl
                  << "第" << count << "組測資 GCD = " << GCD << endl
                  << "第" << count << "組測資 LCM = " << LCM << endl
                  << endl;//save in the A12-100502515.txt.

        count++;//integer count means the order the group of the data.

    }

    return 0;
}

int gcd ( int num1, int num2 )//the Euclidean Algorithm.
{
    if ( num1 == 0 )
        return num2;
    else if ( num2 == 0 )
        return num1;
    else if ( num1 >= num2 )
   {
        num1 %= num2;
        return gcd ( num1, num2 );
    }
    else if ( num2 >= num1 )
    {
        num2 %= num1;
        return gcd ( num1, num2 );
    }
}

int lcm ( int num1, int num2 )//The lcm is to result of the mu;tiple of the two numbers divide by the gcd.
{
    return num1 * num2 / gcd ( num1, num2 );
}
