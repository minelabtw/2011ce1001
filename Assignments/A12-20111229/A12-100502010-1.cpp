#include<iostream>
#include<fstream>
#include<cstdlib>
using namespace std;
int number1,number2;
int gcd(int,int);
int lcm(int,int);
int counter=1;

int main()
{
    ifstream readin( "input.txt", ios::in );//開啟檔案input.txt 
    ofstream output( "A-12-100502010-1.txt", ios::out );//將資料傳出至A-12-100502010-1.txt
    if(!readin)//若開啟失敗 
    {
        cerr<<"could not open the file"<<endl;
        exit(1);
    }
    while(readin>>number1>>number2)//一行一行輸出 
    {
        output<<"第"<<counter<<"組測資 "<<"= "<<number1<<" "<<number2<<endl; 
        output<<"第"<<counter<<"組測資"<<" GCD = "<<gcd(number1,number2)<<endl;
        output<<"第"<<counter<<"組測資"<<" LCM = "<<lcm(number1,number2)<<endl;
        output<<endl;
        counter=counter+1;
    }
    output.close();
    return 0;
}

int gcd(int num1,int num2)//計算最大公因數 
{
    int bigtemp;
    int smalltemp;
    if(num1>num2)//按大小排好順序 
    {
        bigtemp=num1;
        smalltemp=num2;
    }
    else if(num1<num2)//按大小排好順序
    {
        bigtemp=num2;
        smalltemp=num1;
    }
    int temp=bigtemp%smalltemp;
    bigtemp=smalltemp;
    smalltemp=temp;
    
    if(temp==0)
    {
        return bigtemp;
    }
    else if(temp!=0)
    {
        return gcd(bigtemp,smalltemp);//若還未算出答案傳入遞迴 
    }
    
}
int lcm(int num1,int num2)//計算最小公倍數 
{
    int temp1;
    int answer;
    temp1=gcd(num1,num2);
    
    answer=(num1*num2)/temp1;
    
    
} 

