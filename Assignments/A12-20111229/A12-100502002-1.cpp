#include<iostream>
#include<fstream>
#include<cstdlib>
using namespace std;
int gcd(int a, int b)
{
    if((a%b)==0)
        return b;//如果餘除結果是0,結果就是b 
    else
        return gcd(b,a%b);//如果餘除的結果不是0, 就繼續除 
}//用遞迴寫最大公因數,上次小考沒寫出來orz 

int lcm(int a, int b)
{
    return (a*b)/gcd(a,b);//用最小公倍數的公式算最小公倍數 
}
int main()
{
    ifstream input("input.txt",ios::in);//讀input的資料 
     
    if(!input)//如果不能讀就輸出錯誤訊息 
    {
        cerr<<"file could not be opened"<<endl;
        exit(1);
    }

    ofstream output("A12-100502002-1.txt",ios::out);//寫入input的資料 
    
    if(!output)//如果不能讀就輸出錯誤訊息
    {
        cerr<<"file could not be opened"<<endl;
        exit(1);
    }
    
    int a,b,i;
    i=1;
    while(input>>a>>b)//寫入結果
    {
        gcd(a,b);
        lcm(a,b);
        output<<"第"<<i<<"組測資 = "<<" "<<a<<" "<<b<<endl;
        output<<"第"<<i<<"組測資 gcd = "<<" "<<gcd(a,b)<<endl;
        output<<"第"<<i<<"組測資 lcm = "<<" "<<lcm(a,b)<<endl;
        output<<endl;
        i++;
    }
    
    

    system("pause");
    return 0;
}
