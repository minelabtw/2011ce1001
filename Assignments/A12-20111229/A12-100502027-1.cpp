#include<iostream>
#include<cstdlib>
#include<fstream>
using namespace std;

int gcd(int ,int );
int lcm(int ,int );

int main()
{
    int times=1;
    ifstream infiledatas("input.txt",ios::in); //設定所讀取的文件
    if(!infiledatas)
    {
        cerr<<"File could not be opened"<<endl;
        exit(1);
    }
    ofstream outfiledatas("A12-100502027-1.txt",ios::out);//設定所記錄成的文件
    if(!outfiledatas)
    {
        cerr<<"File could not be opened"<<endl;
        exit(1);
    }
    int number1=0,number2=0;
    while(infiledatas>>number1>>number2) //將讀取到的數字帶入變數
    {
        outfiledatas<<"第"<<times<<"組測資 = "<<number1<<" "<<number2<<endl;
        outfiledatas<<"第"<<times<<"組測資 GCD = "<<gcd(number1,number2)<<endl;
        outfiledatas<<"第"<<times<<"組測資 LCM = "<<lcm(number1,number2)<<endl<<endl;
        times++;
    }
    infiledatas.close();//關閉功能，釋放記憶體
    outfiledatas.close();
    system("pause");
    return 0 ;

}

int gcd(int p,int q)
{
    if(p>=q)//to chose the Bigger number
    {
        if(p%q!=0)//to count %  == or != 0
        {
            return gcd(q,p%q);
        }
        else
        {
            return q;
        }
    }
    else
    {
        if(q%p!=0)
        {
            return gcd(p,q%p);
        }
        else
        {
            return p;
        }
    }
}

int lcm(int p,int q)
{
    return p*q/gcd(p,q);
}
