#include<iostream>
#include<cstdlib>
#include<fstream>
using namespace std;

int gcd(int num1,int num2)//compute gcd
{
    if(num2==0)
    {
        return num1;
    }
    else
    {
        return gcd(num2,num1%num2);
    }
}

int lcm(int gcdnum,int num1,int num2)//compute lcm
{
    return num1*num2/gcdnum;
}

int main()
{
    int num1,num2,count=1;
    ifstream in("input.txt",ios::in);//load data from input.txt
    ofstream out("A12-100502008-1.txt",ios::out);//save data as A12-100502008-1.txt
    while(in>>num1>>num2)//check if there is data to load or not
    {//pass data to A12-100502008-1.txt
        out<<"第"<<count<<"組測資 = "<<num1<<' '<<num2<<endl;
        out<<"第"<<count<<"組測資 GCD = "<<gcd(num1,num2)<<endl;
        out<<"第"<<count<<"組測試 LCM = "<<lcm(gcd(num1,num2),num1,num2)<<endl<<endl;
        count++;
    }
    out.close();//close txt
    system("pause");
    return 0;
}
