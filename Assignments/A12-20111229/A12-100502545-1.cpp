#include<iostream>
#include<cstdlib>
#include<fstream>
using namespace std;


 int gcd(int p,int q)
   {//gcd最大公因數 
       if(p>q)	//輸入第一個數大於第二個數,取餘數為0則回傳q
    	{
    		if(p%q==0)
    		{
    			return q;
    		} 
    		else if(p%q!=0)
    		{
    			return gcd(q,(p%q));
    		}
    		
    		
    	}
    	
    	else if(q>p)//反之
    	{
    		if(q%p==0)
    		{
    			return p;
    		} 
    		else if(q%p!=0)
    		{
    			return gcd((q%p),q);
    		}
    	
    	}
    	
    	else
    	{
    		return p;
    	}

    }
    
    int lcm(int p,int q)//lcm最小公倍數 
    {
	
	   return p*q/gcd(p,q);
    }


int main()
{
    ifstream inComeFile;//宣告寫入File 
    ofstream outComeFile;//宣告寫出File 
    inComeFile.open("input.txt",ios::in);//把input.txt  OPEN 
    outComeFile.open("A12-100502545-1.txt",ios::out);//把A12-100502545-1.txt  OPEN 
    
    if(!inComeFile)//check File 是否存在 
    {
        cerr<<"File could not be opened"<<endl;
        exit(1);
    }
    
    int number1,number2;
    int i=1;
    while(inComeFile>>number1>>number2)//兩變數寫入 
    {
        //寫出至A12-100502545-1.txt 
        outComeFile<<"第"<<i<<"組測資 = "<<number1<<" "<<number2<<endl; 
        outComeFile<<"第"<<i<<"組測資 GCD = "<<gcd(number1,number2)<<endl;
        outComeFile<<"第"<<i<<"組測試 LCM = "<<lcm(number1,number2)<<endl<<endl;
        i++;
    }
  
    inComeFile.close();//Close data!! 
    outComeFile.close();
    
}
