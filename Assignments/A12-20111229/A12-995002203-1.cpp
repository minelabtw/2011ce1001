#include<iostream>
#include<fstream>
#include<cstdlib>
#include<iomanip>
using namespace std;

int gcd(int p, int q)                    //求最大公倍數
{
    if(p>q)
    {
        if(p%q==0)
            return q;
        else
            return gcd(q, p%q);
    }
    else
    {
        if(q%p==0)
            return p;
        else
            return gcd(p, q%p);
    }
}

int lcm(int p, int q)                    //求最小公因數
{
    return (p*q)/gcd(p,q);
}

int main()
{
    int num1, num2, count=1;
    ofstream fout("A12-995002203-1.txt");//不用指定mode，因為ofstream的default就是open for output
    ifstream fin("input.txt");           //不用指定mode，因為ifstream的default就是open for input
    if(!fin)                             //回傳false表open operation failed
    {
        cout<<"open file error\n";
        exit(999);
    }
    while(fin>>num1>>num2)               //將input file中的資料讀出 存到變數中
    {
        fout<<"第"<<count<<"組測資 =  "<<setw(4)<<num1<<setw(4)<<num2<<endl;
        fout<<"第"<<count<<"組測資 GCD = "<<setw(5)<<gcd(num1,num2)<<endl;
        fout<<"第"<<count<<"組測資 LCM = "<<setw(5)<<lcm(num1,num2)<<endl<<endl;
        count++;
    }
    //fin.close();                      //destructor會自己關
    //fout.close();
    system("pause");
    return 0;
}

