#include<iostream>
#include<cstdlib>
#include<string>
#include<fstream>

using namespace std;

int gcd(int,int);//prototype
int lcm(int,int);//prototype

int main()
{
    int num1,num2;
    int add = 1;
    ifstream inmyfile("input.txt",ios::in);//call input txt
    if(!inmyfile)//除錯
    {
        cerr << "File could not be opened" << endl;
        exit(1);
    }
    ofstream outmyfile ( "A12-100502020.txt", ios::out );//show a file named
    if(!outmyfile)//除錯
    {
        cerr << "File could not be opened" << endl;
        exit(1);
    }
    while(inmyfile>>num1>>num2)
    {
        if(num2>num1)//change num1, num2 if num2>num1
        {
            int c = num1;
            num1 = num2;
            num2 = c;
        }
        outmyfile << "第" << add << "組測資 = " << num1 << ' ' << num2 <<endl;
        outmyfile << "第" << add << "組測資GCD = " << gcd(num1,num2) << endl;
        outmyfile << "第" << add << "組測資LCM = " << lcm(num1,num2) << endl << endl;//save those in A12-100502020.txt
        add++;//count
    }
    system("pause");
    return 0;
}


int gcd(int P,int Q)
{
    if(P%Q==0)
    {
        return Q;//last number b4 0
    }
    else
        return gcd(Q,P%Q);//keep doing gcd function
}
int lcm(int P,int Q)
{
    return (P*Q)/gcd(P,Q);//calculate lcm
}
