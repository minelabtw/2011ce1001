#include<iostream>
#include<cstdlib>
#include<fstream>
using namespace std;

int GCD(int a1,int a2){
    if(a2==0)
        return a1;
    else
        return GCD(a2,a1%a2);
}

int LCM(int a1,int a2){
    return a1*a2/GCD(a1,a2);

}


int main(){
    int a1,a2,temp; //a1 a2存取每行第1和第2個數字咩
    ifstream in("input.txt");   //開檔咩
    if(!in){    //如果檔案不存在咩
        cout<<"找無檔案 掰"<<endl;
        system("pause");
        return 0;
    }
    ofstream out("A12-985002039-1.txt");    //寫檔咩
    for(int i=1;i<=3;i++){  //計算GCD和LCM並輸出咩
        in>>a1>>a2;
        if(a2>a1){temp=a1;a1=a2;a2=temp;}
        out<<"第"<<i<<"組測資 = "<<a1<<" "<<a2<<endl;
        out<<"第"<<i<<"組測資GCD = "<<GCD(a1,a2)<<endl;
        out<<"第"<<i<<"組測資LCM = "<<LCM(a1,a2)<<endl<<endl;
    }
    //system("pause");
    return 0;
}
