#include<iostream>
#include<cstdlib>
#include<cmath>
using namespace std;

double pi=3.14;//global variable

double Pi_local()//精確的pi
{
	double b,c;
	c=0;

	for(int a=0;a<=100;a++)//跑100次
	{
		switch(a%2)//用2的餘數來判斷奇偶
		{
			case 0:
			b=(double)4/(2*a+1);//型別轉換
			break;

			case 1:
			b=(double)-4/(2*a+1);
			break;
		}
		c=b+c;
	}

	return c;
}

double  surface_area(int r,int h,double pi)//公式
{
	double a=pi*r*r+pow(r*r+h*h,0.5)*pi*r;
	return a;
}

int main()
{
	int r,h,local;

	local=Pi_local();
	cout << local << endl;

	cout<<"請輸入半徑: ";
	cin>>r;

	cout<<"請輸入高: ";
	cin>>h;

	cout<<"使用Global Variable計算表面積的結果: "<<surface_area(r,h,pi)<<endl;
	cout<<"使用Local Variable計算表面積的結果: "<<surface_area(r,h,local)<<endl;

	system("pause");
	return 0;
}
