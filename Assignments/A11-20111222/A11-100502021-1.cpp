#include<iostream>
#include<cstdlib>
#include<iomanip>
#include<cmath>
using namespace std;

double pi=3.14; //宣告
double PI();
double surface(double,int,int);
int main()
{
	while(1)
	{
		double hi,und;
		cout<<"請輸入高與底面半徑:";
		cin>>hi>>und;
		cout<<"用global pi=3.14算 答案是:"<<surface(pi,hi,und)<<endl; 
		double pi=PI(); //讓local_pi=公式算出的pi
		cout<<"用公式第一百項的pi算 答案是:"<<surface(pi,hi,und)<<endl;
	}
	system("pause");
	return 0;
}

double PI()
{
	double up=4.0,und=1.0;
	double pi=0.0;
	while(und<200) //算到第一百項
	{
		pi=pi+up/und;
		up=-up; //分子正負互變
		und=und+2; //分母每次加2
	}
	return pi;
}

double surface(double pi,int hi,int und)
{
	return pi*und*und+pow(hi*hi+und*und,0.5)*pi*und;
}