#include <iostream>
#include <cstdlib>
#include <cmath>
using namespace std;

double pi=3.14; //global pi

double Pi_local() //use Gregory�VLeibniz series to calculate local pi
{
	double pi=0; //local pi
	for(int i=0;i<100;i++)
	{
		if(i%2==0)
		{
			pi = pi + 4/(static_cast<double>(i)*2+1);
		}
		else
		{
			pi = pi - 4/(static_cast<double>(i)*2+1);
		}
	}
	return pi;
}

void surface_area(int height,int radius) //output the answer
{
	cout << "\n";
	cout << "Global pi: " << pi << endl;
	cout << "The surface area is: ";
	cout << pi * height * height + ( pow( height*height + radius*radius , 0.5)*2*pi*radius )/2 << "\n\n"; //calculate and output
	cout << "Local pi: " << Pi_local() << endl;
	cout << "The surface area is: ";
	cout << Pi_local() * height * height + ( pow( height*height + radius*radius , 0.5)*2*Pi_local()*radius )/2 << "\n\n"; //calculate and output
}

int myheight;
int myradius;
int main()
{
	cout << "Calculating surface area of a cone by using global pi and local pi.\n";
	cout << "Please enter the height of the cone: ";
	cin >> myheight;
	cout << "Please enter the radius of the cone: ";
	cin >> myradius;
	surface_area(myheight,myradius); //use the function surface_area to calculate and output the answer
	system("pause");
	return 0;
}
