#include <iostream>
#include <cstdlib>
#include <cmath>

using namespace std;

double pi = 3.14;

double pi_local()
{
    double pi = 0;
    for( int i=0; i<100; i++ )
    {
        int remainder = i%2;
        switch(remainder)
        {
            case 0:
                pi += 4.0/(2*i+1);
                break;

            case 1:
                pi += -4.0/(2*i+1);
                break;
        }
    }

    return pi;
}

void surface_areaA( double height, double radius )
{
    double ans;
    double number = pow(radius*radius+height*height,0.5);
    ans = ((pi*radius*radius)+(0.5)*number*2.0*pi*radius);
    cout <<"\n(Global) The surface area is: " << ans;
}

void surface_areaB( double height, double radius )
{
    double pi = pi_local();
    double ans;
    double number = pow(radius*radius+height*height,0.5);
    ans = ((pi*radius*radius)+(0.5)*number*2.0*pi*radius);
    cout <<"\n(Local) The surface area is: " << ans;
}


int main()
{
    double height;
    double radius;

    cout << pi_local()<< endl;
    cout << pi << endl;

    cout << "Please enter the height and radius. " << "\nHeiht:";
    cin >> height;
    cout << "\nradius: ";
    cin >> radius;

    surface_areaA( height, radius );
    surface_areaB( height, radius );

    system("pause");
    return 0;
}
