#include<iostream>
#include<cstdlib>
#include<cmath>
using namespace std;


double Pi_local(int);
double surface_area(double ,double,double);
double GlobalPi=3.14;

int main()
{
    double height=0,radius=0;
    double LocalPi=Pi_local(100);
    cout<<"The global Pi is:"<<GlobalPi<<endl;
    cout<<"The local Pi is:"<<LocalPi<<endl;
    cout<<"Please enter the height:";
    cin>>height;
    cout<<"Please enter the radius:";
    cin>>radius;
    cout<<"The area of the cone’s surface with the Global Pi is:"<<surface_area(radius,height,GlobalPi)<<endl;
    cout<<"The area of the cone’s surface with the Local Pi is:"<<surface_area(radius,height,LocalPi)<<endl;
    system("pause");
    return 0;
}

double Pi_local(int n)
{
    double up=0,Pi=0,down=0;
    for(int x=0; x<n; x++)
    {
        switch(x%2)//分子隨奇數偶數而改
        {

        case 1:
            up=-1;
            break;

        case 0:
            up=1;
            break;
        }
        down=2*x+1;//分母為項數影響
        Pi+=up*4/down;
    }
    return Pi;
}
double surface_area(double r ,double h,double Pi) //接收半徑.高度.pi直
{
    return Pi*r*r+0.5*pow(r*r+h*h,1.0/2)*2*Pi*r; //利用pow函式算平方根後代公式
}
