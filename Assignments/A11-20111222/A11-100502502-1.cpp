//A11-100502502-1.cpp
#include<iostream>
#include<cmath>
#include<cstdlib>
using namespace std;
double Gpi = 3.14;//Global variable
double Pi_local()//計算圓周率
{
    double lpi = 0;
    double a;
    for( int term = 0; term < 100; term++ )//計算100次
    {
        if( term % 2 == 0 )
            a = 1;
        else if ( term % 2 == 1 )
            a = -1;
        lpi = lpi + a*4/(2*term+1);
    }
    return lpi;
}
double surface_area( double height, double radius, double pi )//計算圓錐的表面積
{
    return  pi * radius * radius  + 0.5 * pow( radius * radius + height * height , 0.5 ) * 2 * pi* radius;
}
int main()//function main 開始
{
    double height, radius;
    cout << "請輸入圓錐體的高和半徑 : " ;
    cin >> height >> radius;//接收高和半徑
    if ( ( height <= 0 ) || ( radius <= 0 ) )//如果高和半徑小於或等於零
        cout << "錯誤!!" << endl;
    else
    {
        cout << "圓錐的表面積(Global) = " << surface_area( height, radius, Gpi ) << endl;//顯示圓錐的表面積(使用Global variable)
        cout << "圓錐的表面積(local) = " << surface_area( height, radius, Pi_local() ) << endl;//顯示圓錐的表面積(使用local variable)
    }
    system( "pause" );//按任一鍵繼續
    return 0;
}//function main 結束
