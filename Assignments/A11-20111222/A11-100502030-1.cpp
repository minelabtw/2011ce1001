#include<iostream>
#include<cstdlib>
#include<cmath>
using namespace std;

double pi=3.14;// global variable

double Pi_local(int precision)
{
	double k;
	double p=0;
	for(int i=1;i<=precision;i++)
	{
		int a;
		a=i%2;// 判斷是奇數向還是偶數項
		switch(a)
		{
			case 0:
			k=-1;
			break;

			case 1:
			k=1;
			break;
		}// 使奇數項是正偶數項是負
		p=p+k/(2*i-1);
	}
	return 4*p;
}


double surface_area(double pi,double r,double h)
{
    return (pi*r*r+pow((r*r+h*h), 0.5)*pi*r);
}


int main()
{
    double radius,height;

    cout<<"請輸入圓錐底之半徑和圓錐之高\n";
    cin>>radius>>height;
    while(radius<=0 || height<=0)
    {
        cout<<"這是一個無效的輸入，請輸入合理的數字\n";
        cin>>radius>>height;
    }// 判斷數字是正數
    cout<<"此圓錐之表面積是(global variable):"<<surface_area(pi,radius,height)<<endl;

    double pi=Pi_local(100);// local variable

    cout<<"此圓錐之表面積是(local variable):"<<surface_area(pi,radius,height)<<endl;

    system("pause");
    return 0;
}
