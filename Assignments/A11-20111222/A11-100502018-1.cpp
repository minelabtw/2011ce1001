#include <iostream>
#include <cstdlib>
#include <cmath>
using namespace std;

double pi = 3.14; //call a global variable
double Pi_local();
void surface_area( double , double );

int main()
{
	double radius,height;
	
	cout << "Please input the radius and height of a cone (ex. 10 20) : ";
	cin >> radius >> height;

	surface_area( radius , height );

	system("pause");
	return 0;
}

double Pi_local()
{
	double pi = 0;
	for(int i=1;i<=100;i++) //use the loop to calculate the pi(local)
	{
		int remainder = i % 2;
		switch(remainder)
		{
			case 0:
				pi += -4 / static_cast < double > ( 2 * i - 1 );
				break;
			case 1:
				pi += 4 / static_cast < double > ( 2 * i - 1 );
				break;
		}
	}
	return pi;
}
void surface_area( double x , double y )
{
	cout << "The surface area of the cone (calculated with GLOBAL PI) is : " << ::pi * x * x + 0.5 * pow( x * x + y * y , 0.5 ) * 2 * ::pi * x << endl; //use the global pi to calculate the area
	cout << "The surface area of the cone (calculate with LOCAL PI) is : " << Pi_local() * x * x + 0.5 * pow ( x * x + y * y , 0.5 ) * 2 * Pi_local() * x << endl; //use the local pi to calculate the area
}