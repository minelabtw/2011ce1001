#include<iostream>
#include<cstdlib>
#include<cmath>
using namespace std;
	double Pi=3.14;     //Declare a Global Variable pi=3.14
	
	
	
	double surfaceArea(double height1,double radius1, double pi1)//計算表面積的函式 
	{
		double r=radius1;
		double h=height1;
		return pi1*(r*r+r*pow(r*r+h*h,1/2));//公式 
		
	}
	
	double pi()//計算PI的函式 
	{
		int k;
		double Answer1=0.0,y=0.0;
		for(k=0;k<100;k++)//跑100項 
		{
			if(k%2==0)//偶次方為正 
			{
				y=1.0*4.0/(2*k+1);	
			}	
			else
			{
				y=-1.0*4.0/(2*k+1);	//奇數次方為負 
			}
			Answer1+=y;
		}
		return Answer1;	
	}
	
	int main()
	{
		int height,radius;
		cout<<"Please enter the height: ";
		cin>>height;
		cout<<"Please enter the radius: ";
		cin>>radius;
		cout<<"surface with the Global Variable pi is"<<surfaceArea(height,radius,Pi)<<endl; 
		cout<<"surface with the Local Variable pi is"<<surfaceArea(height,radius,pi())<<endl;
		system("PAUSE");
		return 0;
	}
