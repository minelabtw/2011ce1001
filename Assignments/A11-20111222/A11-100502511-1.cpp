#include <iostream>
#include <cmath>
#include <cstdlib>
using namespace std;

double PI = 3.14;  // Global PI

double PI_local()  // Local PI 
{
    double PI = 0;
    int item,num;
    for (item=0;item<100;item++)  // 建立 0-99 的 PI 值 ， 共 100 項 
    {
           switch (item%2)  
           {
                  case 0:  // item除以2餘數為0時,num=1 
                  num=1;
                  break;
                  case 1:  // item除以2餘數為1時,num=-1 
                  num=-1;
                  break;
           }
           PI += 4*((double)num/(double)(2*item+1));  // 把item,num定義為double           
    }       
    return PI;              
}

double surface_area()  // 計算表面積 
{
    double high;
    double radius;
    double globalarea;
    double localarea;
    cout << endl << "Please input high: ";  // 輸入高 
    cin >> high;
    cout << "Please input radius: ";  // 輸入半徑 
    cin >> radius;
    globalarea = PI*radius*radius+0.5*2*PI*radius*pow(radius*radius+high*high,0.5);  
    localarea = PI_local()*radius*radius+0.5*2*PI_local()*radius*pow(radius*radius+high*high,0.5);
    cout << "Local surface_area: " << localarea << endl;
    cout << "Global surface_area: " << globalarea << endl << endl;  
}

int main()
{
    for(;;)
    {
           int choosenumber;
           cout << "(1:show Local PI and Global PI 2:surface_area Others:To leave)" << endl;
           cout << "Choose the function you want: ";
           cin >> choosenumber;
           switch(choosenumber)  // 選擇function 
           {
                               case 1:  // 顯示 Global PI and Local PI 
                                    cout << endl << "Local PI = " << PI_local() << endl;
                                    cout << "Global PI = " << PI << endl << endl;
                                    break;
                               case 2:  // 計算表面積 
                                    surface_area();
                                    break;     
                               default:
                                    system("pause");
                                    return 0;  
                                    break; 
           }       
    }
}
