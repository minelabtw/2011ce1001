#include <iostream>
#include <string>
#include <sstream>
#include <cmath>

using namespace std;

const float pi = 3.14; // global variable

float get_float(){

    float float_num = 0.0;
    string input;
    getline (cin, input);
    stringstream ss (input);
    if (ss >> float_num) return float_num;
    cout << "invalid input, expected float.\n";
    return -1;

}

float pi_local () {

    float res = 4.0;

    // must not forget that res already contained the 1st term
    // i begin from 1 not 0
    for (float i = 1.0; i < 100.0; i++){
        res = -res;
        res = res + 4.0/(2.0*i+1.0);
    }

    if (res < 0) res = -res;

    return res;

}

float area (float r, float h, float pi){
    // given function:
    // A = pi * r^2 + 1/2 * sqrt(h^2 +r^2) * 2*pi*r
    // rearranged into:
    // A = pi*r ( r + sqrt(h^2+r^2) )
    float res = h*h + r*r;
    res = r + sqrtf (res);
    return res * pi * r;
}

int main()
{

    float rad;
    float height;

    cout << "please insert cone's radius: ";
    rad = get_float();
    if (rad < 0) {
        cout << "cannot have negative number!";
        return -1;
    }

    cout << "please insert cone's height: ";
    height = get_float();
    if (height < 0) {
        cout << "cannot have negative number!";
        return -1;
    }

    cout << endl;

    cout << "global pi:" << pi << "\n";
    cout << "cone's area calculated with global pi: ";
    cout << area (rad, height, pi) << "\n";

    cout << endl;

    cout << "local pi:" << pi_local() << "\n";
    cout << "cone's area calculated with local pi: ";
    cout << area (rad, height, pi_local()) << "\n";

    return 0;

}
