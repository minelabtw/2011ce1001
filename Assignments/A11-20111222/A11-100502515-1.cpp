#include <iostream>
#include <cstdlib>
#include <cmath>
using namespace std;

double pi = 3.14;//The global PI value.
double pi_local ( );//Function prototype.
double surface_area ( );//Function prototype.


int main( )
{
    surface_area ( );//the surface area is shown here.

    system ( "pause" );
    return 0;
}

double pi_local ( )
{
    double pi = 0.0;
    int mom = 0, son = 0;
    for ( int i = 0; i <= 99; i++ )
    {
            mom = ( 2 * i + 1);

            switch ( i % 2 )
            {
                   case 0:
                   son = 1;
                   break;

                   case 1:
                   son = -1;
                   break;
            }

            pi += 4.0 * ( ( double ) son / ( double ) mom );//Local PI value calculated here according to the Gregory�VLeibniz series.
    }

    return pi;//return the local PI value.
}

double surface_area( )
{
    double h, r;

    cout << "Enter the HEIGHT of the cone: ";
    cin  >> h;//Input the height of the cone.
    cout << "Enter the RADIUS of the cone: ";
    cin  >> r;//Input the radius of the cone.
    // the surface of the cone is calcualted by the following.
    cout << "The surface area( global ): " << pi * r * r + 0.5 * pow ( h * h + r * r, 0.5 ) * 2 * pi * r << endl;
    cout << "The surface area( local ): " << pi_local( ) * r * r + 0.5 * pow ( h * h + r * r, 0.5 ) * 2 * pi_local( ) * r << endl;
}   //Output the surface area calculated by the global and local PI value.
