#include <iostream>
#include <cstdlib>
#include <cmath>

using namespace std;

double pi=3.14;//全域變數pi=3.14 

double Pi_local()//Pi_local function 計算pi最準確的值 
{
    //各項變數初始值 
    double a;
    double b=0;
    double pi=0;
    
    for(int item=1;item<=100;item++) 
	{
		switch(item%2)//item/2的餘數 
		{
            case 0: 
					a=-1;
					break;
					
			case 1: 
					a=1;
					break;
						
		}
		pi+= 4*(a/(2*b+1));//pi公式 
		b++;
	}
	return pi;  
    
}
 
 
double surface_area( double r,double h,double pi ) 
{
    return pi*r*r + 0.5*sqrt(r*r+h*h) * 2*pi*r;  //圓錐表面積計算公式 
    
}

int main()
{
    
    double r;//兩變數r=半徑,h=高為user輸入 
    double h;
    
    
    cout<<"輸入圓錐體的半徑:";
    cin>>r;
    
    cout<<"輸入圓錐體的高:"; 
    cin>>h;
    
    cout<<"以pi計算表面積為:"<< surface_area( r , h , pi)<<endl;//精簡值 
    cout<<"以 n計算表面積為:"<< surface_area( r , h , Pi_local())<<endl;//最準確值 

    system("pause");
    return 0;
}
