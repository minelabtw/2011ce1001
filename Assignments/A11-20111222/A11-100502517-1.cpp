#include<iostream>
#include<cstdlib>
#include<cmath>
using namespace std;

double globalpi = 3.14;

double Pi_local()
{
    int k,a=0,up=0,m;//up 是 分子

	double total = 4;// total 是總和

	for(k=1;k<=100; k++)//預設第一項是4，然後開始跑分數
	{
		a = (2*k+1);
		m = k;
		m = m%2;

		if(m==1)//判斷分子是否正數or負數
		{
				up = -1;
				total = total+4*(double)up / (double)a;//計算圓周率
		}
		else if(m==0)
		{
				up = 1;
				total = total+4*(double)up / (double)a;//計算圓周率
		}

	}
	return total;//回傳第100項的值
}

double surface_area(double height, double radius, double Pi)
{
    return Pi*radius*radius+pow(radius*radius+height*height, 0.5)*radius*Pi;//表面積公式
}
int main()
{
    double high, rad;
    for(;;)
    {
        cout<<"Please cin height: ";

        cin>>high;

        if(high>0)//高度必須大於0
            break;
    }

    cout<<endl;

    for(;;)
    {
        cout<<"Please cin radius: ";

        cin>>rad;

        if(rad>0)//半徑必須大於0
            break;
    }
    cout<<endl;

    cout<<"The surface that using globalPi to calculate is: "<<surface_area(high, rad, globalpi)<<endl
        <<"The surface that using Pi_local() to calculate is: "<<surface_area(high, rad, Pi_local())<<endl;

    system("pause");
    return 0;
}
