#include <iostream>
#include <cstdlib>
#include <cmath>
using namespace	std;

const double global_pi=3.14;

double pi_local()
{
	int i;
	double j=0,local_pi=0.0;

	for(i=0;i<100;i++)
	{
		switch(i%2)	// 判斷是奇數項還是偶數項
		{
			case 1:		// 奇數項
				j=-4.0/(2*i+1);
				break;
			case 0:		 // 偶數項
				j=4.0/(2*i+1);
				break;
		}
		local_pi+= j;		 // 計算各項和	
	}
	
	return local_pi;		//回傳Pi
}

void surface_area(double high,double rad)
{
	double local_pi=pi_local();
	cout<<"使用 local variable 計算的結果為: \n";
	cout<<(local_pi*rad*rad)+(pow(((rad*rad)+(high*high)),(0.5))*local_pi*rad);
	cout<<endl;
	cout<<"使用 global variable 計算的結果為: \n";
	cout<<(global_pi*rad*rad)+(pow(((rad*rad)+(high*high)),(0.5))*global_pi*rad);
	cout<<endl;
}
int	main()
{
	double radius,height;
	cout<<"請輸入高度: ";
	cin>>height;
	cout<<endl;
	cout<<"請輸入半徑: ";
	cin>>radius;
	cout<<endl;
	surface_area(height,radius);

	system ("pause");
	return 0;
}