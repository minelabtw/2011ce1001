/******************************************************************************
 * Assignment 11-1
 * School/Year: National Central University, Taiwan / Freshman
 * Name:        江衍霖 ; Chiang,Yen-lin
 * E-mail:      flashcity123@kimo.com
 *              100502514@cc.ncu.edu.tw
 * Course:      NCU1001-CE1001A
 * Course Name: Introduction to Computer Science
 * Class:       CSIE 1A
 * Submitted:   Dec.22,2011
 *
 * Description:
 * Declare a GLOBAL variable and a local function for PI value.
 * Local function PI_local(n) to estimate PI is referenced from Quiz #2.
 * Then Calculate a cone's surface area with the input height and radius.
 * Both values of surface area in Global variable and Local function
   will be shown.
 * This Program will use n=100 as the parameter for function PI_local(n).
 * This code may include <cmath> for function sqrt(x).
 *
 * NOTICE: From now and then, I will usually include my own header.
 * If the compiling passed but there's a function never seen,
   it may be defined in my own header.
 *
 ******************************************************************************/

/****************************************
 *       Include my Custom Header       *
 ****************************************/
#include "A11-100502514-1.h"
#include <cmath>
using namespace FlashTeens;
using namespace std;

//Line Constant Arrangement
const int INPUT_MESSAGE_LINE = 1;
const int INPUT_NUMBER_LINE = 3;


/********************************************
 *  Global Variables & Function Prototypes  *
 ********************************************/

double global_PI = 3.14;
double local_PI(int);
double surface_area(double, double, bool useLocal=false);

const int N_FOR_LOCAL_PI = 100;

//Main Function
int main(){
    double input_height, input_radius;
    //Messages before input
    cout << gotoXY(0, INPUT_MESSAGE_LINE)
         << toColorString("<skyblue>Please input 2 positive numbers for a cone\'s ")
         << toColorString("<yellow>height<skyblue> and <yellow>radius<skyblue>:")
         << gotoXY(0, INPUT_NUMBER_LINE) << setfg(GREEN);

    //To Prevent from error inputs
    if( !(cin>>input_height>>input_radius) || input_height<0 || input_radius<0 ){
        cout << toColorString(
            "\n<red> AN ERROR OCCURRED!! THIS PROGRAM WILL EXIT.");
        pause();
        return 1;
    }

    //Calculations and Outputs
    cout << toColorString(
            "\n<skyblue> Surface Area by <yellow>GLOBAL<skyblue> variable is: ");
    cout << setfg(YELLOW) << fixed << setprecision(10)
         << surface_area(input_height, input_radius);

    cout << toColorString(
            "\n<skyblue> Surface Area by <yellow>LOCAL<skyblue>  function is: ");
    cout << setfg(YELLOW) << fixed << setprecision(10)
         << surface_area(input_height, input_radius, true);

    //Let the text color back to the preset color.
    cout << setfg();

    pause();
    return 0;
}


/******************************
 *    Function Definitions    *
 ******************************/
//The function below is referenced from Quiz #2.
double local_PI(int est){
    if(est<0)
        return 0.0; //0.0 stand for invalid

    double result=0.0;
    for(int i=0;i<=est;i++){
        result+=4.0/( static_cast<double>( (2*i+1)*( 1-(i%2)*2 ) ) );
    }
    return result;
}

//Calculating the surface area for a cone with height and radius.
double surface_area(double height, double radius, bool useLocal){
    double PI, area;
    if(useLocal) PI = local_PI(N_FOR_LOCAL_PI);
    else PI = global_PI;
    //Use the given formula to calculate for the answer.
    return PI*radius*radius + 0.5*sqrt(radius*radius + height*height)*2*PI*radius;
}
