#include<iostream>
#include<iomanip>
#include<cstdlib>
#include<cmath>
using namespace std;

double pi=3.14;

double Pi_local();//算出來的pi

void surface_area(double,double,double);//計算表面積

int main()
{
    double height;
    double radius;

    cout << "Enter the height first,then enter the radius: ";
    cin >> height >> radius;

    cout << "\nThe answer with the global variable pi is ";

    surface_area(height,radius,pi);//顯示使用全域變數pi的結果

    {
        double pi=Pi_local();

        cout << "The answer with the local variable pi is ";

        surface_area(height,radius,pi);//顯示使用區域變數pi的結果
    }

    system("pause");

    return 0;
}

double Pi_local()//根據公式計算pi
{
    double sum=0;
    double n;

    for(int k=0;k<=100;k++)
    {
        n=k;

        if(k%2==0)
            sum+=1/(2*n+1);

        if(k%2==1)
            sum-=1/(2*n+1);
    }

    return sum*4;
}

void surface_area(double height,double radius,double pi)//根據公式計算表面積
{
    cout << pi*pow(radius,2)+sqrt(pow(radius,2)+pow(height,2))*pi*radius << "\n\n";
}
