#include<iostream>
#include<cstdlib>
#include<cmath>

using namespace std;

double volume(double pi,double radius,double height)//計算圓錐體積 
{
    double x;
    x=pow(radius*radius+height*height,0.5);//先把(r^2+h^2)^1/2設成一個代數方便計算和回傳 
    return pi*radius*radius+1/2*x*2*pi*radius;
}

double pi_2()//計算精準版本的pi 
{
     double a=1;//設定各項初始值
	 double b=1;
	 double pi=0;
     for(int k=0;k<1000;k++)//設定相加的迴圈1000項 
	 {
        
		
        b=2*k+1;
		switch(k%2)//k/2的餘數
		{
			case 0://k/2的餘數為0
				pi=pi+4*a/b;
				break;
			case 1://k/2的餘數為1
				pi=pi-4*a/b;
				break;
         }
      }
      return pi;
}
	

int main()
{
    double radius=0;//設定各項初始值
    double height=0;
    double pi_1=3.14;
    
    cout << "請輸入圓錐體的半徑:";
    cin >> radius;
    cout << "請輸入圓錐體的高:"; 
    cin >> height;
    
    cout << "以圓周率pi帶入計算圓錐體體積:" << volume(pi_1,radius,height)  << endl;//精簡版pi 
    cout << "以圓周率π帶入計算圓錐體體積:" << volume(pi_2(),radius,height)<< endl;//準確版pi 
    
    
    system("pause");
    
    return 0;
}



	
	

	


