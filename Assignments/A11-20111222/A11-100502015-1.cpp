#include<iostream>
#include<cstdlib>
#include<cmath>
using namespace std;
double pi=3.14;//第一種pi 
double npi(int k)//第二種pi,相加100次 
{
    double total=1;
    for(int i=1;i<=k;i++)
    {       if(i%2==1)
            {         
                      total=total-(1/(2*double(i)+1));
            }
            else if(i%2==0)
            {         
                      total=total+(1/(2*double(i)+1));
            }
    }
    return total*4;

}
double cone(double inheight,double inr ,double inpi)//算體積的函式 
{
    return inpi*pow(inr,2)+0.5*pow(pow(inr,2)+pow(inheight,2),0.5)*2*inpi*inr;
}
int main()
{
    double height,r;

    cout<<"input height & r"<<endl;
    cin>>height;
    cin>>r;
    cout<<"3.14 pi is: "<<cone(height,r,pi)<<endl;
    cout<<"100項 pi is: "<<cone(height,r,npi(99))<<endl;

    system("pause");
    return 0;
}
