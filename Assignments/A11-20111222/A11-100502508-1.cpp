#include<iostream>
#include<cstdlib>
#include<cmath>
using namespace std;
double Pi_local(double);//prototype
double surface_area(double,double,double);//prototype 
double pi=3.14;//global variable
int main()//the main function execute
{
    double localpi;//local variable to main
    double height;
    double radius;
    int flag=1;
    while(flag)//repeat to execute
    {
        cout<<"Global pi = "<<pi<<endl;//call the global variable
        cout<<"Local pi = "<<Pi_local(localpi)<<endl;//function call
        cout<<"Enter the height and the radius of the cone."<<endl;
        cout<<"Enter the height."<<endl;
        cin>>height;
        cout<<"Enter the radius."<<endl;
        cin>>radius;
        cout<<"surface area by global pi = "<<surface_area(pi,height,radius)<<endl;//function call of the global variable ,height and radius
        cout<<"surface area by local pi = "<<surface_area(Pi_local(localpi),height,radius)<<endl;//function call of the local variable ,height and radius
        cout<<"\nEnter 0 to leave."<<endl;
        cin>>flag;
        system("cls");
    }
    system("pause");
    return 0;
}//end main
double Pi_local(double valueofpi)//calculate the Gregory�VLeibniz series
{
    double denominator;
    double sigma1=0;
    double sigma2=0;
    for(int item=0;item<100;item++)//loop
    {
        switch(item%2==0)//remainder equals to zero
        {
            case 1:
                denominator=2*item+1;
                sigma1=sigma1+4*(1/denominator);//add every worth of k which is even
                break;//necessary to exit switch
        }//end switch
        switch(item%2!=0)//remainder doesn't equal to zero
        {
            case 1:
                denominator=2*item+1; 
                sigma2=sigma2-4*(1/denominator);//minus every worth of k which is odd
                break;//necessary to exit switch
        }//end switch
    }//end for
    return sigma1+sigma2;
}//end function Pi_local
double surface_area(double Pi,double Height,double Radius)//calculate the cone of surface area
{
    if(Pi==pi)//equal the global variable
    {
        return Pi*Radius*Radius+(1/2)*pow((Height*Height+Radius*Radius),0.5)*2*Pi*Radius;
    }    
    else//not equal the global variable
    {
        return Pi*Radius*Radius+(1/2)*pow((Height*Height+Radius*Radius),0.5)*2*Pi*Radius;
    }
}//end function surface_area
    
