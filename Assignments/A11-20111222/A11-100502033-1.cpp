#include <iostream>
#include <cstdlib>
#include <cmath>
using namespace std;
int num1 ;
double num2 , num3 , high , r , Pi2 , surface , surface2 , Pi = 3.14;
void function_Pi()
{
    for (int i = 0 ; i <= 99 ; i++) //Pi2 由0 至 99 的值
    {
        num1 = 2 * i + 1;
        num2 = 1.0 / num1 ;
        if ( i % 2 != 0)
        {
            num2 = num2 * -1 ;
        }
        num3 = num3 + num2 ;
    }
    Pi2 = 4 * num3 ; //計算Pi2的值
    cout << Pi2 ; //印出Pi2的值
}
void  function_surface(double high , double r)
{
    surface = Pi2 * pow(r,2) + 0.5 * pow( (pow(r,2) + pow(high,2)) , 0.5  ) * 2 * Pi2 * r; //圓錐體表面面積(局部變量π)
    surface2 = Pi * pow(r,2) + 0.5 * pow( (pow(r,2) + pow(high,2)) , 0.5  ) * 2 * Pi * r; //圓錐體表面面積(全局變量π)
    cout << "\n 圓錐體表面面積(局部變量π) = " << surface ;
    cout << "\n 圓錐體表面面積(全局變量π) = " << surface2 ;
}
int main ()
{
    cout << "局部變量π = " ;
    function_Pi() ; //呼叫function
    cout << "\n全局變量π = 3.14" ;
    bool Variable = false ;
    while (Variable == false ) //當Variable等於false運行
    {
        cout << "\n請輸入圓錐體的高度:" ;
        cin >> high ; //輸入一個值
        if ( high <= 0 )
        {
            cout << "輸入錯誤，強制結束\n" ;
            Variable = true ;
            break ;
        }
        cout << "\n請輸入圓錐體的半徑:" ;
        cin >> r ; //輸入一個值
        if ( r <= 0)
        {
            cout << "輸入錯誤，強制結束\n" ;
            Variable = true ;
            break ;
        }
        function_surface( high , r) ; //呼叫function
        cout << endl ;
    }
    system("pause");
}
