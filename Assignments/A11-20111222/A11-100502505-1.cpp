#include<iostream>
#include<cstdlib>
#include<cmath>
using namespace std;

double pi = 3.14;//global variable

double Pi_local()//計算pi到100項 
{
    double pi=0;//local variable
    double a;
    double b=0;
    for(int terms=1;terms<=100;terms++) 
	{
		switch(terms%2)
		{
			case 1: a=1;//餘1 a就是正的
			break;
			case 0: a=-1;//餘0 a就是負的
			break;
		}
		pi += 4 * (a/(2*b+1));//從第一項加到第100項 
		b++;
	}
	return pi;//回傳pi值 
}

double surface_area(double r,double h,double Pi)//計算圓錐體的表面積 
{
    return Pi*r*r + 0.5*sqrt(r*r+h*h) * 2*Pi*r;//公式  
}

int main()
{
    double r,h;
    cout << "請輸入圓錐體的半徑和高:";
    cin >> r >> h;
    cout << "表面積為:" << surface_area(r,h,pi) << endl;
    cout << "表面積為:" << surface_area(r,h,Pi_local()) << " (pi算到100項)" << endl;
    system("pause");
    return 0;    
}
