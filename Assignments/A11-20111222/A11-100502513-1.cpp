#include<iostream>
#include<cstdlib>
#include<cmath>
using namespace std;

double pi=3.14;//Global Variable的pi值 
double  Pi_local()//funtion計算的pi值 
{
   double a;
   double b=0; 
   double c=0;
   for(int i=0;i<=99;i++)
   {
      switch(i%2)//把i除以2,來判斷奇偶數 
      {
         case 1://如果餘數為1,代表i為奇數 ,把a=-1,k為基數項為負 
             a = -1;                
             break;                
         case 0:
             a = 1;//如果餘數為0,代表i為偶數 ,把a=1,k為偶數項為正  
             break;       
      }
      b = 2*i+1;//分母 
      c += 4*(a/b);//整個式子 
   }
   return c;
} 

double surface_area(double r,double h,double enterpi)//(半徑,高,pi值) 
{
   return ( enterpi*r*r + enterpi *r*pow( (r*r+h*h),0.5 ) );//表面積方程式 
}

int main()
{
   double radius,height;
   
   for(;;)
   {
      cout<<"\n請輸入圓錐的半徑: ";
      cin>>radius;
      cout<<"請輸入圓錐的高: ";
      cin>>height;
      cout<<"此圓錐體表面積為(Global pi=3.14): "<<surface_area(radius,height,pi)<<endl;
      cout<<"此圓錐體表面積為(local pi="<<Pi_local()<<"): "<<surface_area(radius,height,Pi_local())<<endl;
   }
   return 0;
}    
