#include <iostream>
#include <cstdlib>
#include <cmath>
using namespace std;

double Pi_local() // calcute the pi value to 100 terms with the Gregory�VLeibniz series
{
    double denominator;
    double result = 0;
    for ( int i=1; i<=100; i++ )
	{
		denominator = i * 2 - 1;
		if ( ( i % 2 ) == 1 ) // if term is odd, add "4 / denominator" to result
		    result += 4 / denominator;
        if ( ( i % 2 ) == 0 ) // if term is even, substract "4 / denominator" from result
            result -= 4 / denominator;
	}
    return result;
}

double surface_area( double h, double r, double pi ) // calculate the surface are of the cone
{
    return pi * r * r + ( 1.0 / 2 ) * pow( r * r + h * h, ( 1.0 / 2 ) ) * 2 * pi * r;
}

double pi = 3.14; // declare global pi

int main()
{
    int height = 0, radius = 0; // declare and initialize the height and the radius

    while ( height <= 0 ) // check the validity of the height
    {
        cout << "Please enter the height of the cone: ";
        cin >> height;
        if ( height <= 0 ) // if it is invalid, show error message and let user input again
            cout << "Error input!Please enter the positive number." << endl;
    }
    while ( radius <= 0 ) // check the validity of the radius
    {
        cout << "Please enter the radius of the cone: ";
        cin >> radius;
        if ( radius <= 0 )
            cout << "Error input!Please enter the positive number." << endl;
    }
    cout << "Use global pi, the surface area of the cone is " << surface_area( height, radius, pi) << endl; // display the surface area of the cone that use global pi to calculate

    double pi = Pi_local(); // declare and initialize the local pi

    cout << "Use local pi, the surface area of the cone is " << surface_area( height, radius, pi ) << endl; // display the surface area of the cone that use local pi to calculate
    system("pause");
    return 0;
}
