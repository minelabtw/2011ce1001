//Include
#include<iostream>
#include<cstdlib>
#include<cmath>

using namespace std;

//Declare a global variable
double const pi = 3.14 ;

//Declare a function for calculating pi(100)
double Pi() ;

//Declare a function for calculating two surface areas with the input of radium and height
double SurfaceArea( double radius , double height ) ;

int main()
{
    double radius ;
    double height ;

    cout << "Please key in your radius and height in turn.\n" ;
    cin >> radius >> height ;

    SurfaceArea( radius , height ) ;

    system( " pause " ) ;

    return 0 ;
}

//Define a function for calculating pi(100)
double Pi()
{
    double numberpi = 0 ;
    int counter = 1 ;
    int const constant = 4 ;

    for ( counter = 0 ; counter <= 100 ; counter++ )
    {
        if( counter % 2 == 1 )
        {
            numberpi = numberpi + constant / ( 2 * counter - 1 ) ;
        }
        else
        {
            numberpi = numberpi - constant / ( 2 * counter - 1 ) ;
        }
    }

    return numberpi ;
}
//Define a function for calculating two surface areas with the input of radium and height
double SurfaceArea( double radius , double height )
{
    double area ;

    area = pi * radius * radius + pow( radius * radius + height * height , 0.5 ) * 2 * pi * radius *0.5 ;
    cout << "Surface area calculated by global variable is\t" << area << "\t.\n" ;

    area = Pi() * radius * radius + pow( (radius * radius + height * height) , 0.5 ) * 2 * Pi() * radius*0.5 ;
    cout << "Surface area calculated by local variable is\t" << area << "\t.\n" ;

    return area ;
}
