#include<iostream>
#include<cstdlib>
#include<cmath>
using namespace std;

double pi=3.14;//global variable
double pi_local(int n=100);//function to compute pi(local)
double surface_area(double,double,double);//function to compute the cone's surface area
double check(double);

int main()
{
    double height,radius;
    cout<<"Claculate the area of the cone's surface"<<endl;
    cout<<"  enter the cone's height:";
    cin>>height;
    height=check(height);//check the entered number is acceptable
    cout<<"  enter the cone's radius:";
    cin>>radius;
    radius=check(radius);//check the entered number is acceptable
    cout<<"the area of the cone's surface is:"<<endl
        <<" 1)Global Variable: "<<surface_area(height,radius,pi)<<endl//print the area (computed from global pi)
        <<" 2)Local Variable : "<<surface_area(height,radius,pi_local())<<endl;//print the area (computed from local pi)
    system("PAUSE");
    return 0;
}

double pi_local(int n)//calculate the value of pi (100 terms)
{
    int i=0;
    double pi_tmp=0;
    while(i<n)
    {//use the equation to calculate pi's value (100 terms)
        if(i%2==0)//odd items
            pi_tmp=pi_tmp+4./(2*i+1);
        else if(i%2==1)//even items
            pi_tmp=pi_tmp-4./(2*i+1);
        i=i+1;
    }
    return pi_tmp;
}
double surface_area(double height,double radius,double pi)
{//use the equation to calculate the area of the cone's surface
    return pi*pow(radius,2)+sqrt(pow(radius,2)+pow(height,2))*pi*radius;
}
double check(double enter)
{
    while(enter<0)
    {
        cout<<"   the values of the cone's height and radius should be positive!!"<<endl
            <<"   please enter again : ";
        cin>>enter;
    }
    return enter;
}
