#include <iostream>
#include <cmath>
using namespace std;
double pi = 3.14;//要求1的全域變數
double localPi(double);//用來公式計算的Pi
void area(double, double,double);//表面積計算
void error();//因輸入錯誤而顯示的訊息
int main()
{
    double height =-1 , radius = -1, number=-1;//高度,半徑,公式計算至第幾項 的變數
    cout << "圓錐體面積計算\n";
    while (height<=0)//高度輸入
    {
        cout << "請輸入高度(cm)";
        cin >> height;
        if (height<=0)//檢查
            error();
    }
    while (radius <=0)//半徑輸入
    {
        cout << "請輸入半徑(cm)";
        cin >> radius;
        if (radius<=0)//檢查
            error();
    }
    while (number<=0)//公式項數輸入
    {
        cout << "請輸入Gregory-Leibniz series要計算至第幾項";
        cin >> number;
        if (number<=0)//檢查
            error();
    }
    cout << "計算至第"<< number << "項的Pi值為：" << localPi(number)<<endl;
    area(height,radius,number);//計算
    return 0;
}
double localPi(double i)
{
    if (i>=1)
        return localPi(i-1)+pow(-1,i+1)*4/(2*i-1);//公式
    else
        return 0;
}
void area(double h,double r, double no)
{
    cout << "按localPi計算結果為："<< localPi(no)*pow(r,2)+pow((pow(r,2)+pow(h,2)),0.5)*localPi(no)*r<<"(cm^2)"<<endl;//localPi的計算結果
    cout << "按pi=3.14的結果為："<< pi*pow(r,2)+pow((pow(r,2)+pow(h,2)),0.5)*pi*r<<"(cm^2)";//3.14的計算結果
}
void error()//錯誤訊息
{
    cout << "輸入錯誤，";
}
