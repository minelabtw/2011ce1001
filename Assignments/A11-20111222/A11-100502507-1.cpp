/*Assignment 11
100502507-�L�l�W*/
#include <iostream>
#include <cstdlib>
#include <cmath>
using namespace std;

double pi = 3.14;//Global variable pi

int main()//Function main begins execution
{
	int r, h;//Variable radius and height
	double Pi_local();
	double surface_area( double, double, double );
	cout << "Pi_Global = "
		<< pi
		<< "\nPi_Local = "
		<< Pi_local()
		<< "\nNow enter radius: ";
	cin >> r;
	cout << "Enter height please: ";
	cin >> h;
	cout << "Global result = "
		<< surface_area( r, h, pi )
		<< "\nLocal result = "
		<< surface_area( r, h, Pi_local() );	
	system("pause");
	return 0;
}//End function main

double Pi_local()//Function which calculates the value of local pi
{
	double sum;
	sum = 1;//Condition n = 0
	for( int i=1; i<100; i++ )
	{
		if( i%2 == 0 )//Positive condition
		{
			sum = sum + 1.0/((2.0*i)+1);
		}
		else//Negative condition
		{
			sum = sum + (-1.0)/((2.0*i)+1);
		}
	}
	return 4*sum;
}

 double surface_area( double r, double h, double pi )//Function which calculates the value of surface
 {
	 return (pi*r*r)+pi*r*sqrt((r*r)+(h*h));
 }