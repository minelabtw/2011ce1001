#include<iostream>//allow program to input and output
#include<cstdlib>
#include<cmath>
using namespace std;
 
double Pi_global=3.14;//define global varible Pi
double Pi_local();//define function express local varible Pi
double surface_area(double,double,double);//define the 表面積 function 
int main()
{
    double Height,Radius,judge=1;
    while(judge!=0)
    {
           cout << "請輸入圓錐面積的高及半徑長\n";
           cout << "Height : ";       
           cin >> Height;            
           if(Height<0)//若輸入的高度為0則顯示錯誤 
                       cout << "輸入錯誤!!\n請輸入一正數唷\n";
           else
           {
                       cout << "Radius : ";
                       cin >> Radius;
                       if(Radius<0)//若輸入的半徑為0則顯示錯誤 
                                   cout << "輸入錯誤!!\n請輸入一正數唷\n";
                       else
                       {
                                   cout << "Surface_area by Pi_global is " << surface_area(Height,Radius,Pi_global) << endl;//計算出圓錐表面積，利用global varible 
                                   cout << "Surface_area by Pi_local is " << surface_area(Height,Radius,Pi_local()) << endl;//計算出圓錐表面積，利用local varible
                       }
           }
           cout << "\n停止 : 0 ; 繼續 : 1\n";//判斷是否要繼續輸入 
           cin >> judge;
    }
    system("pause");
    return 0;
} 
double Pi_local() 
{
       double sum,total=0;//定義變數及初始化total 
       for(int i=1;i<=100;i++)//計算Pi到100項 
       {
               sum=0;//每次計算完畢，則將sum歸零 ，再重新計算 
               switch(i%2)// 求除以2的餘數 
               {
                          case 1://若為奇數 
                               sum+=2*i-1;//計算每項的分母值(單數項) 
                               break;//跳出switch 
                          case 0://若為偶數 
                               sum+=(-1)*(2*i-1);//計算每項的分母值(偶數項) 
                               break;//跳出switch 
                          default:
                               break;
               }
               total+=4/sum; 
       }
       return total;//回傳Pi值
}
double surface_area(double height,double radius,double PI)
{
       return PI*pow(radius,2.0)+0.5*pow((pow(radius,2.0)+pow(height,2.0)),0.5)*2*PI*radius;//回傳圓錐表面積公式 
}
