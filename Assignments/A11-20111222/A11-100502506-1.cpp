#include<iostream>
#include<cstdlib>
#include<cmath>
using namespace std;
double pi=3.14;//pi global

double Pi_local()//pi local
{
    int arraysize=100;
    double array[arraysize];//100 個陣列
    double pi=0;
    for(int k=0;k<=arraysize-1;k++)//從0開始 跑100次
    {
        array[k]=((2*k)+1);//100個從1開始,公差2的奇數
    }
    double sign;
    for(int k=0;k<=arraysize-1;k++)//從0開始 跑100次
    {
        switch(k%2)//判別奇數項為正數,偶數項為負數
        {
            case 0://奇數項為正數
                sign=1;
                break;
            case 1://偶數項為負數
                sign=-1;
                break;
        }
        pi=pi+sign*4/(array[k]);//每一項之和
    }
    return pi;
}

double surface_area(double pi,double height,double radius)//表面積
{
    return ((pi*radius*radius)+((0.5)*(pow(((radius*radius)+(height*height)), 0.5))*2*pi*radius));//表面積公式
}

double surfaceArea(double radius, double height, double pi) // function to calculate the area of cone's surface
{
    double fanRadius;
    fanRadius = pow(((radius*radius)+(height*height)), 0.5); // fanRadius = (radius^2 + height^2)^(1/2)
    // the area of cone's surface = area of cone's bottom + area of cone's fan
    //                            = pi*(radius^2) + {(1/2)*[(radius^2 + height^2)^(1/2))]}*2*pi*radius
    //                            = pi*(radius^2) + [(1/2)*fanRadius]*2*pi*radius
    //                            = pi*radius*(radius + fanRadius)
    return (pi*radius*(radius + fanRadius));
}


int main()
{
    double hgt;//高度
    double rds;//半徑
    cout<<"Pi_global = "<<pi<<endl;
    cout<<"Pi_local = "<<Pi_local()<<endl;
    cout<<"請輸入圓錐表面積的高以及半徑\n";
    do
    {
        cin>>hgt>>rds;
        if(hgt<0||rds<0)//負不合
        {
            cout<<"請輸入正數的高和半徑\n";
        }
    }while(hgt<0||rds<0);
    system("cls");
    cout<<"以Pi_global = "<<pi<<" 的圓錐表面積 = "<<surface_area(pi,hgt,rds)<<endl;
    cout<<"以Pi_local = "<<Pi_local()<<" 的圓錐表面積 = "<<surface_area(Pi_local(),hgt,rds)<<endl;
    //cout<<"以Pi_global = "<<pi<<" 的圓錐表面積 = "<<surfaceArea(rds, hgt, pi)<<endl;
    //cout<<"以Pi_global = "<<pi<<" 的圓錐表面積 = "<<surfaceArea(rds, hgt, Pi_local())<<endl;

    system("pause");
    return 0;
}
