#include<iostream>
#include<cstdlib>
#include<iomanip>
#include<cmath>
using namespace std;

double pi=3.14;//Global Variable
double Pi_local()//計算Pi的準確值
{
	int remainder,m;
	double n,r,o,p,q;
	n=0;
	r=0;
	m=0;
	for(q=1;q<=100;q=q+1)//計算100項
	{
		o=2*m+1;
		p=1/o;
		m=m+1;
		remainder=m%2;//決定奇偶項
		switch(remainder)
		{
		case 0 ://偶數
			n=n+p;
			break;
		case 1 ://奇數
			r=r+p;
			break;
		}
	}
	cout<<setprecision(8)<<fixed;//顯示π
	return (r-n)*4;
}
double surface_area(double height,double radius)
{
    return (pi*radius*radius)+((pow((height*height)+(radius*radius),(0.5))*2*pi*radius)/2);//計算面積
}

int main()
{
    double height,radius;
    cout<<"Global Variable Pi is : "<<pi<<endl;//global的Pi
    {
        cout<<"Local Variable Pi is : "<<Pi_local()<<endl;//local的Pi
    }
    cout<<"input the height and radius to caculate the area of the cone."<<endl;
    cout<<"Height:";
    cin>>height;
    cout<<"Radius:";
    cin>>radius;
    cout<<"Global Variable area is : "<<surface_area(height,radius)<<endl;//global的area
    {
        pi=Pi_local();
        cout<<"Local Variable area is : "<<surface_area(height,radius)<<endl;//local的area
    }
    system("pause");
    return 0;
}
