#include<iostream>
#include<cstdlib>
#include<iomanip>
#include<cmath>
using namespace std;
double pi=3.14;//initial global pi
double Pi_local(int loop)//function Pi_local
{
    double pi=0;//initialize pi
	for(int i=1;i<=loop;i++)
	{
	   double x=static_cast<double>(i);//let (2*x-1) be a decimal
	   int r=i%2;//determine even or odd
		switch(r)
		{
			case 1://odd
				pi += 4/(2*x-1);//plus
				break;
			case 0://even
				pi -= 4/(2*x-1);//minus
				break;
		}//end switch
	}//end for
	return pi;	
}//end function Pi_local
double surface_area(int height, int radius, double pi)
{
    return pi*radius*radius+sqrt(radius*radius+height*height)*pi*radius;
}
int main()
{
    int height, radius;
    cout << "請輸入高: ";
    cin >> height;
    cout << "請輸入半徑: ";
    cin >> radius;
    cout << "Global Pi = " << surface_area(height,radius,pi) << endl;//global pi
    double pi = Pi_local(100);//initial local pi
    cout << "Local Pi = " << surface_area(height,radius,pi) << endl;//local pi
    system("pause");
    return 0;
}
