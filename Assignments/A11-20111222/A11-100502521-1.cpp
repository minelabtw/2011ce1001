﻿#include <iostream>
#include <cstdlib>
#include <cmath>
using namespace std;
double pi=3.14;					//Global Variable
double  Pi_local()
{
	double total=0;
	for(int i=0;i<100;i++)			//計算Pi by Gregory–Leibniz series
	{
		switch (i%2)
		{
			case 1:
				total=total-4.0/(2*i+1);
				break;
			case 0:
				total=total+4.0/(2*i+1);
				break;
		}
	}
	return total;
	
}
double surface_area(double r,double h,double p)		//計算圓錐表面積
{
	return p*r*r+1.0/2*pow((r*r+h*h),1.0/2)*2*p*r;
}
int main()
{
	int choose=-1;
	double pi,radius,high;
	pi=Pi_local();			 //local variable 
	while(choose!=0)
	{
		system("cls");
		do								
		{
			cout<<"請輸入半徑";
			cin>>radius;
			if(radius<=0)
			{
				cout<<"半徑不得小於等於0\n請重新輸入"<<endl;
			}
		}while(radius<=0);
		do
		{
			cout<<"請輸入高:";
			cin>>high;
			if(high<=0)
			{
				cout<<"高度不得小於等於0\n請重新輸入"<<endl;
			}
		}while(high<=0);
		cout<<"Global pi算="<<surface_area(radius,high,::pi)<<endl;
		cout<<"local pi算="<<surface_area(radius,high,pi)<<endl;
		cout<<"是否要繼續輸入? <0>離開程式 <1>繼續"<<endl;
		cin>>choose;
	}
	cout<<"88~"<<endl;
	system("pause");
	return 0;
}