#include<iostream>
#include<cstdlib>
#include<cmath>//for pow function
using namespace std;

double pi=3.14;//global variable
double Pi_local();
double surface_area(double pi,double r,double h);
int main()
{
    double radius=0;//半徑
    double height=0;//高度
    while(1)
    {
        cout<<"Enter the radius: ";
        cin>>radius;
        cout<<"Eneter the height: ";
        cin>>height;
        if(radius<=0 || height<=0)//當半徑或高度小於等於0，輸出錯誤訊息
            cout<<"The radius or the height can't smaller than 0."<<endl;
        else//否則跳出迴圈
            break;
    }
    cout<<"The global pi = "<<pi<<endl;
    cout<<"The surface calculated by global pi = "<<surface_area(pi,radius,height)<<endl<<endl;
    {//用中括號來使用local pi
        double pi=Pi_local();
        cout<<"The local pi = "<<pi<<endl;
        cout<<"The surface calculated by local pi = "<<surface_area(pi,radius,height)<<endl<<endl;//輸出參數改用Pi_local
    }
    system("pause");
    return 0;
}

double Pi_local()
{
    const int n=100;
    double pi=0;
    for(int i=1;i<=n;i++)
    {
        if(i%2==1)
            pi+=4*(1/(2*static_cast<double>(i)-1));//用static_cast來保持double型別
        else
            pi-=4*(1/(2*static_cast<double>(i)-1));
    }
    return pi;
}

double surface_area(double pi,double r,double h)//r是半徑,h是高度
{
    double surface=0;
    surface=pi*pow(r,2)+0.5*pow(pow(r,2)+pow(h,2),0.5)*2*pi*r;//公式
    return surface;
}
