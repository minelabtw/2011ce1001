#include <cstdlib>
#include <iostream>
#include <cmath>

using namespace std;

double pi = 3.14; // global variable pi
double Pi(double); // function used to calculate pi
double surface_area(double, double, double); // function used to calculate the area of the surface

int main(void){
	double pi_local = Pi(100);
	double height, radius;
	
	cout << "Please enter the height and the radius: ";
	cin >> height >> radius; // enter the arguments
	
	cout << "The surface calculate by Global Variable pi= ";
	cout << surface_area(pi, height, radius) << endl;
	cout << "The surface calculate by Local Variable pi= ";
	cout << surface_area(pi_local, height, radius) << endl;
	
	system("pause");
	return 0;
}

double Pi(double in){ // use the code in Q2
	double answer = 0;
	for(int i = 0; i < in; i++){
		if(i % 2 == 0)
			answer = answer + 1 / (double)(2 * i + 1);
		else
			answer = answer - 1 / (double)(2 * i + 1);
	}
	return 4 * answer;
}

double surface_area(double p, double h, double r){ // use the formula in the hint
	return p * r * r + sqrt(r * r + h * h) * p * r;
}
