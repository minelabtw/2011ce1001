#include<iostream>
#include<cstdlib>
#include<cmath>//for function pow()

using namespace std;

double Pi_local(int);//function prototype

double surface_area(double,double,double);//function prototype

double pi=3.14;//declare a global variable named pi

int main()
{
    double pi=Pi_local(100);//declare a local variable also named pi and use function assign its value
    double height,radius;
    cout<<"Please input cone height: ";
    cin>>height;
    cout<<"Please input cone radius: ";
    cin>>radius;
    if(height<=0||radius<=0)
    {
        cout<<"wrong input";
    }

    else
    {//use the unary scope resolution operator (::) to access global variable when local variable of the same name is in scope
        cout<<"The surface area is(global variable): "<<surface_area(height,radius,::pi)<<endl;
        cout<<"The surface area is(local variable): "<<surface_area(height,radius,pi)<<endl;
    }
    system("pause");
    return 0;

}

double Pi_local(int term)//calculate the pi value to 100 terms with the Gregory�VLeibniz series
{
    double pi_fu=4.0;
    if(term==1)
        return pi_fu;
    else
    {
        int counter=1,odd;
        double k=1.0;
        for(counter;counter<=term-1;counter++,k++)
        {
            odd=counter%2;
            switch(odd)//use odd to determine plus or subtract
            {
                case 1:
                    pi_fu=pi_fu-4/(k*2+1);
                    break;
                case 0:
                    pi_fu=pi_fu+4/(k*2+1);
                    break;
            }
        }
        return pi_fu;
    }
}

double surface_area(double h,double r,double p)//take user��s input calculate the area of the cone��s surface
{
    return p*r*r+0.5*pow(r*r+h*h,0.5)*2*p*r;
}
