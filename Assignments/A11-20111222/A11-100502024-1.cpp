#include <iostream>
#include <cstdlib>
#include <cmath>
using namespace std;

double pi = 3.14; // 設定Global Variable
double Pi_local ();
void surface_area (double,double);

int main ()
{
	int r,high;
	while (true) // 讓while loop 一直跑
	{
		cout << "請輸入要計算圓錐的半徑 (離開請按 0):" << endl;
		cin >> r;
		if (r == 0) // 若輸入0則跳出while loop
		{
			break;
		}
		cout << "請輸入高 :" << endl;
		cin >> high;
		surface_area (r,high); // call surface_area function,並傳入r & high當引數
	}

	system ("pause");
	return 0;
	
}

double Pi_local ()  // 計算Local Variable Pi
{
	double sum = 0;
	int k,c;
	for (double i=0;i<100;i++)
	{
		k = i;
		c = k%2;
		switch (c)
		{
			case 0:
				sum = sum + 4*1/(2*i+1);
				break;
			case 1:
				sum = sum + (-4)*1/(2*i+1);
				break;
		}
	}
	return sum;
}
void surface_area (double R,double High)  // 用Global Variable & Local Variable 來計算圓錐表面積
{
	cout << "The surface area is :" << endl;
	cout << pi*R*R + 0.5*pow (R*R+High*High,0.5)*2*pi*R << endl; // Global Variable pi
	cout << Pi_local ()*R*R+0.5*pow (R*R+High*High,0.5)*2* Pi_local ()*R << endl; // Local Variable Pi
}