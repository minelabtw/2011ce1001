#include<iostream>
#include<cmath>
using namespace std;

double pi = 3.14;  // pi 的 global variable
double Pi_local(int);  //計算 pi 的 local variable
void surface_area(double,double);  //計算圓柱表面積

int main()
{
	double  Radius = 1,Height;  //宣告半徑為Radius , 高為 Height
	cout << "The value of global variable Pi is:" << pi << endl;  // 顯示 pi 的 global variable
	cout << "The value of local variable Pi is:" << Pi_local(100) << endl;  //顯示 pi 的 local variable
	while(Radius != 0)  //當半徑不等於0時,執行while
	{
		cout << "計算圓柱表面積(需大於0)(在半徑中輸入0離開程式)\n輸入半徑:";  //輸入0可離開程式
		cin >> Radius;
		if(Radius ==0)  //當半徑輸入0
		{
			return 0;  //離開程式
		}
		else if(Radius < 0)  //若在半徑輸入小於0的數
		{
			cout << "錯誤!請重新輸入" << endl;  //顯示出錯誤
		}
		else  //若大於0,執行以下程式
		{
			cout << "輸入高度:";
			cin >> Height;
			if(Height <= 0)  //若在高度輸入小於等於0的數
			{
				cout << "錯誤!請重新輸入" << endl;  //顯示出錯誤
			}
			else  //若大於0,執行以下程式
			{
				surface_area(Radius,Height);  //計算分別以 pi 的 global variable ,  pi 的 local variable 計算出的圓柱表面積
			}
		}
	}
}

double Pi_local(int b)  //以 Gregory–Leibniz series 計算第100項的 pi
{
	double Pi = 4.0;
	for(int c = 1;c <= b-1 ; c++)
	{
		int d = c % 2;
		if (d == 0)
		{
			Pi += 4 / ( 2 * static_cast<double>(c) +1 );
		}
	else if (d == 1)
		{
			Pi -= 4 / ( 2 * static_cast<double>(c) +1);
		}
	}
	return Pi;
}

void surface_area(double r,double h)  //計算表面積的函式
{
	cout << "以 global variable Pi 計算出來的表面積為:" << pi * r*(r + pow(pow(r,2)+pow(h,2),0.5) ) << endl;  //以 pi 的 global variable 計算出的圓柱表面積
	cout << "以 local variable Pi 計算出來的表面積為:" << Pi_local(100) * r * ( r + pow( pow(r,2) + pow(h,2) ,0.5 ) ) << endl;  //以pi 的 local variable 計算出的圓柱表面積
}