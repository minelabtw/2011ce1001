#include<iostream>
#include<cstdlib>
#include<cmath>
using namespace std;

double pi=3.14;//set a global variable to store 3.14
double Pi_local();
double surface_area(double,double,double);

int main()
{
    double height,radius,tempt;//set variables to store the inputs and the local pi
    cout<<"Please enter the height and the radius of the cone: "<<endl;
    cin>>height>>radius;
    tempt=Pi_local();//store the result of local pi
    cout<<"The area of the cone��s surface with the Global Variable pi is: "<<surface_area(height,radius,pi)<<endl;
    cout<<"The area of the cone��s surface with the local variable pi is: "<<surface_area(height,radius,tempt)<<endl;
    system("pause");
    return 0;
}

double Pi_local()//function to count the local pi
{
    double odd=0;
    double even=0;
    for(int i=0;i<100;i++)//set a loop to calculate the 100 terms with the Gregory�VLeibniz series
    {
        double hold=i;
        double total=4/(2*hold+1);
        int number=i%2;
        if(number == 0)//set conditions to clarify the complement 0 or 1
        {
            even=even+total;
        }
        else
        {
            odd=odd-total;
        }
    }
    return (even+odd);
}

double surface_area(double h,double r,double p)//function to calculate the area of the cone's surface
{
    double ans=pow((h*h + r*r),0.5);
    return (p*r*r + ans*p*r);
}
