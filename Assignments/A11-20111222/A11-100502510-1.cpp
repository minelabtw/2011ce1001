#include<iostream>
#include<cstdlib>
#include<cmath>
using namespace std;
double pi=3.14;
void Pi_local();
void surface_area();


int main()
{
	cout<<"The global variable Pi="<<pi<<endl;
	double sum=4;//第一項為4
	void Pi_local();
    {

	   int n;
       for( n=1;n<100;n++)//總共一百項
	   {
		   int one;
		   if((n%2)==1)//定義(-1)的k次方
		   {
			   one=-1;
	       }
		   else
		   {
			   one=1;
		   }

		   sum=sum+4.0*one/(2.0*n+1);
       }
    }
	cout<<"Pi calculated by Gregory–Leibniz series is:"<<sum<<endl;
	double r;
	double h;
	double surface1;
	double surface2;
	cout<<"please input radius and high:";
	void surface_area();
	{
		cin>>r>>h;
		surface1=pi*r*r+0.5*pow((r*r+h*h),0.5)*2*pi*r;//global variable的pi所算出來的值
		surface2=sum*r*r+0.5*pow((r*r+h*h),0.5)*2*sum*r;//local的pi所算出來的值

	}
	cout<<"Calculating the area of the cone’s surface with the Global Variable pi:"<<surface1<<endl;
	cout<<"Calculate the area of the cone’s surface with the local variable pi:"<<surface2;
	system("pause");
	return 0;
}
