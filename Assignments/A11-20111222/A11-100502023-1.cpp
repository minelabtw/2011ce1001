#include <iostream>
#include <cstdlib>
#include <cmath>
using namespace std;

double pi=3.14;  //Global Variable pi=3.14

double Pi_local()  //Gregory–Leibniz series
{
	double sum = 0;
	int intI,remainder;

	for (double i=0;i<100;i++)
	{
		intI=i; //將double i 轉換為int I;
		remainder=intI%2;  //搭配下方switch，判斷intI為奇數或偶數

		switch(remainder)  
		{
			case 0:
				sum = sum + 4*1/(2*i+1);  //公式計算
				break;
			case 1:
				sum = sum + (-4)*1/(2*i+1);
				break;
		}
	}

	return sum;
}

void surface_area(double height,double radius) //Calculate the area of the cone’s surface
{

	cout<<"The area of the cone’s surface with the Global Variable pi is "<<pi*pow(radius,2)+0.5*pow(pow(radius,2)+pow(height,2),0.5)*2*pi*radius<<"\n";  //公式計算
	cout<<"The area of the cone’s surface with the local variable pi is "<<Pi_local()*pow(radius,2)+0.5*pow(pow(radius,2)+pow(height,2),0.5)*2*Pi_local()*radius<<"\n";
}

int main()
{
	double height;
	double radius;
	int choice;
	bool enter=true;

	while (enter==true)
	{
		cout<<"請選擇服務:\n";
		cout<<"(1)Calculate the area of the cone’s surface with the Global Variable pi and the local variable pi.\n";
		cout<<"(0)exit\n";
		cin>>choice;

		switch (choice)
		{
			case 1:
				cout<<"Please input the height of the cone :";
				cin>>height;

				if (height<0)
				{
					cout<<"請輸入正數，請再次操作\n";
					break;
				}

				cout<<"Please input the radius of the cone :";
				cin>>radius;

				if (radius<0)
				{
					cout<<"請輸入正數，請再次操作\n";
					break;
				}

				surface_area(height,radius);
				break;

			case 0:
				enter=false;
				break;

			default:
				cout<<"您輸入不合理的代碼，請重新操作\n";
				break;
		}
	}
	
	system("pause");
	return 0;

}