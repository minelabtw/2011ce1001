#include<iostream> //allows program to perform input and outout
#include<cstdlib> //allows program to use system pause
#include<cmath> //contains function prototypes for math library functioin
using namespace std; //enables program to use all the names in any standard C++ header file

double height,radius;
double pi = 3.14; //global variable named pi

// function to calculate the local pi with formula
double Pi_local()
{
    double pi=0; //local variable named pi to use in the function Pi_local
    double d;
    int k , n;
    for (int k=0; k<100; k++) //for loop to display local pi with 100 terms
    {
        switch (k % 2) //switch statement to determine k is even or odd
        {
            case 1: //case that k is odd
                n = -1; //numerator is -1
                break; //exit switch

            case 0: //case that k is even
                n = 1; //numerator is 1
                break; //exit switch
        } //end switch statement

        d = 2 * k + 1; //the formula of denominator
        pi = pi + 4 * (n/d); //the formula of pi
    } //end for loop
    return pi; //return the local pi
} //end function Pi_local

//function to take both global and local pi to calculate the surface area of the cone
double surface_area(double pi,double height,double radius)
{
    return pi*radius*radius+0.5*pow(radius*radius+height*height,0.5)*2*pi*radius;
} //end function surface_area

int main() //function main begins program execution
{
    while(true) //while loop to run program repeatedly
    {
        cout << "Input height and radius to calculate the area of the cone's surface with\n"
             << "the global variable pi and the local variable pi.\n" << endl;
        cout << "Height: ";
        cin >> height; //prompt the user for height
        cout << "Radius: ";
        cin >> radius; //prompt the user for radius
        cout << "The area with global Pi is: " << surface_area(pi,height,radius) << endl; //call the function surface_area and take global as argument
        pi = Pi_local(); //set the value that function Pi_local return to the variable pi
        cout << "The area with local Pi is: " << surface_area(pi,height,radius) << endl; //call the function surface_area and take local as argument

        system ("pause");
        system ("cls");
        //pi = 3.14; //set the pi back to its original number
    } //end while loop
    return 0; //indicate that program ended successfully
} //end function main
