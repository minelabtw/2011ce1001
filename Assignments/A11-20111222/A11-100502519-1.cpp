#include<iostream>
#include<cstdlib>
#include<cmath>

using namespace std;

double pi = 3.14;			// Global的pi
double Pi_local();			//宣告function Pi_local 用算 local的pi
void surface_area(double,double);			//宣告function surface_area 用來算cone的表面積


int main()
{
	int height;
	int radius;
	bool success_flag = false;			//用來判斷是否有正確輸入高和半徑
	bool end_flag = false;			//用來判斷是否要結束
	int choice;

	cout << "Global pi is : " << pi << endl;
	cout << "Local pi is : " << Pi_local() << endl << endl;			//call function Pi_local

	cout << "Start calculate the surface area of cone" << endl;

	while(end_flag==false)			//while 迴圈 使重複使用
	{
		cout << "Enter the height : ";
		cin >> height;

		if(height>=0)			//用來判斷是否輸入正確的高
		{
			cout << "Enter the radius : ";
			cin >> radius;

			if(radius>=0)			//用來判斷是否輸入正確的半徑
			{
				success_flag = true;			//當輸入的高和半徑都正確時 讓success_flag等於true  使進入判斷下一步驟的if
			}
			else
			{
				cout << "\nError.  The radius should >=0" << endl << endl << endl;			//當輸入不正確的半徑時 顯示錯誤訊息
				success_flag=false;
			}
		}
		else
		{
			cout << "\nError.  The height should >=0" << endl << endl << endl;			//當輸入不正確的高時 顯示錯誤訊息
			success_flag=false;
		}

		if(success_flag==true)			//if success_flag等於true  (即輸入的高和半徑都正確時)
		{
			cout << endl;
			surface_area( height , radius );			//call function surface_area

			for(int correct=0;correct!=1;correct+=0)			//讓使用者輸入1或2以外的選項時 能再次選擇
			{
				cout << "_______________________________________________________________________" << endl << endl;
				cout << "Please select next step\n"
					 << "1.Calculate the surface area again\n"
					 << "2.Exit\n"
					 << "you choose : ";
				cin >> choice;

				switch(choice)			//switch 使用者輸入的選項
				{
					case 1:
						end_flag=false;			//使能夠再使用(不給跳出迴圈)
						correct=1;			//選擇正確 使跳出while
						cout << "_______________________________________________________________________" << endl << endl;
						break;
					case 2:
						end_flag=true;			//使結束
						correct=1;			//選擇正確 使跳出while
						cout << "\n\nThanks for use!!" << endl << endl;
						break;
					default:
						correct=0;			//選擇錯誤 不給跳出while
						cout << "\nError!  You can only enter 1 or 2" << endl;
						break;
				}
			}
		}
	}
	

	system("pause");
	return 0;
}



double Pi_local()
{
	int term = 100;
	int numerator=0;			//分子
	double ans=0;			//答案
	int odd_even;			//奇or偶

	while(term>=0)			//while迴圈 使從100降到0
	{
		term=term-1;

		odd_even=term%2;			//除以2的餘數 來判斷奇數or偶數

		numerator=term;

		switch(odd_even)
		{
			case 0:			//餘數=0 表偶數
				numerator = numerator*2+1;			//偶數項的分子
				ans = ans+4.0/numerator;			//答案
				break;
			case 1:			//餘數=1 表奇數
				numerator = (numerator*2+1)*-1;			//奇數項的分子
				ans = ans+4.0/numerator;			//答案
				break;
			default:
				break;
		}
	}

	return ans;			//回傳答案
}


void surface_area( double height , double radius )
{
	double surface_global;
	double surface_local;

	surface_global = pi*radius*radius+0.5*pow(radius*radius+height*height,0.5)*2*pi*radius;			//用global的pi計算表面積的式子
	surface_local = Pi_local()*radius*radius+0.5*pow(radius*radius+height*height,0.5)*2*Pi_local()*radius;			//用local的pi計算表面積的式子

	cout << "The surface area calculated by global pi is : " << surface_global << endl;
	cout << "The surface area calculated by local pi is : " << surface_local << endl << endl;
}