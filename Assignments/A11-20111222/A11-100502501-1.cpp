#include<iostream>
#include<cstdlib>
#include<cmath>
using namespace std;

double pi=3.14; //global variable pi
double Pi_local(double); //function prototype
double surface_area(double,double); //function prototype

int main()
{
	double rad,h;
	cout<<"Please input the radius : ";
	cin>>rad;
	cout<<"Please input the height : ";
	cin>>h;
	//output the surface area of the cone with global variable pi
	cout<<"The surface area of the cone (Global) = "<<pi*surface_area(rad,h);
	{
		double pi=Pi_local(100); //local variable pi which is calculated by function Pi_local()
		//output the surface area of the cone with local variable pi
		cout<<"\nThe surface area of the cone  (Local) = "<<pi*surface_area(rad,h)<<endl;
	}
	system("pause");
	return 0;
}

double Pi_local(double k) //function to calculate pi
{
	double a,dem=1,pi=0;
	for(int i=1;i<=k;i++) //loop for k terms of pi
	{
		a=4/dem;
		dem+=2;
		if(i%2==1)
		{
			pi+=a;
		}
		else
		{
			pi-=a;
		}
	}
	return pi;
}
double surface_area(double radius,double height) //function to calculate surface area of the cone
{
	int surface=radius*(radius+pow((radius*radius+height*height),0.5));
	return surface;
}