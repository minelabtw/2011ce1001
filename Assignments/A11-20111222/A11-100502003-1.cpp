#include <iostream>
#include <cstdlib>
#include <cmath>
using namespace std;

// prototype of functions
double Pi_local();
void surface_area(double height, double radius);

double pi = 3.14; // global valuable of pi
int main()
{
    double pi = Pi_local();
    double Height, Radius;
    int stop = 1; // valuable for diciding whether the program should continue

    while(stop != 0)
    {
        cout << "Please enter the height of the cone: ";
        cin >> Height;
        if(Height<0) // height can't be a negative number
        {
            cout << "Wrong accept! Please enter a positive number!!" << endl;
            cout << "Do you still want to continue?\n"
                << "If yes, enter 1. If not, enter 0.\n"
                << "If you enter other numbers that are not 0, it will still continue!!" << endl;
            cin >> stop;
            continue;
        }
        cout << "Please enter the radius of the cone: ";
        cin >> Radius;
        if(Radius<0) // radius can't be a negative number
        {
            cout << "Wrong accept! Please enter a positive number!!" << endl;
            cout << "Do you still want the program to run?\n"
                << "If yes, enter 1.(It will run from the begining) If not, enter 0.\n"
                << "If you enter other numbers that are not 0, the program will still run!!" << endl;
            cin >> stop;
            continue;
        }
        surface_area(Height, Radius); //  call function to show the result
        cout << "Do you still want to continue?\n"
                << "If yes, enter 1. If not, enter 0.\n"
                << "If you enter other numbers that are not 0, it will still continue!!" << endl;
        cin >> stop;
    }

    system("pause");
    return 0;
}

double Pi_local() // function for the local valuable of pi
{
    double even_i, even_value=0;
    double odd_j, odd_value=0;

    for(int i=0;i<100;i+=2) // count the total answer for those who are positive
    {
        even_i = 4.0/(2*i+1); // each member that is positive for pi
        even_value += even_i; // add those members
    }
    for(int j=1;j<100;j+=2) // count the total answer for those who are negative
    {
        odd_j = 4.0/(2*j+1); // each member that is negative for pi
        odd_value -= odd_j; // add those members
    }
    return even_value + odd_value; // return the value of pi(local)
}

void surface_area(double HEIGHT, double RADIUS) //  function for counting the area of the cone
{
    double POW_ANS = pow(RADIUS*RADIUS+HEIGHT*HEIGHT, 0.5);
    cout << "The answer counted by global_pi is: "
        << pi*RADIUS*RADIUS+0.5*POW_ANS*2*pi*RADIUS << endl; // use global valuable of pi
    cout << "The answer counted by local_pi is: "
        << Pi_local()*RADIUS*RADIUS+0.5*POW_ANS*2*Pi_local()*RADIUS << endl; // use local valuable of pi
}


