#include<iostream>
#include<cstdlib>
#include<cmath>
#include<iomanip>
using namespace std;

double taxi;
double const pi=3.14;
double Pi_local(int);
double GBsurface_area(float,float);
double LCsurface_area(float,float);
int main()
{
 int radius;
 int height;

 cout<<"Global value of pi = "<<pi<<"\n Local value of pi = "<<Pi_local(100);
 cout<<"\n\n請輸入半徑: ";
 cin >>radius;
 cout<<"\n請輸入高度: ";
 cin >>height;
 cout<<"\nsuface area of the cone -> "<<setw(8)<<GBsurface_area(radius,height)<<" ( Global value of pi )";
 cout<<"\n                        -> "<<setw(8)<<LCsurface_area(radius,height)<<" (  Local value of pi )";
 system("pause");
 return 0;
}

double Pi_local(int item)//refer to my hw.6
{
 double pi=0;
 double array[1001];
 int a,even;
 double denomi;

 for(a=1,denomi=1 ; a<=1000,denomi<=2001 ; a++,denomi=denomi+2)
    {
     array[a]=1/denomi;
    }
 for(even=2;even<=1000;even=even+2)
    {
     array[even]=array[even]*(-1);
    }
 for(a=1;a<=item;a++)
    {
     pi=pi+array[a];
    }
 pi=pi*4;
 taxi=pi;//sent the local value pi to the global value taxi
 return pi;
}

double GBsurface_area(float height,float radius)
{
 float h=height;
 float r=radius;
 float sfar;
 sfar=pi*r*r+pi*r*pow((r*r+h*h),0.5);
 return sfar;
}

double LCsurface_area(float height,float radius)
{
 double pi=taxi;//sent the global value taxi to the local value pi
 float h=height;
 float r=radius;
 float sfar;
 sfar=pi*r*r+pi*r*pow((r*r+h*h),0.5);
 return sfar;
}
