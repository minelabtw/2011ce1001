#include<iostream>
#include<cstdlib>
#include<cmath>
using namespace std;

double pi = 3.14; //global variable
double Pi_local(); // calculate pi
double surface_area( double , double , double ); // calculate surface area

int main()
{
    double lopi = Pi_local(); // define local pi variable
    double height,radius; // the height and radius of cone
    cout << "Please input the height and the radius :" << endl;
    cin >> height >> radius;
    cout << "the global pi cone surface area is :" <<surface_area(pi,height,radius)<<endl;
    cout << "the local pi cone surface area is :" <<surface_area(lopi,height,radius);
    system("pause");
    return 0;
}

double Pi_local() // calculate pi function
{
    double pi=0; //initial value
    double answer; // each of answer
    for(int b=0;b<=99;b++)
    {
        answer = 4.0/(2*b+1);
        if(b%2 == 0) // b除2之餘數=0
            pi += answer;
        else   // b除2之餘數=1
            pi -= answer;
    }
    return pi;
}

double surface_area(double pi,double height,double radius) // calculate surface area function
{
    double area;
    area = pi*radius*radius+pi*radius*pow(radius*radius+height*height,0.5); // the formula
    return area;
}
