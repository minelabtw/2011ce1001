#include <iostream>
#include <math.h>
#include <cstdlib>
using namespace std;

double pi=3.14;

int add_or_minus(int i) {//決定+- 偶數是+ 負數是- 
   switch(i%2) {
      case 0: return 1; break;
      case 1: return -1; break;
   }
}

double Leibniz() {    //利用add_or_minus回傳的正負號計算pi值 
   double pi=0;
   int count=0;
   while(count<100) {
      pi = pi + 4*( add_or_minus(count) / (double)( 2*(count)+1 ) );
      count++;
   }
   return pi;
}

void surface_area(int height, int radius) {
   double area1 = pi * pow(radius,2) + 1/2 * pow( pow(radius,2) + pow(height,2) , 0.5 ) * 2 * pi * radius;
   double area2 = Leibniz() * pow(radius,2) + 1/2 * pow( pow(radius,2) + pow(height,2) , 0.5 ) * 2 * Leibniz() * radius;
   cout<<"area use global variable pi is : "<<area1<<endl;
   cout<<"area use function Leibniz() is : "<<area2<<endl;
} 

int main() {
   int h, r;
   cout<<"Enter the height : ";
   cin>>h;
   cout<<"Enter the radius : ";
   cin>>r;
   surface_area(h,r);
   system("pause");
   return 0;
}
