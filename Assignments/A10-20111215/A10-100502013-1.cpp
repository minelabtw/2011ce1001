#include<iostream>
#include<cstdlib>
#include<iomanip>
using namespace std;

double power(double base,int exponent) //use recursive function to calculate base^exponent
{
	if(exponent==1)
	{
		return base;
	}
	else
	{
		return base * power(base,exponent-1);
	}
}

int reFibonacci(int n) //use recursive function to find Fibonacci
{
	if(n==1)
	{
		return 0;
	}
	else if(n==2)
	{
		return 1;
	}
	else
	{
		return reFibonacci(n-1) + reFibonacci(n-2);
	}
}

void nonreFibonacci() //another way to find Fibonacci (nonrecursive)
{
	int n=3; //begin with the third Fibonacci number
	int fibon01=0;
	int fibon02=1;
	int fibonsave; //save the final correct number
	cout << "Fibonacci(  1 ): " << fibon01 << endl;
	cout << "Fibonacci(  2 ): " << fibon02 << endl;
	while(fibon01>=0 && fibon02>=0) //add to each other in turn. when the number is negative,it is overflowed and should be stopped.
	{
		if(n%2==1) //n is odd. add fibon02 to fibon01
		{
			fibon01 = fibon01 + fibon02;
			if(fibon01>0)
			{
				fibonsave = fibon01;
				cout << "Fibonacci(" << setw(3) << n << " ): " << fibon01 << endl;
			}
			else
			{
				cout << "Fibonacci(" << setw(3) << n << " ): " << "The number is overflow.\n\n";
			}
		}
		else //n is even. add fibon01 to fibon02
		{
			fibon02 = fibon02 + fibon01;
			if(fibon02>0)
			{
				fibonsave = fibon02;
				cout << "Fibonacci(" << setw(3) << n << " ): " << fibon02 << endl;
			}
			else
			{
				cout << "Fibonacci(" << setw(3) << n << " ): " << "The number is overflow.\n\n";
			}
		}
		n++;
	}
	cout << "The largest integer Fibonacci number that can be printed on this computer: \n";
	cout << "Fibonacci(" << setw(3) << n-2 << " ): " << fibonsave << "\n\n\n"; //output the largest Fibonacci number
}

int choose=1;
double mybase;
int myexponent;
int myfibon;
int main() 
{
	cout << "Enter 1 to calulate power.\n";
	cout << "Enter 2 to find a certain number of a Fibonacci.\n";
	cout << "Enter 3 to find the largest number of Fibonacci that this computer can show.\n";
	cout << "Enter 0 to exit.\n\n\n";
	while(choose!=0) //when choose = 0 ,  exit
	{
		choose = 1;
		cout << "What do you want to do: ";
		cin >> choose;
		switch(choose) //use switch to choose a function
		{
			case 1:
				cout << "Please enter a base number: ";
				cin >> mybase;
				cout << "Please enter a exponent number: ";
				cin >> myexponent;
				if(myexponent>=1)
				{
					cout << mybase << " ^ " << myexponent << " = " << power(mybase,myexponent) << "\n\n";
				}
				else
				{
					cout << "The exponent should be larger than 1 or equal to 1.\n\n";
				}
				cout << endl;
				break;
				
			case 2:
				cout << "Please enter a positive number: ";
				cin >> myfibon;
				if(myfibon<0)
				{
					cout << "This number is invalid.\n\n\n";
				}
				else
				{
					cout << "Fibonacci( " << myfibon << " ) : " << reFibonacci(myfibon) << "\n\n\n";
				}
				break;
				
			case 3:
				nonreFibonacci();
				break;
				
			case 0:
				break;
				
			default: //invalid number
				cout << "This is invalid.\n\n\n";
				break;
		}
	}
	system("pause");
	return 0;
}
