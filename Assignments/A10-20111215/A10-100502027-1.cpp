#include<iostream>
#include<cstdlib>
using namespace std;

double power(double,int);
unsigned long Fibonacci(unsigned long);
void nrFibonacci();

int main()
{
    int switchstate=0;
    int switchchoice=0;
    double base =0;
    int exponent =0,number=0;
    while(switchstate!=1)
    {
        cout<<"請輸入指令(1:power,2.Fibonacci,3.nrFibonacci,4.Exit):";
        cin>>switchchoice;
        switch(switchchoice) //switch-動作選擇
        {
            case 1:
                cout<<"請輸入base:";
                cin>>base;
                cout<<"請輸入exponent[>=1]:";
                cin>>exponent;
                while(exponent<1)//指數正整數判斷
                {
                    cout<<"錯誤，exponent請輸入>=1的數：";
                    cin>>exponent;
                }
                cout<<base<<"^"<<exponent<<"為："<<power(base,exponent)<<endl;
                break;

            case 2:
                cout<<"請輸入項數:";
                cin>>number;
                while(number<1) //項數正整數判斷
                {
                    cout<<"請輸入正整數"<<endl;
                    cin>>number;
                }
                cout<<"Fibonacci第"<<number<<"項為："<<Fibonacci(number-1)<<endl;
                break;

            case 3:
                nrFibonacci();
                break;

            case 4:
                switchstate=1;//啟動開關 結束迴圈
                break;

            default:
                cout<<"輸入指令錯誤，請重新輸入"<<endl;
                break;
        }
    }
    system("pause");
    return 0 ;
}

double power(double base , int exponent)//遞迴至指數為1時，即X1
{
    if(exponent<1)
    {
        return 1;
    }
    else
    {
        return base * power(base ,exponent-1);
    }
}

unsigned long Fibonacci(unsigned long number) //遞迴加法
{
    if(number==0||number==1)
    {
        return number;
    }
    else
    {
        return Fibonacci(number-1)+Fibonacci(number-2);
    }
}

void nrFibonacci()//以for迴圈計算費氏
{
    int Fibonacciuse1=0,Fibonacciuse2=1,Fibonaccicount=0;
    cout<<"Fibonacci-1："<<Fibonacciuse1<<endl;
    cout<<"Fibonacci-2："<<Fibonacciuse2<<endl;
    for(int n=3;Fibonaccicount>=0;n++)
    {
        Fibonaccicount=Fibonacciuse1+Fibonacciuse2;
        if(Fibonaccicount<0)//當數值溢位時即中斷離開
        {
        break;
        }
        cout<<"Fibonacci-"<<n<<"："<<Fibonaccicount<<endl;
        Fibonacciuse1=Fibonacciuse2;
        Fibonacciuse2=Fibonaccicount;
    }
}
