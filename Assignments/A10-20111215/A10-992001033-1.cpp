#include<iostream>
#include<cstdlib>
#include<iomanip>
using namespace std;

double power(double base,int exponent)
{
    if(exponent==1)//次方1，則為自己
        return base;
    else//recursive
        return power(base,exponent-1)*base;
}
int Fibonacci(int n)
{
    while(1)
    {
        if(n==1)//第一項
            return 0;
        else if(n==2)//第二項
            return 1;
        else if(n>2)//recursive
            return Fibonacci(n-2)+Fibonacci(n-1);
        else//輸入小於0，需要重新輸入
            cout<<"Please enter the n>0."<<endl;
            cout<<"Enter new n: ";
            cin>>n;
    }
}
int nrFibonacci()
{
    int Fibonacci=0;
    int F1=0;//第一項
    int F2=1;//第二項
    for(int i=1;i>=1;i++)
    {
        if(i==1)
            Fibonacci+=0;
        else if(i==2)
            Fibonacci+=1;
        else
            if(i%2==1)//如果為奇數項，則加總並放成新的第一項
            {
                Fibonacci=F1+F2;
                F1=Fibonacci;
            }
            else if(i%2==0)//如為偶數項，則加總並放乘新的第二項
            {
                Fibonacci=F1+F2;
                F2=Fibonacci;
            }
        if(Fibonacci>=0)//沒overflow
            cout<<setw(5)<<i<<setw(15)<<Fibonacci<<endl;
        else//overflow
        {
            cout<<setw(5)<<i<<setw(15)<<Fibonacci<<endl;
            if(F1>F2)//用此來判斷overflow前一項是哪個
                cout<<"The largest int Fibonacci number is "<<F1<<endl;
            else
                cout<<"The largest int Fibonacci number is "<<F2<<endl;
            return 0;
        }
    }
}
int main()
{
    int choice=0;
    double base=0;
    int exponent=0;
    int n=0;
    while(1)
    {
        cout<<"1.power       fuction"<<endl;
        cout<<"2.Fibonacci   function"<<endl;
        cout<<"3.nrFibonacci function"<<endl;
        cout<<"4.Exit"<<endl;
        cout<<"Enter the number(1~4): ";
        cin>>choice;
        switch(choice)
        {
        case 1:
            cout<<"Enter the base: ";
            cin>>base;
            cout<<"Enter the exponent: ";
            cin>>exponent;
            cout<<"The power is "<<power(base,exponent)<<endl;
            break;
        case 2:
            cout<<"Enter the n: ";
            cin>>n;
            cout<<"The Fibonacci is "<<Fibonacci(n)<<endl;
            break;
        case 3:
            nrFibonacci();
            break;
        case 4:
            return 0;
        default://限定輸入
            cout<<"Enter 1、2、3 or 4."<<endl;
            break;
        }
    }
}
