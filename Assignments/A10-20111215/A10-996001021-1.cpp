#include <iostream>
#include <cstdlib>
using namespace std;

int power(int,int);
int Fibonacci(int);
void nrFibonacci(int * const, int * const,int * const);

int main(){
	int exponent;
	int Fi;
	int base,exp,result;
	int choice;
	int term;
	int first=0;
	int second=1;
	do
	{
		cout << "What do you wanna do\n"; //顯示選項
		cout << "Enter 1 to do power function\n";
		cout << "Enter 2 to do Fibonacci function\n";
		cout << "Enter 3 to do nrFibonacci function\n";
		cout << "Enter else to Exit\n";
		cin >> choice;

		switch (choice)//用switch判斷做的選擇
		{
			case 1://指數
				{	
					cout << "Enter the base value: \n";
					cin >>base;
					cout<<"Enter the exponent value: \n";
					do//判斷錯誤
					{
						cin>>exp;
						if (exp <1)
						{
							cout << "Worng! Please Enter again\n";
							exponent=1;
						}
						else
							exponent=0;
					}
					while(exponent);
					result=power(base,exp);//呼叫函式
					cout << "result=" << result << endl;
					break;
				}
			case 2://費式數列
				{
					cout << "How much term do you want \n";
					do
					{
						cin>>term;
						if (term <1)//判斷錯誤
						{
							cout << "Worng! Please Enter again\n";
							Fi=1;
						}
						else
							Fi=0;
					}
					while(Fi);
					result=Fibonacci(term);//呼叫函式
					cout <<"result=" <<result << endl;
					break;
				}
			case 3:
				{
					int i=1;
					cout << "term" << "\t" << "vaule" << endl;
					do
					{
						cout << i << "\t" << first << endl;//輸出結果
						nrFibonacci(&first,&second,&result);//呼叫函式
						i++;
						if (first < 0)//結束判定
							break;
					}
					while(1);
					break;
				}
			default://離開
				{
					choice=0;
					break;
				}
				
			
		}

	}
	while(choice);
				system("pause");
				return 0;
	}
int power(int m, int n)
{
	if(n==1)
		return m;//當n＝1，回傳結果
	else 
	{
		n--;//乘的次數減少
		return m*(power(m,n));
	}

}
int Fibonacci(int t)
{
		if (t==1)		
			return 0;
		else if (t==2)
			return 1;
		else 
		{
			t--;//項數減少
			return Fibonacci(t)+Fibonacci(t-1);
			
		}
}
void nrFibonacci(int *const aPtr , int *const bPtr,int * const cPtr)
{
	*cPtr=*aPtr+*bPtr;//第n項跟第n+1項相加
	*aPtr=*bPtr;//第n項變第n+1項
	*bPtr=*cPtr;//第n+1項變第n+2項
}