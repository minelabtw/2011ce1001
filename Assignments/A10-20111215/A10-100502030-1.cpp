#include<iostream>
#include<cstdlib>
using namespace std;

double power(double base,int exponent)
{
    if(exponent==0)
        return 1;
    else
        return base*power(base,exponent-1);
}// 計算某數的次方

int Fibonacci(int n)
{
    if(n==1)
        return 0;
    else if(n==2)
        return 1;
    else
        return Fibonacci(n-1)+Fibonacci(n-2);
}// 算出費氏數列第n項的值(recursive function)

int nrFibonacci(int n)
{
    int next=1,now=0,a;
    for(int i=0; i<n-1; i++)
    {
        a=next;
        next=next+now;
        now=a;
    }
    if(n==1)
        return 0;
    else
        return now;
}// 算出費氏數列第n項的值(non-recursive function)

int main()
{
    int exit=0;
    int choice;
    double base;
    int expo;
    int number;

    while(1)
    {
        cout<<"Please choose the function you want to use.\n"
            <<"1.power function\n"
            <<"2.Fibonacci function\n"
            <<"3.nrFibonacci function\n"
            <<"4.exit\n";//告知使用者要輸入的東西
        cin>>choice;
        while(choice!=1 && choice!=2 && choice!=3 && choice!=4)
        {
            cout<<"It is a error input.\n"
                    <<"Please input again\n";
            cin>>choice;
        }// 判斷輸入是否有效
        if(choice==4)
            break;// 離開
        switch(choice)
        {
            case 1:// 功能1
            cout<<"Please input base and exponent:";
            cin>>base>>expo;
            while((base==0 & expo==0) or expo<0)
            {
                cout<<"It is not a reasonable operation.\n"
                    <<"Please input again\n";
                cin>>base>>expo;
            }// 當0的0次方時無效
            cout<<"("<<base<<")^"<<expo<<"="<<power(base,expo)<<endl;// 輸出結果
            break;

            case 2:
            cout<<"Please input a number:";
            cin>>number;
            while(number<=0)
            {
                cout<<"It is a error input.\n"
                    <<"Please input again:";
                cin>>number;
            }// 當輸入負的項數無效
            cout<<"Fibonacci("<<number<<")="<<Fibonacci(number)<<endl;
            break;

            case 3:
            int i;
            for(i=1; nrFibonacci(i)>=0; i++)
            {
                cout<<"nrFibonacci("<<i<<")="<<nrFibonacci(i)<<endl;
            }// 輸出所有電腦可容許的值
            cout<<"The largest integer can be input is "<<(i-1)<<endl;
            break;
        }
        system("pause");
        system("cls");
    }
    return 0;
}












