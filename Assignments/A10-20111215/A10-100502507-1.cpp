/*Assignment 10
100502507-�L�l�W*/
#include <iostream>
#include <cstdlib>
using namespace std;

class start
{
public:
	start( int a )
	{
		judge = 1;//Execution condition
		while( judge == 1 )
		{
			cout << "Please enter\n"
				<< "0 to exit\n"
				<< "1 for power\n"
				<< "2 for Fibonacci(recursive)\n"
				<< "3 for Fibonacci(non-recursive): ";
			cin >> choice;
			switch( choice )
			{
				case 0://Exit
					judge = 0;
					break;
				case 1://Power function
					cout << "Enter base please: ";
					cin >> Base;
					if( Base < 0 )//Avoid error
					{
						cout << "Incorrect number entered!\n";
						break;
					}
					cout << "Enter exponent please: ";
					cin >> Exponent;
					if( Exponent < 0 )//Avoid error
					{
						cout << "Incorrect number entered!\n";
						break;
					}
					cout << "The result is: " << power( Base, Exponent ) << endl;
					system("pause");
					system("cls");
					break;
				case 2://Fibonacci function(recursive)
					cout << "Please enter n: ";
					cin >> n;
					if( n < 0 )//Avoid error
					{
						cout << "The n you entered will cause error take place, please try again!\n";
					}
					else
					{
						cout << "The result is: " << Fibonacci( n ) << endl;
					}
					system("pause");
					system("cls");
					break;
				case 3://Fibonacci function(non-recursive)
					cout << "The result is: ";
					nrFibonacci();
					system("pause");
					system("cls");
					break;
				case '\t':
				case '\n':
				case ' ':
					break;
				default:
					cout << "Incorrect number input!!\n";
					break;
			}
		}
	}

	int power( int base, int exponent )//Power function
	{
		if( exponent <= 1 )//Stop condition
		{
			return base;
		}
		else
		{
			return base*power( base, exponent - 1 );
		}
	}

	unsigned long Fibonacci( unsigned long n )//Fibonacci function(recursive)
	{
		if( (n == 0) || (n == 1) )
		{
			return n;
		}
		else
		{
			return Fibonacci( n - 1 ) + Fibonacci( n - 2 );
		}
	}

	void nrFibonacci()//Fibonacci function(non-recursive)
	{
		first = second = 0;
		for( unsigned int i=0;; i++ )
		{
			if( i == 0 )
			{
				first = 0;
			}
			else if( i == 1 )
			{
				second = 1;
			}
			else
			{
				int hold;//A variable to store the previous variable "second" as the current variable "first"
				hold = second;
				second = second + first;//Produce the current variable "second"
				if( second < 0 || second < first )//Stop condition
				{
					second = hold;//Revert to the pervious "second"
					cout << ", and it's the largest int Fibonacci number that can be printed on your computer!" << endl;
					break;
				}
				first = hold;//Produce the current "first"
			}
			cout << "F(" << i << ") = " << second << endl;
		}
	}
private:
	int judge;//Variable let user decide to continue or not
	int choice;//Variable let user decide which function to execute
	unsigned int n;//Variable Fibonacci function needs
	unsigned int Base;//Variable Power function needs
	unsigned int Exponent;//Variable Power function needs
	unsigned int first;//Variable Fibonacci function needs
	unsigned int second;//Variable Fibonacci function needs
};

int main()//Function main begins execution
{
	cout << "Are you ready??\n";
	system("pause");
	system("cls");
	start Start( 1 );//Call class start
	return 0;
}//End function main