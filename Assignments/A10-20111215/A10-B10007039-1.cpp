#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

double power(double,int);
int Fibonacci(int);
void nrFibonacci();

int main(int argc, char *argv[]){
    float base=0;
    int WhileNoBreak = 1,exponent = 1,N=0;

    while(WhileNoBreak){
        cout << "------Function Menu------" << endl;
        cout << "1.Recursive function power(base, exponent)" << endl;
        cout << "2.Recursive function Fibonacci(n)" << endl;
        cout << "3.Non-Recursive function nrFibonacci()" << endl;
        cout << "4.Exit" << endl;
        cout << "-------------------------" << endl;
        cout << "Choose:";
        switch(getchar()){
              case '1'://第一題
                   cout << "Base:";
                   cin >> base;
                   cout << "Exponent:";
                   cin >> exponent;
                   cout << "Anser:" << power(base,exponent) << endl;
                   system("PAUSE");
                   break;
              case '2'://第二題
                   cout << "N:";
                   cin >> N;
                   
                   //數字過大計算時間會比較久的提示 
                   if(N > 39)cout << "The number N is large,maybe take some time." << endl;
                   
                   cout << "Anser:" << Fibonacci(N) << endl;
                   system("PAUSE");
                   break;
              case '3'://第三題 
                   nrFibonacci();
                   system("PAUSE");
                   break;
              case '4'://離開 
                   WhileNoBreak = 0;
                   break;
              default:
                   cout << "Do not have this option, please try again." << endl;
                   system("PAUSE");
                   break;                      
        }
        getchar();
    }
    return EXIT_SUCCESS;
}

double power(double base,int exponent = 1){//第一題
          
          double Anser=1;
          
          //定義0次方為1
          if(exponent == 0)return 1;
          
          //計算底數乘與前次方積 
          Anser = base * power(base,exponent-1);
          return Anser;
          
}

int Fibonacci(int Number){//第二題
    if(Number > 0){
        //定義第一二項 
        if(Number == 1)return 0;
        if(Number == 2)return 1;
        
        int Anser;
        //呼叫本身計算前兩項 
        Anser = Fibonacci(Number-1)+Fibonacci(Number-2);

        return Anser;
    }
    return 0;
}
void nrFibonacci(){//第三題 
     
     int ForLevel1=0;
     int Number1=0,Number2=1;
     int Test=0,Large=0,Counter=1;
     
     while(Test>=0){
           //定義第一項第二項第三項 
           if(Counter == 1)Number2 = 0;
           if(Counter == 2)Number2 = 1;
           if(Counter == 3)Number1 = 0;
           
           //計算前兩項相加 
           Test = Number1 + Number2;
           
           //項目取代 
           Number1 = Number2;
           Number2 = Test;
           
           //檢查是否overflow，為真則不記錄並跳出 ，否則紀錄並計數器增加數值和輸出螢幕 
           if(Test >= 0){
                   Large = Test;
                   cout << "No." << setw(2) << Counter << " " << setw(12) << Large << endl;
                   Counter++;
           }
           
     }
     
     
}
