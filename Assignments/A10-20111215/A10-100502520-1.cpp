#include <iostream>
#include <cstdlib>
using namespace std;

double power(double,int);
int fibonacci(int);
void nrFibonacci();

int main()
{
	int bas,ind,choose,jud=1;
	int number;

	while(jud==1) //重複選擇
	{
		choose=0;
		cout<<"請選擇功能: \n";
		cout<<"1.求次方\n";
		cout<<"2.求費氏數列\n";
		cout<<"3.求費事數列的極限\n";
		cout<<"4.離開\n";
		cin>>choose;
		switch(choose)
		{
			case 1: 
				cout<<"底數: ";
				cin>>bas;
				cout<<endl;
				cout<<"指數: ";
				cin>>ind;
				cout<<endl;
				cout<<"所求為: "<<power(bas,ind)<<endl<<endl; //呼叫次方函式
				break;
			case 2:
				cout<<"請輸入第幾位: ";
				cin>>number; 
				cout<<"所求為: "<<fibonacci(number-1)<<endl<<endl; //呼叫費氏數列函式
				break;
			case 3:
				nrFibonacci(); //呼叫費氏數列極限的函式
		}
	}
	return 0;
}

double power(double base,int index=1)
{

	if(index==0) //指數若為0回傳1
	{	
		return 1;
	}
	else 
	{
		return (power(base,index-1)*base);  //以遞迴做計算
	}
	
}

int fibonacci(int numb)
{
	if(numb==0)  //第一項為0
	{
		return 0;
	}
	else if(numb==1) //第二項為1
	{
		return 1;
	}
	else
	{
		return (fibonacci(numb-1)+fibonacci(numb-2));  //以遞迴做相加
	}
}

void nrFibonacci()
{
	int numb1=0,numb2=1,result;
	
	for(int i=0;i>=0;i=result)  //值若還沒溢位就繼續計算
	{
		result=numb1+numb2;
		numb1=numb2;
		numb2=result;

		if(result>0)
			cout<<result<<endl;
	}
}
