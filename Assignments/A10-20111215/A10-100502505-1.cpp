#include<iostream>
#include<cstdlib>
using namespace std;

int power(int base,int exponent)
{
    if(exponent==0)
    {
        return 1;    
    }else if(exponent==1){
        return base;    
    }else{
        return power(base,exponent-1)*base;
    }    
}//算次方 

int Fibonacci(int n)//n是第幾項 
{
    if(n==0)
    {
        return 0;
    }else if(n==1){
        return 1;    
    }else{
        return Fibonacci(n-1) + Fibonacci(n-2);
    }   
}//算費氏數列 

void nrFibonacci()
{
    for(int i=0;;i++)
    {
        if(Fibonacci(i)<0)
        {
            break;    
        }     
        cout << "Fibonacci(" << i << ")=" <<Fibonacci(i) << endl;
    }    
}//顯示費氏數列 

int main()
{
    int number,base,exponent,n;
    for(;;) 
    {
        cout << "請問要第幾個選項? 1.算次方 2.費氏數列的值 3.列出所有費氏數列 4.離開 :";
        cin >> number;
        switch(number)//選要哪一個 
        {
            case 1:cout << "請輸入數字和次方:";
                   cin >> base >> exponent;
                   cout << power(base,exponent) << endl;  
                   break;
            case 2:cout << "請輸入你要第幾項:";
                   cin >> n; 
                   cout << Fibonacci(n) << endl;
                   break;
            case 3:nrFibonacci();
                   break;
            case 4:system("pause");
                   return 0;
            default:cout << "請重新輸入!" << endl;
        }
    }    
}//完成! 
