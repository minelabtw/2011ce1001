#include<iostream>
#include<cstdlib>
#include<iomanip>
using namespace std;

double power(double,int);
unsigned long Fibonacci(unsigned long);
void nrFibonacci();

int main()
{   
 int Item=1;
 unsigned long item;
 double base;
 int part;
 int exponent;   
 int e;
 
 while(Item==1||Item==2||Item==3)
    {
     cout<<"Key in 1 or 2 or 3 to choose which function you want to excute.\nKey in 0 to exit.\n\n";
     cin >>Item;             
     switch(Item)
        {
         case 1:
            cout<<"Please enter a base and an exponent.\n";
            cin >>base>>exponent;
            if (exponent==0&&base==0)//0^0 be defined as "nonsense".
               {
                cout<<"無意義\n\n";                     
               }
            if (exponent==1||exponent==0)
               {           
                cout<<base<<"^"<<exponent<<" = "<<power(base,exponent)<<"\n\n";
               }
            else
               {
                cout<<base;                             
                for(e=1 ; e<=exponent-1 ; e++)
                   {
                    cout<<"*"<<base;    
                   }
                cout<<"="<<power(base,exponent)<<"\n\n";                                                                
               }   
            break;
            
         case 2:
            cout<<"Choose which item you want to know in Fibonacci.\n";
            cin >>item;
            cout<<item<<" th = "<<Fibonacci(item-1)<<"\n\n";
            break;
               
         case 3:      
            cout<<"\n\n";
            nrFibonacci();
            cout<<"\n\n";              
            break;                     
        }
    }       
 system("pause");
 return 0;   
}

double power(double b,int exp)
{
 if (exp==0) 
    return 1; 
 else
    return b*power(b,exp-1);//b*b (遞迴)
   
}
    
unsigned long Fibonacci(unsigned long fib)
{
 if ((fib==0)||(fib==1))
    return fib;
 else
    return Fibonacci(fib-1)+Fibonacci(fib-2);//f(n)=f(n-1)+f(n-2)(遞迴)                   
}
   
void nrFibonacci()
{
 int a;             
 int nrfib[50];
 nrfib[1]=0;
 nrfib[2]=1;
 nrfib[3]=1;  
     cout<<"  1 st = 0\n  2 nd = 1\n  3 rd = 1\n";        
 for(a=4;a<=50;a++)
    {
     nrfib[a]=nrfib[a-1]+nrfib[a-2];
     if (nrfib[a]<0)//超載的話就跳出迴圈 
        break;
     else
        cout<<setw(3)<<a<<" th = "<<nrfib[a]<<"\n";//顯示各項值                       
    }                 
}
