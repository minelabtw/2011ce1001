#include <iostream>
#include <cstdlib>
using namespace std;

int power(int base, int exponent)  // (recursive function) power : base^exponent
{	
	if (exponent==0)
	{
		return 1;
	}
	else
	{
		return base*power(base,exponent-1);
	}
}

int Fibonacci(int n)  //(recursive function) Fibonacci
{
	int result=0;

	if (n==1)
	{
		return 0;
	}
		
	else if(n==2)
	{
		return 1;
	}

	else
	{	
		return result=Fibonacci(n-1)+Fibonacci(n-2);
	}
}

void nrFibonacci()  //(non-recursive function) Fibonacci
{
	int Fib[100000];

	Fib[0]=0;
	Fib[1]=1;

	for (int i=0;i<100000;i++)
	{
		if (i==0)
		{
			cout<<"第1項 "<<Fib[0]<<"\n";
		}

		else if (i==1)
		{
			cout<<"第2項 "<<Fib[1]<<"\n";
		}

		else
		{
			Fib[i]=Fib[i-1]+Fib[i-2];

			if (Fib[i]<0)
			{		
				cout<<"在這台電腦裡，費式數列中第"<<i<<"項為 the largest int Fibonacci number="<<Fib[i-1]<<"\n";
				break;
			}

			else
			{
				cout<<"第"<<i+1<<"項 "<<Fib[i]<<"\n";
			}
		}

		
	}
}
int main()
{
	int choice=9;//使用者輸入的選項代碼;初始為9，以進入while迴圈
	
	while (choice!=0)
	{	
		cout<<"請選擇服務: (請輸入代碼)\n";
		cout<<"(1)compute base^exponent\n";
		cout<<"(2)show Fibonacci Series\n";
		cout<<"(3)show the largest int Fibonacci number that can be printed on your computer\n";
		cout<<"(0)exit\n";

		cin>>choice;

		switch(choice)
		{
			case 1:  //compute base^exponent
				int base,exponent;
				cout<<"input the base:";
				cin>>base;
				cout<<"input the exponent:";
				cin>>exponent;
				cout<<"The result is "<<power(base,exponent)<<"\n";
				break;

			case 2:  //show Fibonacci Series
				int n;
				cout<<"您要顯示第幾項:";
				while (1)
				{
					cin>>n;
					if (n<=0)
					{
						cout<<"您輸入的項數不合理，請重新輸入\n";
					}
				
					else
					{
						break;
					}
				}

				cout<<"The result is "<<Fibonacci(n)<<"\n";
				break;

			case 3:  //show the largest int Fibonacci number that can be printed on your computer
				nrFibonacci();
				cout<<"\n";
				break;

			case 0:  //exit
				choice=0;
				break;

			default:  //不合理的輸入
				cout<<"您輸入不合理的代碼，請重新操作\n";
				choice=0;
		}
	}

	system("pause");
	return 0;

}