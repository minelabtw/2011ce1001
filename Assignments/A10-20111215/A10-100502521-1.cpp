#include <iostream>
#include <cstdlib>
using namespace std;
int power(int base, int exponent)
{
	if(exponent==0)								//recursive function每次回傳時將次方-1，如果次方等於1時則回傳1
	{
		return 1;
	}
	else
	{
		return base*power(base,exponent-1);
	}
}
int Fibonacci(int time)									//recursive function 每次回傳自己-1跟-2的2項相加  當算到第1項回傳0 第2項回傳1
{
	if(time==2)
	{
		return 1;
	}
	else if(time==1)
	{
		return 0;
	}
	else
	{
		return Fibonacci(time-1)+Fibonacci(time-2);
	}
}
void nrFibonacci()
{
	int time=3;												//time紀錄是算到第幾項
	int total_1=0,total_2=1,total_3=0;						//3項的值
	cout<<"第1項為0\n第2項為1"<<endl;
	for(;;time++)
	{
		total_1=total_2+total_3;							//第3項(total_1)等於前兩項相加
		if(total_1>0)										//如果total_1>0才印，小於0代表溢位了!
		{
			cout<<"第"<<time<<"項為"<<total_1<<endl;
			total_3=total_2;								//設定下一輪要加的兩項值
			total_2=total_1;
		}
		else
		{
			break;
		}
	}
}		
int main()
{
	int choose=-1;			//使用者輸入的選擇 123進入函式123  0離開
	int base,exponent;
	while(choose!=0)
	{
		system("cls");
		cout<<"<1>算n次方\n<2>算費氏數列第n項的值By recursive function\n<3>印出費氏數列每項的值直到溢位By non-recursive function\n<0>離開程式\n請輸入選擇: ";
		cin>>choose;
		switch(choose)
		{
			case 1:
				do
				{
					cout<<"輸入基底跟指數: ";
					cin>>base>>exponent;
					if(exponent<1) //Assume that exponent is an integer greater than or equal to 1.
					{
						cout<<"指數不得小於1!"<<endl;
					}
				}while(exponent<1);
				cout<<base<<"^"<<exponent<<"="<<power(base,exponent)<<endl;
				system("pause");
				break;
			case 2:
				cout<<"請輸入要第幾項的值: ";
				cin>>base;
				cout<<Fibonacci(base)<<endl;
				system("pause");
				break;
			case 3:
				nrFibonacci();
				system("pause");
			case 0:
				break;
		}
	}
	cout<<"88~"<<endl;
	system("pause");
	return 0;
}