#include<iostream>
#include<cstdlib>
#include<iomanip>
using namespace std;

float power(float base, int exp);
int fibonacci(int n);                                                               //用遞迴算的費氏數列
int nrFibonacci(int n);                                                             //用迴圈算的費氏數列

int main()
{
    int choice, inputExp, inputN, tmp;
    float inputBase;
    while(1)
    {
        cout<<"\nchoice 1 : Power\n"
            <<"choice 2 : Fibonacci(recursive function)\n"
            <<"choice 3 : Fibonacci(non-recursive function)\n"
            <<"choice 4 : Clean the screen\n"                                       //清空螢幕
            <<"choice 5 : Exit\n"
            <<"your choice: ";
        cin>>choice;
        switch(choice)
        {
            case 1:
                cout<<"base: ";
                cin>>inputBase;
                cout<<"exponent: ";
                cin>>inputExp;
                if(inputExp<1)                                                      //若使用者輸入的exponent小於一則不合規定
                {
                    cout<<"exponent should be greater than or equal to one!\n";
                    break;
                }
                cout<<"result: "<<power(inputBase, inputExp)<<endl;                 //呼叫power()
                break;

            case 2:
                cout<<"please input an argument: ";                                 //使用者輸入欲求費氏數列中的第幾項
                cin>>inputN;
                if(inputN<0)                                                        //項數不可以小於0
                {
                    cout<<"argument should be greater than zero!\n";
                    break;
                }
                cout<<"result: "<<fibonacci(inputN)<<endl;                          //呼叫fibonacci()
                break;

            case 3:
                for(int i=0;;i++)
                {
                    if((tmp=nrFibonacci(i))<0)                                      //呼叫nrfibonacci()，用tmp暫存
                    {                                                               //若發現tmp是負數則表示overflow了
                        cout<<"no. "<<i-1<<" is the largest fibonacci integer!\n";
                        break;
                    }
                    else
                        cout<<"no."<<setw(3)<<i<<" : "<<setw(11)<<tmp<<endl;
                }
                break;

            case 4:
                system("cls");
                break;

            case 5:
                exit(100);

            default:
                cout<<"WRONG input!!\n";
                break;
        }
    }
    system("pause");
    return 0;
}

float power(float base, int exp)
{
    if(exp==0)                                                    //在exp==0時，已乘出結果，所以乘上最後一個一，結束函式
        return 1;
    else
        return base*power(base,exp-1);
}

int fibonacci(int n)
{
    if(n==0||n==1)
        return n;
    else
        return fibonacci(n-1)+fibonacci(n-2);
}

int nrFibonacci(int n)
{
    int preN=1, prepreN=0, ans;
    if(n==0||n==1)                                                //費氏數列的第零項和第一項(前兩項)是定義而來的
        ans=n;
    for(int i=2;i<=n;i++)                                         //從第二項開始才需要運算
    {
        ans=preN+prepreN;
        prepreN=preN;
        preN=ans;
    }
    return ans;
}
