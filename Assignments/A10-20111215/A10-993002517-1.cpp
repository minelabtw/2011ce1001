#include<cstdlib>
#include<iostream>
#include<iomanip>
using namespace std;

double Power(double,int);
int Fibonacci(int);
void nrFibonacci();

int main()
{
    cout<<"1) exponential"<<endl
        <<"2) Fibonacci"<<endl
        <<"3) non-recursive Fibonacci"<<endl;
    int choose=-1;
    while(choose!=0)
    {
        cout<<"choose a function to do (enter 0 to leave): ";
        cin>>choose;
        switch(choose)      //choose what function to do
        {
            case 0:         //leave
            cout<<"-the end-"<<endl;
                break;
            case 1:         //compute base^exponent
                double base;
                int exp;
                cout<<"  enter a number as base: ";
                cin>>base;
                cout<<"  enter a number as exponent: ";
                cin>>exp;
                if(exp<0)   //check if the entered number  is reasonable
                    cout<<"  exponent should greater than or equal to 0"<<endl;
                else        //if it is reasonable
                    cout<<"  "<<base<<" ^ "<<exp<<" = "<<Power(base,exp)<<endl;
                break;
            case 2:         //compute Fibonacci series (recursive)
                int term;
                cout<<"  enter which term to show: ";
                cin>>term;
                if(term<0)  //check if the entered number is reasonable
                    cout<<"  terms can not be neagtive"<<endl;
                else
                    cout<<"  the "<<term<<"th term = "<<Fibonacci(term)<<endl;
                break;
            case 3:         //compute Fibonacci series (non-recursive)
                nrFibonacci();
                break;
            default:
                cout<<"  there are only 3 choise!!"<<endl;
                break;
        }
    };
    system("PAUSE");
    return 0;
}

double Power(double base,int exp)   //compute base^exponent
{
    if(exp==0)          //if exponent = 0
        return 1;
    else if(exp==1)     //if exponent = 1
        return 1*base;
    else if(exp>1)      //base^exponent=base*base*base*....
        return base*Power(base,exp-1);
}
int Fibonacci(int term) //compute Fibonacci series (recursive)
{
    if(term-1==0)       //the 1st term
        return 0;
    else if(term-1==1)  //the 2nd term
        return 1;
    else if(term-1>1)   //the n-th term=the (n-1)-th term + the (n-2)-th term
        return Fibonacci(term-1)+Fibonacci(term-2);
}
void nrFibonacci()      //compute Fibonacci series (non-recursive)
{
    cout<<setw(5)<<"term"<<setw(15)<<"number"<<endl;
    int term=1,ans1=0,ans2=1,ans3;
    ans3=ans1+ans2;
    while(ans3>0)       //run the function to the limit
    {
        if(term==1)     //the 1st term
            cout<<setw(5)<<"1"<<setw(15)<<"0"<<endl;
        else if(term==2)//the 2nd term
            cout<<setw(5)<<"2"<<setw(15)<<"1"<<endl;
        else            //the other terms
        {
            ans3=ans1+ans2;
            if(ans3>0)
                cout<<setw(5)<<term<<setw(15)<<ans3<<endl;
            ans1=ans2;
            ans2=ans3;
        }
        term=term+1;
    }
}
