#include<iostream>
#include<cstdlib>
using namespace std;

double power(const double,double);
int Fibonacci(int);
int nrFibonacci();

int main()
{
   double cinbase,cinexponent;
   int choose1,choose2,n;
   for(;;) 
   {
      cout<<"\n1.運算次方\n2.費氏數列\n3.結束程式\n選擇: ";
      cin>>choose1;
      switch(choose1)
      {
         case 1://運算次方 
              cout<<"\n(底數,次方)\n";
              cout<<"底數=";
              cin>>cinbase;
              cout<<"次方=";
              cin>>cinexponent;
              if(cinbase==0 && cinexponent<=0)//請參考下面一大串話       
              {   
                 cout<<"0的正數次方等於0，0的負數次方無意義（因為分母不可為0）。";
                 cout<<"0的0次方是懸而未決的，多數領域則不定義，部分領域（如組合數學、範疇論）定義為0!!\n";
                 break;
              } 
              if(cinexponent>=0)//次方大於0時的情況 
              {
                 cout<<"("<<cinbase<<","<<cinexponent<<")= "
                     <<cinbase<<"^"<<cinexponent<<"= "<<power(cinbase,cinexponent)<<endl;
              }
              else//次方<0時 
              {
                 cout<<"("<<cinbase<<","<<cinexponent<<")= "
                     <<cinbase<<"^"<<cinexponent<<"= "<<1/power(cinbase,-cinexponent)<<endl;//結果為分數 
              }
              break;
         case 2://費氏數列 
              cout<<"1.費氏數列的值\n2.費氏數列的MAX\n選擇: ";
              cin>>choose2;
              switch(choose2)
              {
                 case 1:
                      cout<<"輸入項數: ";
                      cin>>n;
                      cout<<"Fibonacci("<<n<<")= "<<Fibonacci(n)<<endl;
                      break;
                 case 2:
                      nrFibonacci();
                      break;
                 default:
                      break;           
              }
              break;
         case 3:
              system("pause");
              return 0;
         default:
              break;
      }
   }
}

double power(const double base,double exponent)//計算次方 
{
   if(exponent==0)//次方值為0時return 1 
      return 1;
   else
      return base*power(base,exponent-1);//call自己,並且次方減1直到為0 
}            
             
int Fibonacci(int number)//費氏數列 
{
   if(number==1 || number==0)//第一項與第0項值為1,0 
      return number;
   else
      return Fibonacci(number-1)+Fibonacci(number-2);//費氏數列的公式(請參考課本P241) 
}

int nrFibonacci()//非recursive的費氏數列 
{
   int hold1,hold2,max;
   hold1=0;//先設為第0項 
   hold2=1;//先設為第1項 
   cout<<"Fibonacci(0)= 0\nFibonacci(1)= 1"<<endl;
   for(int i=2;;i++)//從第2項開始計算 
   {
      max=hold1+hold2;//第n項的值為第n-1項加第n-2項      
      hold1=hold2;//第n-2項的值給第n-1項 
      hold2=max;//第n項再給第n-1項,如此重複計算 
      cout<<"Fibonacci("<<i<<")= "<<max<<endl;
      if(max<0)//<0時結束費氏數列 
      {   
         cout<<"其值為負，停止計算\n"; 
         cout<<"費氏數列最大值為:\n第"<<i-1<<"項= "<<hold1<<endl; 
         break;
      } 
   }
}
