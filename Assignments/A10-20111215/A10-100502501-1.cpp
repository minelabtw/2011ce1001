#include<iostream>
#include<cstdlib>
#include<iomanip>
using namespace std;

int power(int,int);//function prototype
unsigned long Fibonacci(unsigned long); //function prototype
int nrFibonacci(int);//function prototype
int main()
{
	int choice;
	while(true) //loop to repeat the question
	{
		cout<<"\n1.Calculate the Power(base, exponent)"<<"\n2.Calculate the Fibonacci"
			<<"\n3.Show the largest int Fibonacci number"<<"\n4.Leave"<<"\nChoose one : ";
		cin>>choice;
		switch(choice) //switch statement for the choice
		{
			case 1: //case for the the power(base,exponent)
				int Base,Exp,result;
				cout<<"Please enter the base value :";
				cin>>Base;
				cout<<"Please enter the exponent value :";
				cin>>Exp;

				if(Exp<1)
					cout<<"Error!"<<endl;
				else
				{
					result=power(Base,Exp); //use function power
					cout<<"The result is "<<result<<endl; //output the result
				}
				break;
			case 2: //case for the Fibonacci
				unsigned long nth;
				cout<<"Please enter the number :";
				cin>>nth;
				//calculate the Fibonacci value
				cout<<"Fibonacci("<<nth<<")="<<Fibonacci(nth)<<endl; //output the nth value of the Fibonacci
				break;
			case 3: //case for the largest int Fibonacci number
				for(int m=0;m<60;m++) //loop to calculate the Fibonacci until overflow
                {
					cout<<" Fibonacci( "<<m<<" )"<<" = "<<nrFibonacci(m)<<endl;
					if(nrFibonacci(m)<0)
					{
						cout<<"The largest int Fibonacci number is "<<nrFibonacci(m-1)<<endl;
					    break;
					}
				}
				break;
			case 4: //case to leave
				return -1;
				break;
			default:
				cout<<"Error!"<<endl;
				break;
		}
	}
	system("pause");
	return 0;
}
//function to calculate the Power
int power(int base,int exponent)
{
	if(exponent==1) //base cases
		return base*1;
	else if(exponent>1) //recursion step
		return base*power(base,(exponent-1));
}
//recursive function Fibonacci
unsigned long Fibonacci(unsigned long num)
{
	if((num==0)||(num==1)) //base cases
		return num;
	else //recursion step
		return Fibonacci(num-1)+Fibonacci(num-2);
}

int nrFibonacci(int num) //non-recursive function nrFibonacci
{
	int f0=1,f1=0,Fib=0;
	if(num>=1)
	{
		for(int i=0;i<num;i++)//loop to add the sum of the two preceding terms
		{
			Fib=f0+f1;
			f0=f1;
			f1=Fib;
		}
	}
	return Fib;
}
