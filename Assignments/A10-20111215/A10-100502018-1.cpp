#include <iostream>
#include <cstdlib>
#include <iomanip>
using namespace std;

int power( int , int );
int Fibonacci( int );
void nrFibonacci();

int main()
{
	int choice;
	do
	{
		cout << "1. Function Power()\n2. Function Fibonacci()\n3. Function nrFibonacci()\n4. Get Away From The Function\nPlease choose which service you want to : "; //show the choice
		cin >> choice;
		switch( choice ) //tell the choice
		{
			case 1:
				int base,exp;
				cout << "Please input the base : ";
				cin >> base;
				cout << "Please input the exponent : ";
				cin >> exp;
				if( exp < 0 )
					cout << "This operation is invalid. Please try again." << endl;
				else
					cout << "The result is " << power( base , exp ) << endl;
				break;
			case 2:
				int item;
				cout << "Please input the number of item you want to know : ";
				cin >> item;
				if( item <= 0 )
					cout << "This operation is invalid. Please try again." << endl;
				else
					cout << "The result is " << Fibonacci( item -1 ) << endl;
				break;
			case 3:
				nrFibonacci();
				break;
			case 4:
				cout << "Thanks for your using. Good-bye~~" << endl;
				break;
			default:
				cout << "This operation is invalid. Please try again." << endl;
		}
		if( choice != 4 )
		{
			system("pause");
			system("cls"); //clean the content after every choice
		}
	}while( choice != 4 );

	system("pause");
	return 0;
}

int power( int x , int y ) //a function is used to calculate the number
{
	if( y == 0 && x != 0 )
		return 1;
	else if( y == 0 && x == 0 )
		return 0;
	else if( y == 1)
		return x;
	else
		return x * power( x , y - 1 );
}
int Fibonacci( int x ) //a function is used to calculate the Fibonacci series by recursion
{
	if( x == 0 || x == 1 )
		return x;
	else
		return Fibonacci( x - 1 ) + Fibonacci( x - 2 ); //F(x) = F(x-1)+F(x-2), until x = 0 || 1
}
void nrFibonacci() //a function is used to calculate the Fibonacci series by loop
{
	int i=1;
	int element1=0,element2=0,reg=0;

	for(i;reg>=0;i++) //when i is increasing, the series transate
	{
		if( i - 1 == 0 )
			element1 = i-1;
		else if( i - 1 == 1 )
			element2 = i-1;
		element1 = element2;
		element2 = reg;
		reg = element1 + element2;
		if( reg >= 0 )
			cout << "Fibonacci(" << i << ") is " << reg << endl;
		else
			break;
	}
	cout << "The limit of Fibonacci is the " << i-1 << " item : " << element2 << endl;
}