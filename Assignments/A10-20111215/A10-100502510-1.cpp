#include<iostream>
#include<cstdlib>
using namespace std;
double power(double base,double exponent)//power function
{

	double sum=1;


	for(int times=1;times<= exponent;times++)
	{
		sum=sum*base;
	}
	return sum;
}
int Fibonacci(int n)//recursive function Fibonacci(n)
{
	if(n==0||n==1)
	{
		return n;
	}
	else
	{
		return Fibonacci(n-1)+Fibonacci(n-2);
	}
}
void nrFibonacci()//non-recursive function nrFibonacci()
{
	int times;
	for(times=0;;times++)
	{
	    if(Fibonacci(times)<0)
		{
			break;//cannot overflow
		}
		cout<<"Fibonacci("<<times<<") = "<< Fibonacci(times)<<endl;

	}
}

int main()
{
	int choice;
	int choicetwo;
	int number;
	while(1==1)
	{
	   cout<<"Please enter your choice:( 1:power function 2:Fibonacci Series 3:Exit)"<<endl;//determine which function to use
	   cin>>choice;
	   switch(choice)
	   {
	      case 1:
	      cout<<"Please enter base and exponent:";
	      double exponent;
	      double base;
	      cin>>base;
	      cin>>exponent;
          if(base==0&&exponent==0)
	      {
		     cout<<"0的0次方是懸而未決的，多數領域則不定義，部分領域（如組合數學、範疇論）定義為0"<<endl;
		     break;
	      }
	      else if(base==0&&exponent<0)
	      {
	          cout<<"無意義"<<endl;
	          break;
	      }
	      else if(base!=0&&exponent<0)
	      {
	          cout<<"Error!!"<<endl;
	          break;
	      }
	      else
	      {
	         cout<<"Power("<<base<<","<<exponent<<") = "<<power(base,exponent)<<endl;
	         break;
	      }


	      case 2:
	      cout<<"Please enter what you want to do:( 1:reveal nth of Fibonacci 2:determine the largest int Fibonacci number )";
	      cin>>choicetwo;
	      switch(choicetwo)
	      {
	         case 1:
	    	 cout<<"Please enter the number you want:";
		     cin>>number;
		     cout<<"Fibonacci("<<number<<") = "<< Fibonacci(number)<<endl;
		     break;

		     case 2:
	         nrFibonacci();
		     break;
	      }
	      break;

	      case 3:
	      system("pause");//finish using
	      return 0;
	   }
	}
	system("pause");
	return 0;
}


