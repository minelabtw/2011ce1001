#include<iostream>
#include<cstdlib>
#include<iomanip>


using namespace std;

double  power(double,int);
int fibonacci(int);
int nrFibonacci(int);


int main()
{
    
    
    int choice;
    bool flag = true;// declare a flag for exit, false means exit
    while(flag)
    {
    
       cout << "請選擇:\n\n"
            << "(1)recursive function power\n"
            << "(2)recursive function Fibonacci(n)\n"
            << "(3)non-recursive function nrFibonacci()\n"
            << "(4)exit\n\n";
    
       cin >> choice;
        
       switch(choice)
       {
          case 1://display recursive function power
               double x;
               int y;
               int temp;    
    
               cout << "請輸入底數:"; 
               cin >>  x;
               cout << endl << "請輸入指數:";
               cin >> y;
               temp=y;
               cout << "\n\n\n";
         
   
   
               if(y<1)
               {
                   cout << "指數請輸入一個大於1的數\n\n"; 
               }
               else//顯示指數的計算過程 
               {     
                   cout<< "power"
                   << "(" 
                   << x 
                   << "," 
                   << y 
                   << ")" 
                   <<"=";
                   while(temp>1)
                   {
                         cout << x << "*";
                         temp--;
                   }
    
                   cout << x << "=";  
       
                   cout << power(x,y)<< "\n\n";
                }
                
                system ("pause");
                system("cls");
                 
                break;
    
       
    
    
    
    
          case 2:// diaplay recursive function Fibonacci(n)
               int a;
               cout << "廢式數列,請輸入項數:";
               cin >> a;
               cout << "費式數列第" 
                    << a 
                    << "項" 
                    << "是" 
                    << fibonacci(a-1)
                    <<"\n\n"; 
               system ("pause");
               system("cls");
               
               break;
         
     
          case 3://display non-recursive function nrFibonacci()
               cout << "\n\n";
               for(int i=1;;i++)
               {
                      
                      if(fibonacci(i-1)<0)
                      {
                           break;
                      }
                      else
                      {
                          cout << i 
                               << ".\t"
                               << fibonacci(i-1) << endl;
                      }
                      
               }
               
               system ("pause");
               system("cls");
               break;
         
       
          case 4://exit
               system("cls");
               cout << "恩掰!!" << endl;
               system ("pause");
               system("cls");
               cout <<"下次再會\n"; 
               flag = false;// set flag to false, and then end the while-loop
               break;
                
            default:
                break;  
     
       }   
    }
    
    
    
    
    
    
    system ("pause");
    
    return 0;
    
    
}




double  power(double base_num,int index)//指數函式 
{       
        if(base_num==0)
            return 0;
        else if((base_num==1)||(index==0))
            return 1;          
        else
            return base_num*power(base_num,index-1);
       
}



int fibonacci(int num)//費式數列函式 
{
    if((num==0)||(num==1))
        return num;
    
    else
        return fibonacci(num-1)+fibonacci(num-2);
}
