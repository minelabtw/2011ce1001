#include<iostream>
#include<cstdlib>
using namespace std;

int power(int base, int exponent)//次方 
{
    if(exponent==0)//當次方時=0 
        return 1;
    else
        return base*power(base,exponent-1);
}

int Fibonacci(int n)//費式數列 
{    
    if(n==1)//當項數=1時 
        return 0;
    else if(n==2)//當項數=2時 
        return 1;
    else
        return Fibonacci(n-1)+Fibonacci(n-2);
}
void nrFibonacci()//不是遞迴的費氏數列 
{
    int array[99]={};
    for(int i=1;;i++)//第i項的迴圈 
    {
        for(int j=i;j>0;j--)//費氏數列的迴圈 
        {
            if(j==1)
                array[j]=0;
            else if(j==2)
                array[j]=1;
            else
                array[j]=array[j-1]+array[j-2];
        }
        if(array[i]<0)//溢位 
            break;
        else
            cout << "第 " << i << " 項 = " << array[i] << endl;        
    }
}
int main()
{
    int option, base, exponent, n, x=0;
    while(true)
    {
        cout << "1. Power\n2. Fibonacci\n3. nrFibonacci\n4. Exit" << endl;
        cin >> option;//輸入選項 
        while(option!=1&&option!=2&&option!=3&&option!=4)//輸入錯誤 
        {
            cout << "Error!!!!請再輸入一次: ";
            cin >> option;
        }
        switch(option)
        {
            case 1://次方 
                cout << "請輸入底: ";
                cin >> base;
                cout << "請輸入次方: ";
                cin >> exponent;
                cout << base << " ^ " << exponent << " = " << power(base,exponent) << endl;
                break;
            case 2://費氏數列 
                cout << "請輸入項數: ";
                cin >> n;
                cout << "第 " << n << " 項 = " << Fibonacci(n) << endl;
                break;
            case 3://不是遞迴的費氏數列 
                nrFibonacci();
                break;
            case 4://離開 
                x=1;
                break;
            default:
                cout << "You can't see me!!";  
        }
        cout << endl;
        if(x==1)//接收離開的條件 
            break;
    }
    system("pause");
    return 0;
}
