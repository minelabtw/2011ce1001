#include<iostream>
#include<cstdlib>
using namespace std;

int power(int,int);  //次方函式的function prototype
int Fibonacci(int);  //費式數列計算的function prototype
void nrFibonacci();

int main()
{
	int loop = 1; //先假設loop的初始值為1
	while(loop == 1)  //進入while loop
	{
		int choice;
		cout << "選擇你需要的計算方式( 1.算次方 2.費式數列 3.費式數列在電腦中的最大值 4.離開 ):";  //選擇一種方式
		cin >> choice;
		switch(choice)  
		{
			case 1:  //選擇次方的話 執行case 1 
				int number1 , number2;
				cout << "輸入你要運算的底數:";  //將number1設為底數
				cin >> number1;
				cout << "輸入你要運算的指數:";  //將number2設為指數
				cin >> number2;
				if(number1 == 0)  //若底數為0 則無意義
				{
					cout << "無意義，請重新輸入\n";
				}
				else
				{
					cout << "計算" << number1 << "的" << number2 << "次方為:" << power( number1 , number2 ) << endl;  //顯示出答案
				}
				break;
			case 2:  //選擇費式數列的話，執行case 2
				int number3;
				cout << "你想知道第幾項的值?";  //將number3設為你想知道的第幾項
				cin >> number3;
				cout << "在費式數列中，第" << number3 << "的值為" << Fibonacci(number3) << endl;  //顯示出所要的答案
				break;
			case 3:  //選擇離開的話，執行case 3 
				nrFibonacci();
				break;
			case 4:
				return 0;  //離開
				break;
			default: //若打了其他的數字，重新輸入
				cout << "錯誤的數字!請再輸入一次" << endl;
				break;
		}
	}
	system("pause");
	return 0;
}

int power(int base , int exponent)  //power的運算函式
{
	if( exponent == 0 )  //若指數為0，則答案為1
	{
		return 1;
	}
	else
	{
		if(exponent == 1)  //若指數為1，則答案為底數
		{
			return base;
		}
		return base * power(base , exponent - 1);  //將其中的一次方提出來，再呼叫自己的函式
	}
}

int Fibonacci(int Fib)  //將橡樹重新設為Fib
{
	if( ( Fib == 1 ) || ( Fib == 2 ) )  //若Fib為1則回傳0，若Fib為2則回傳1
	{
		return Fib -1;
	}
	else
	{
		return  Fibonacci( Fib -1) + Fibonacci( Fib - 2);  //回傳第n項的值
	}
}

void nrFibonacci()
{
	int Fibon[10000];
	Fibon[0] = 0;
	Fibon[1] = 1;
	int counter = 0;
	bool Max = true;
	while(Max)
	{
		if(counter == 0)
		{
			cout<<Fibon[0]<<endl;
		}
		else if(counter == 1)
		{
			cout<<Fibon[1]<<endl;
		}
		else 
		{
			Fibon[counter] = Fibon[counter - 1] + Fibon[counter -2];
			if( Fibon[counter] < 0)
			{
				Max = false;
			}

			else
			{
				cout<< Fibon[counter]<<endl;
			}
		}
		counter++;
	}
	cout << "在此電腦中最大費式數列的項數為:" << "第" << counter -1 << "項，值為" << Fibon[counter-2] << "\n" ;
}