/******************************************************************************
 * Assignment 10-1
 * School/Year: National Central University, Taiwan / Freshman
 * Name:        江衍霖 ; Chiang,Yen-lin
 * E-mail:      flashcity123@kimo.com
 *              100502514@cc.ncu.edu.tw
 * Course:      NCU1001-CE1001A
 * Course Name: Introduction to Computer Science
 * Class:       CSIE 1A
 * Submitted:   Dec.16,2011
 *
 * Description:
 * Practice for Recursive Functions:
 * Power(x^y), Fib(fibonacci), nrFib(the maximum x for Fib(x) as int)
 * (The last function isn't recursive)
 *
 *
 * NOTICE: From now and then, I will usually include my own header.
 * If the compiling passed but there's a function never seen,
   it may be defined in my own header.
 *
 ******************************************************************************/

//Include my Custom Header
#include "A10-100502514-1.h"
using namespace FlashTeens;

//Line Constant Arrangement
const int MENU_LINE=1;
const int INPUT_LINE=7;
const int NRFIB_TABLE=3;

//Function Prototypes
int Power(int x, int y, bool resetState=true);
int Fib(int n, bool resetClock=true);
int nrFib();
string isTLE(int fib_num);

//Global Boolean for Power(x,y) to identify the overflow situation.
bool power_is_overflow=false;

//Global Array for nrFib()
int nr_array[60]={};

//Main Function
int main(){

    //Broaden the buffer width
    //setBufferSize('w',100);

    //The following code will run until user presses ESC.
    while(1){
        system("cls");
        cout<<gotoXY(0,MENU_LINE)<<toColorString("<green>Please select one of the actions:");
        cout<<gotoXY(1,MENU_LINE+1)<<toColorString("<green>Press <yellow>P<silver>   to <pink>show x^y using Recursion");
        cout<<gotoXY(1,MENU_LINE+2)<<toColorString("<green>Press <yellow>F<silver>   to <pink>show Fibonacci(x) using Recursion");
        cout<<gotoXY(1,MENU_LINE+3)<<toColorString("<green>Press <yellow>N<silver>   to <pink>show nrFibonacci() using While-loop");
        cout<<gotoXY(1,MENU_LINE+4)<<toColorString("<green>Press <yellow>Esc<silver> to <pink>EXIT");

        char keyPress;
        do{
            keyPress=pause();
            //Transfering to Lower Case
            if(keyPress>='A'&&keyPress<='Z')keyPress+='a'-'A';
        }while(keyPress!='p'&&keyPress!='f'&&keyPress!='n'&&keyPress!=27);
        //Show one of the ways user chosen.
        int x, y; //for x^y input
        int pow_result; //for x^y result
        int n; //for Fib(n) input
        int nMax; //Maximum value n for Fib(n) that is non-overflow
        switch(keyPress){
            case 'p':
                cout<<gotoXY(0,INPUT_LINE)<<toColorString("<yellow>Please input integers x, y for x^y :");
                cout<<gotoXY(1,INPUT_LINE+1)<<toColorString("(y must be non-negative)\n<skyblue>");
                //x, y are for input
                //Quit this program if input causes error.
                if(!(cin>>x>>y) ){
                    cout<<toColorString("<red>YOU INPUT AN ERROR VALUE!!\nTHIS PROGRAM WILL QUIT!!");
                    pause();
                    return 0;
                }else if(y<0){
                    cout<<toColorString("<red>y must be &gt;= 0 !!");
                    break; //Break out of the switch statement to input again
                }else if(x==0 && y==0){
                    cout<<toColorString("<red>0^0 doesn't work!!");
                    break; //Break out of the switch statement to input again
                }
                pow_result=Power(x, y);
                if(power_is_overflow)cout<<toColorString(" <white>")<<x<<" ^ "<<y<<toColorString(" <yellow>OVERFLOWS!!");
                else cout<<toColorString(" <white>")<<x<<" ^ "<<y<<toColorString(" = <green>")<<pow_result;
                break;
            case 'f':
                cout<<gotoXY(0,INPUT_LINE)<<toColorString("<yellow>Please input a POSITIVE integer n for Fibonacci(n): ")
                    <<toColorString("<skyblue>");
                //n is for input
                //Quit this program if input causes error.
                if(!(cin>>n) ){
                    cout<<toColorString("<red>YOU INPUT AN ERROR VALUE!!\nTHIS PROGRAM WILL QUIT!!");
                    pause();
                    return 0;
                }
                cout<<toColorString(" <white>Fibonacci(")<<n<<toColorString(") = <green>")<<isTLE(Fib(n));
                break;
            case 'n':
                //Clear the console before showing table
                system("cls");

                nMax=nrFib();

                cout<<gotoXY(1,MENU_LINE)<<toColorString(" <white>The Maximum n for Fibonacci(n) is :\n")
                <<toColorString("  Fibonacci(")<<nMax<<toColorString(") = <green>")<<nr_array[nMax-1];
                for(int i=1;i<=60;i++){
                    //List from Fib(1) to Fib(30)
                    //Stop printing result when overflow.
                    cout<<gotoXY(2+((i-1)/20)*20,NRFIB_TABLE+(i-1)%20+1)
                        <<toColorString("<white><bg red>F(")<<setw(2)<<setfg(YELLOW)<<i
                        <<toColorString("<white>)=");
                    if(i<=nMax){
                        cout<<gotoXY(8+((i-1)/20)*20,NRFIB_TABLE+(i-1)%20+1)
                            <<toColorString("<skyblue><bg darkblue>")<<setw(14)<<nr_array[i-1];
                    }else if(i==nMax+1){
                        //Show the first overflow value
                        cout<<gotoXY(8+((i-1)/20)*20,NRFIB_TABLE+(i-1)%20+1)
                            <<toColorString("<red><bg yellow>")<<setw(14)<<nr_array[i-1];
                    }else{
                        cout<<gotoXY(8+((i-1)/20)*20,NRFIB_TABLE+(i-1)%20+1)
                            <<toColorString("<silver><bg gray>")<<"-- OVERFLOW --";
                    }
                }
                cout<<setfg()<<setbg();
                break;
            default://ESC
                system("cls");
                cout<<toColorString("<yellow>Goodbye!! XD");
                pause();
                return 0;
        }

        pause();
    }
    return 0;
}

//Recursive Functions
int Power(int x, int y, bool resetState){
    //is_overflow is global variable
    if(resetState)power_is_overflow=false;
    if(y>0){
        if(x==0)return 0;//Prevents result/x causing Error
        int prev_num=Power(x, y-1,false);
        int result=x*prev_num;
        if(result/x!=prev_num){
            //Overflow
            power_is_overflow=true;
        }
        return result;
    }
    //if y<=0, 1 will be returned.
    return 1;
}
int Fib(int n, bool resetClock){
    //Get start time
    static int timer=clock();
    if(resetClock)timer=clock();
    //If Over 2-second time limit, it'll return -1 for TLE status.
    //In this program, the 1st overflow won't go to -1.
    if(clock()-timer>2000)return -1;

    if(n>2){

        //Take Fib(n-1) and Fib(n-2) apart to save the calculating time.

        int F_of_n1=Fib(n-1, false);
        //Check if F_of_n1 is Overflow or TLE
        if(F_of_n1<0)return F_of_n1;

        int F_of_n2=Fib(n-2, false);
        //Check if F_of_n2 is Overflow or TLE
        if(F_of_n2<0){
            return F_of_n2;
        }

        //If not Overflow, the following value will be returned,
        //No matter the sum is Overflow or not.
        int result=F_of_n1+F_of_n2;
        return result;
    }
    else if(n==2)return 1;
    else return 0;
}
//Non-recursive function nrFib()
int nrFib(){
    int count=1;
    nr_array[0]=0;
    nr_array[1]=1;
    while(++count && nr_array[count-1]>=0){
        nr_array[count]=nr_array[count-1]+nr_array[count-2];
    }
    return count-1;
}
string isTLE(int fib_num){
    ostringstream strout;
    //-1 is for TLE
    if(fib_num!=-1)strout<<fib_num;
    else strout<<"TIME LIMIT 2s EXCEEDS!!";
    return strout.str();
}
