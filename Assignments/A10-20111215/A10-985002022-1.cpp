#include <iostream>
#include <cstdlib>
using namespace std;

static void line() {                 //畫線當作間隔 
   cout<<endl;
   for(int i=0; i<40; i++) {
      cout<<"__";
      if(i==39) {
         cout<<endl;
      } 
   }
}

static void show(int i) {           //數字之間的間隔 
   if(i<10) {
      cout<<"            ";
   }
   else if(i<100) {
      cout<<"           ";
   }
   else if(i<1000) {
      cout<<"          ";
   }
   else if(i<10000) {
      cout<<"         ";
   }
   else if(i<100000) {
      cout<<"        ";
   }
   else if(i<1000000) {
      cout<<"       ";
   }
   else if(i<10000000) {
      cout<<"      ";
   }
   else if(i<100000000) {
      cout<<"     ";
   }
   else if(i<1000000000) {
      cout<<"    ";
   }
   else if(i<2000000000) {
      cout<<"   ";
   }
}

int power(int a, int b) {           //a的b次方 
   if(b==1) {
      return a;
   }
   else {
      return a*power(a,b-1);
   }
}

int fibonacci_1(int i) {            //recursive fibonacci 
   if(i==0) {
      return 0;
   }
   else if(i==1) {
      return 1;
   }
   else {
      return fibonacci_1(i-1)+fibonacci_1(i-2);
   }
}

void fibonacci_2() {                //non_recursive fibonacci 
   int a[100000];
   a[0]=0;
   a[1]=1;
   int count=2;
   line();
   cout<<a[0];                      //先印出第0和第1個fibonacci number 
   show(a[0]);
   cout<<a[1];
   show(a[1]);
   while(true) {
      a[count] = a[count-1] + a[count-2];    //計算第2個之後的fibonacci數 
      if((a[count])<0) {                     //overflow之後會變成負數 while停止 
         break;
      }
      cout<<a[count];                        //若沒有overflow則印出 
      show(a[count]);
      count++;
      if(count%5==0) {
         line();
      }
   }
   line();
   cout<<endl;
}

int main() {
   int t;                           //事件1-4 
   cout<<"1 power"<<endl;
   cout<<"2 recursive fibonacci"<<endl;
   cout<<"3 non_recursive fibonacci"<<endl;
   cout<<"4 exit"<<endl;
   while(t!=4) {
      cout<<"which do you want: ";
      cin>>t;
      switch(t) {
         case 1: {                  //power 
            int base=0;
            int exponent=0;
            while(exponent<1) {
               cout<<"base:";
               cin>>base;
               cout<<"exponent:";
               cin>>exponent;
               if(exponent<1) {
                  cout<<"exponent should be greater than or equal to 1"<<endl;
               }
            }
            cout<<"power("<<base<<","<<exponent<<")="<<power(base,exponent)<<endl;
            break;
         }
         case 2: {                  //recursive fibonacci 
            int i;
            cout<<"enter which fibonacci number do you want : ";
            cin>>i;
            cout<<"the "<<i<<" th fibonacci number is : "<<fibonacci_1(i)<<endl;
            break;
         }
         case 3: {                  //non_recursive fibonacci 
            fibonacci_2();
            break;
         }
      }
   }
   system("pause");
   return 0;
}
