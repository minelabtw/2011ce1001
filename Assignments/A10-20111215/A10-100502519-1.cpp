#include<iostream>
#include<cstdlib>
#include<iomanip>
using namespace std;

int power( int,int );			//宣告function
int Fibonacci( int );
void nrFibonacci();

int main()
{
	int base1;			//cin的底數
	int exponent1;			//cin的指數
	int term0 = 0;			//用來從零到指定的項數
	int term1;			//cin的項數
	int reuse=1;			//用來重複跑
	int choice;			//cin的選項

	while(reuse>0)			//while迴圈 使可以重複使用
	{
		cout << "Show: 1.power  2.Fibonacci  3.nrFibonacci  4.exit : ";
		cin >> choice;

		reuse++;

		switch(choice)			//switch 接受使用者的選擇
		{
			case 1:
				cout << "enter base : ";
				cin >> base1;
				cout << "enter exponent : ";
				cin >> exponent1;

				if(exponent1>=0)			//if 指數>=0 則使用function
				{
					cout << "The answer is : " << power(base1 , exponent1) << endl;
					cout << endl << endl;
				}
				else			//if 指數<0 則顯示錯誤訊息
				{
					cout << "error!!! exponent should >=0 !!!" <<endl;
					cout <<endl << endl;
				}
				break;

			case 2:
				cout << "enter term : ";
				cin >> term1;

				if(term1>0)			//if 項數>0 則進入
				{
					cout << setw(2) << "項" << setw(12) << "值" << endl;
					while(term0<term1)			//while迴圈 使term0 從0加到欲求得的項數term1  並使用function
					{
						cout << setw(2) << term0+1 << setw(12) << Fibonacci(term0) << endl;
						term0++;
					}
					cout << endl <<endl;
				}
				else			//if 項數<=0 則顯示錯誤訊息
				{
					cout << "error!!! term should > 0 !!!" << endl;
					cout << endl <<endl;
				}
				term0=0;			//讓term0 歸零 讓下次使用不會錯誤
				break;

			case 3:
				cout << setw(2) << "項" << setw(12) << "值" << endl;
				nrFibonacci();			//使用function
				cout << endl << endl;
				break;

			case 4:
				reuse=0;			//讓reuse等於0 使跳出while迴圈
				cout << endl << "Thank for use !!" << endl << endl;
				break;

			default :			//不是選1~4時 顯示錯誤訊息
				cout << "error!!!  You can only choose 1~4 !!!" <<endl;
				cout << endl << endl;
				break;
		}
	}


	system("pause");
	return 0;
}


int power( int base , int exponent )
{
	if(exponent==1)			//任何數的1次方等於自己
	{
		return base; 
	}
	else if(exponent==0)			//任何數的0次方等於1
	{
		return 1;
	}
	else			//剩下的就用recursive
	{
		exponent--;
		return base*power( base , exponent );			//自己(base) 乘以 自己-1(上一行減了) ←遞回
	}
}


int Fibonacci( int term )
{
	if( term==0 || term==1 )			//第1項(term=0) = 0  第二項(term=1) =1
	{
		return term;
	}
	else
	{
		return Fibonacci(term-1)+Fibonacci(term-2);			//其他的用遞回 (前兩項相加)
	}
}


void nrFibonacci()
{
	int value=0;			//各項的值
	int first=0;			//第一項=0
	int second=1;			//第二項=1
	int tabular=3;			//用來做出板面 (從第3項開始)

	while(value>=0)			//值=>0 表示還沒overflow
	{
		if(value==0)			//第1項
		{
			cout << setw(2) << "1" << setw(12) << first << endl;
		}
		else if(value==1)			//第2項
		{
			cout << setw(2) << "2" << setw(12) << second <<endl;
		}
		else			//第3項開始
		{
			value = first + second;			//值=前兩項相加
			if(value>=0)			//若值>=0 (還沒overflow) 則cout
			{
				cout << setw(2) << tabular << setw(12) << value << endl;
				tabular++;			//跑板面
			}
			first=second;			//讓第1項值變成第2項的
			second=value;			//讓第2項值變成第3項的
			/* 
				ex	1	(2)	(3)
					(2)	(3)	 5
			*/
		}
		value++;
	}
}