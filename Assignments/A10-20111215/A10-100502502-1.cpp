//A10-100502502-1.cpp
#include <iostream>
#include <cstdlib>
using namespace std;
int power( int base, int exponent )//n次方的計算
{
    if ( base == 0 )
        return 0;
    else if ( exponent == 0 )//當0次方時
        return 1;
    else
        return base * power( base, exponent - 1 );//回傳結果
}
int Fibonacci( int nth )//費式數列的運算
{
    if (( nth == 0 ) || ( nth == 1))
        return nth;
    else
        return Fibonacci( nth - 1) + Fibonacci( nth - 2);//回傳結果
}
void nrFibonacci()//使用迴圈的費式數列運算
{
    int answer1;
    int answer2;
    int answer3;
    for(int n = 1; n > 0; n++ )
    {
        if ( n == 1)//第一項的結果
        {
            answer1 = 0;
            cout << "第" << n << "項" << " = " << answer1 << endl;
        }
        else if ( n == 2 )//第二項的結果
        {
            answer2 = 1;
            cout << "第" << n << "項" << " = " << answer2 << endl;
        }
        else if ( n >= 2 )//其他項的結果
        {
            answer3 = answer1 + answer2;
            if( answer3 >= 0)
            {
                cout << "第" << n << "項" << " = " << answer3 << endl;
                answer1 = answer2;
                answer2 = answer3;
            }
            else//overflow時結束
                break;
        }
    }
}
int main()//function main 開始
{
    cout << "1.power" << endl << "2.Fibonacci" << endl << "3.nrFibonacci" << endl;//顯示3種function
    while( true )
    {
        int choice;
        cout << "請輸入要使用的function編號(離開請按0): ";
        cin >> choice;
        if ( choice == 0 )//按0離開
            break;
        switch( choice )
        {
            case 1://選擇function1時
                int base, exponent;
                cout << "請輸入底數和次方 : ";
                cin >> base;
                cin >> exponent;
                if ( exponent < 0 )
                    cout << "錯誤!!" << endl;//次方小於0顯示錯誤訊息
                else
                    cout << "結果 =  " << power( base, exponent ) << endl;//次方大於等於0顯示運算結果
                break;
            case 2://選擇function2時
                int n;
                cout << "你想知道第幾項的結果 : ";
                cin >> n;
                if ( n <= 0 )
                    cout << "錯誤!!" << endl;
                else
                    cout << "結果 = " << Fibonacci( n - 1 ) << endl;//顯示第n項的結果
                break;
            case 3://選擇function3時
                nrFibonacci();//顯示每一項的結果
                break;
            default:
                cout << "錯誤!!" << endl;
                break;
        }
    }
    system ( "pause" );//請按任一鍵繼續
    return 0;//結束
}
