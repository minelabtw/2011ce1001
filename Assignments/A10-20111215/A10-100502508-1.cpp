#include<iostream>
#include<cstdlib>
using namespace std;
double power(double,int);//function prototype
int Fibonacci(int);//function prototype
int nrFibonacci(int);//function prototype
int main()//the mian function execute
{
    int choose;
    double base;
    int exponent;
    int item;
    cout<<"Which do you choose?"<<endl;
    cout<<"Function 1 , 2 and 3 , press 0 to leave."<<endl;
    cin>>choose;
    system("cls");
    while(choose!=0)//repeat to execute
    {
        switch(choose)//categorize
        {
            case 1:
                cout<<"Please enter the number of base and exponent."<<endl;
                cin>>base>>exponent;
                if(exponent>=0)//exponent is positive or zero
                {
                    if(base!=0||exponent!=0)
                    {
                        for(int a=1;a<=exponent;a++)
                        {
                            power(base,a);//function call
                        }
                        cout<<"The answer is "<<power(base,exponent)<<endl;//function call
                    }//base and exponent are equal zero
                    else
                    {
                        cout<<"Not difinite!!"<<endl;
                    }
                }
                else//enponent is negative
                {
                    if(base!=0)
                    {
                        for(int b=1;b<=-exponent;b++)
                        {
                            power(base,b);//function call
                        }
                        cout<<"The answer is "<<1/power(base,exponent)<<endl;//function call
                    }//base is equal zero
                    else
                    {
                        cout<<"No meaning!!"<<endl;
                    }
                }
                break;//exit switch
            case 2:
                cout<<"Please enter the item of Fibonacci Series."<<endl;
                cin>>item;
                for(int c=1;c<=item;c++)
                {
                    Fibonacci(c-1);//function call
                }
                cout<<"The answer is "<<Fibonacci(item-1)<<endl;//function call
                break;//exit switch
            case 3:
                for(int item2=0;item2>=0;item2++)
                {
                    if(item2==0)//first item 
                    {
                        cout<<"fibonacci("<<item2+1<<")="<<nrFibonacci(item2)<<endl;//function call
                    }
                    else 
                    {
                        if(nrFibonacci(item2)>=nrFibonacci(item2-1))
                        {
                            cout<<"fibonacci("<<item2+1<<")="<<nrFibonacci(item2)<<endl;
                        }
                        else//overflow
                        {
                            break;
                        }
                    }
                } 
                break;//exit switch
            default:
                cout<<"Wrong!!"<<endl;
                break;//exit switch
        }//end switch
        system("pause");
        system("cls");
        cout<<"Which do you choose?"<<endl;
        cout<<"Function 1 , 2 and 3 , press 0 to leave"<<endl;
        cin>>choose;
        system("cls");
    }//end while
    system("pause");
    return 0;
}//end main 
double power(double basenumber,int exponentnumber)
{
    if(exponentnumber==0)//base case
    {
        return 1;
    }
    else if(exponentnumber==1)//base case
    {    
        return basenumber;
    }
    else if(exponentnumber>1)
    {
        return basenumber*=power(basenumber,exponentnumber-1);//recursion step
    }
}//end function power
int Fibonacci(int itemmnumber)
{
    if(itemmnumber==0||itemmnumber==1)//base case
    {
        return itemmnumber;
    }
    else
    {
        return Fibonacci(itemmnumber-1)+Fibonacci(itemmnumber-2);//recursion step
    }
}//end function Fibonacci
int nrFibonacci(int item2mnumber)
{
    int first=0;
    int second=1;
    int answer;
    if(item2mnumber==0)//base case
    {
        return first;
    } 
    else if(item2mnumber==1)//base case
    {
        return second;
    }
    else 
    {
        for(int e=2;e<=item2mnumber;e++)//iterative nrFibonacci calculation
        {
            answer=first+second;
            first=second;
            second=answer;
        }
        return answer;
    }
}//end function nrFibonacci
