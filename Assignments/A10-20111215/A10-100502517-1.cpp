#include<iostream>
#include<cstdlib>
#include<iomanip>
using namespace std;

int power(int base, int exponent)//底數.次方
{
    if(exponent>1)
        return base*power(base,exponent-1);

    else if(exponent==1)
        return base;

    else if(exponent==0)
        return 1;
}

int Fibonacci(int N)//費式數列
{
    if(N==1)
        return 0;

    else if(N==2)
        return 1;

    else if(N>2)
        return Fibonacci(N-1)+Fibonacci(N-2);//公式
}

int main()
{
    int run = true;//which

    int choice;//switch

    int Base, Exponent;//case 1

    int term;//case 2


    while(run!=false)//進入迴圈
    {

        int first = 0;//case3
        int second = 1;
        int next = 1;
        int nextone=1;
        int number = 4;

        cout<<"\nWelcome to use~!!!"<<endl
            <<"\n<1>power <2>Fibonacci(recursive) <3>Fibonacci(non-recursive) <4>Exit"<<endl
            <<"\nPlease choose: ";
        cin>>choice;

        switch(choice)//選擇
        {
            case 1://底數.次方
                cout<<"Base: ";
                cin>>Base;

                for(;;)
                {
                    cout<<"Exponent: ";
                    cin>>Exponent;

                    if(Exponent<0)//檢查次方是否>=0
                    {
                        cout<<"Exponent: ";
                        cin>>Exponent;
                    }
                    else
                    {
                        cout<<"You will get "<<power(Base, Exponent)<<endl;
                        break;
                    }
                }
                break;

            case 2://費式數列(recursive)
                cout<<"Please enter the term that you want to see: ";
                cin>>term;

                for(;;)
                {
                    if(term<=0)//檢查項數是否>0
                    {
                        cout<<"Please enter the term that you want to see: ";
                        cin>>term;
                    }
                    else
                        break;
                }

                for(int terms = 1; terms <= term; terms++)
                {
                    cout<<"No."<<terms<<"\t"<<Fibonacci(terms)<<endl;
                }
                cout<<endl;
                break;

            case 3://費式數列(non-recursive)
                cout<<"No.1\t"<<first<<endl
                    <<"No.2\t"<<second<<endl
                    <<"No.3\t"<<next<<endl;
                for(;;)
                {
                    nextone = next + second;//第N項 = 第N-1項 + 第N-2項

                    if(nextone<0)//判斷是否overflow
                        break;
                    else
                    {
                        cout<<"No."<<number<<"\t"<<nextone<<endl;
                        second = next;//從第N-1項變第N-2項
                        next = nextone;//從第N項變成第N-1項
                        number++;
                    }
                }
                cout<<"\nThe biggest number in int is No."<<number-1<<"\t"<<next;
                break;

            case 4://跳出
                cout<<"Bye~BYe~"<<endl;
                run = false;
                break;

            default://輸入錯誤
                cout<<"Wrong input~!!!"<<endl
                    <<"Please try again~"<<endl;
                break;
        }


    }
    system("pause");
    return 0;

}
