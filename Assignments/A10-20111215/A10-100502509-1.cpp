#include<iostream>
#include<cstdlib>
#include<cmath>
using namespace std;
	unsigned long power(int base, int expo)//計算次方的函式 
	{
		if(expo==0)//次方為0 
		{
			return 1;	
		}
		if(expo==1)//次方為1 
		{
			return base;	
		}
		else
		{
			return	base*power(base,expo-1);	
		}
	}
	
	inline unsigned long Fibonacci(int item)//計算費氏數列的函式 
	{
		if( (item==0) || (item==1) )
		{
			return item;	
		}	
		else
		{
			return Fibonacci(item-1)+Fibonacci(item-2);	
		}
	}
	
	int nrFibonacci()//計算費氏數列的各項 
	{
		int answer,max,item,answer1=0,answer2=1;
		cout<<"fibonacci("<<"1"<<")="<<answer1<<endl;
		cout<<"fibonacci("<<"2"<<")="<<answer2<<endl;
		for(item=2;item>1;item++)
		{	
			
			answer=answer1+answer2; 
			answer1=answer2;//把前一項丟給下一項 
			answer2=answer;
			
			if(answer>0)
			{
				cout<<"fibonacci("<<item<<")="<<answer<<endl;	
			}
			else//發生溢位 
			{
				cout<<"fibonacci("<<item<<")="<<answer<<"為負數"<<endl;
				cout<<"所以第"<<item-1<<"為最大值: "<<answer1<<endl;
				item=-1;
				break;	
			}
				
		}	
			
		
			
	} 
	
	int main()
	{
		int choose;
		cout<<"1.Power function\n2.Exponent function\n3.Exit\n";
		for(;;)
		{
			cout<<"Which function do you want to choose: ";
			cin>>choose;
			switch(choose)
			{
				case 1://選擇計算次方的函式 
					int base,expo;
					cout<<"Please enter the base: ";
					cin>>base;
					
					cout<<"Please input the exponent: ";
					cin>>expo;
					power(base,expo);
					cout<<"power("<<base<<","<<expo<<")="<<power(base,expo)<<endl;
					break;
					
				case 2://選擇費氏數列的函式 
					int items,userchoose;
					cout<<"1.費氏數列第N項的值\n2.費氏數列的最大值\n";
					cout<<"Which function do you want to choose: ";
					cin>>userchoose;
					if(userchoose==1)//選擇第幾項的值 
					{
						cout<<"Please enter the items";
						cin>>items;
						Fibonacci(items);
						cout<<Fibonacci(items)<<endl;
						
					}
					
					else if(userchoose==2)//選擇各項的值 
					{
						nrFibonacci();
					}
					else
					{
						cout<<"輸入錯誤,請重新輸入"<<endl;	
						break;
					}
					break;
				case 3:
					cout<<"非常感謝您的使用"<<endl;
					break;	
				default:
					cout<<"輸入有誤喔,請重新輸入"<<endl;
					break;
			}
		}
		system("PAUSE");
		return 0;
	}
