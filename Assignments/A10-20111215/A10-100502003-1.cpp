#include <iostream>
#include <iomanip>
#include <cstdlib>
using namespace std;

// 宣告function的prototype
int Power(int, int);
int Fibonacci(int);
void nrFibonacci();

int main()
{
    int choice; // 將被用來存選項的變數

    cout << "Which do you want to know??\n"
        << "Enter '1' for power; '2' for Fibonacci; '3' for nrFibonacci; '0' for leave\n";
    cin >> choice;

    while(choice != 0) // 只要choice不為離開(選0)，迴圈就繼續跑
    {
        switch(choice)
        {
            case 1: // 選power的情況
                int Base, Exponent;
                cout << "Please enter the base number that is going to be used: ";
                cin >> Base;

                if(Base>=0) // 判斷底數是否>=0(只讓使用者輸入>=0的數)
                {
                    cout << "Please enter the exponent number that is going to be used: ";
                    cin >> Exponent;
                }
                else // 若不符條件，重新輸入
                {
                    cout << "Wrong accept!Please enter a non-negative integer!!" << endl;
                    cout << "Try again!!" << endl;
                    break;
                }

                if(Exponent>=1) // 判斷Exponent是否符合條件(>=1)，符合條件才輸出結果
                {
                    cout << "The Result is: " << Power(Base, Exponent) << endl; // 輸出結果，呼叫function的值
                }
                else // 若不符條件，重新輸入
                {
                    cout << "Please enter an integer that is greater than or equal to 1!!" << endl;
                    cout << "Try again(from the begining of this choice)!!" << endl;
                    break;
                }
                // 讓使用者繼續選擇其他功能
                cout << "Which do you want to know??\n"
                    << "Enter '1' for power; '2' for Fibonacci; '3' for nrFibonacci; '0' for leave\n";
                cin >> choice;
                break;

            case 2: // 選Fibonacci的情況
                int num;
                cout << "Which one of the number in the Fibonacci do you want to know: ";
                cin >> num;
                cout << "The Result is: " << Fibonacci(num) << endl; // 輸出結果，呼叫function的值
                cout << "Which do you want to know??\n"
                    << "Enter '1' for power; '2' for Fibonacci; '3' for nrFibonacci; '0' for leave\n";
                cin >> choice;
                break;

            case 3: // 選擇nrFibonacci的情況
                cout << "In the following, you will see the whole int Fibonacci number that can be printed.\n"
                    << "The largest one is the last one." << "\n" << endl;
                cout << "Fibonacci order" << setw(15) << "Number" << endl;
                nrFibonacci(); // 使用function: nrFibonacci
                cout << "Which do you want to know??\n"
                    << "Enter '1' for power; '2' for Fibonacci; '3' for nrFibonacci; '0' for leave\n";
                cin >> choice;
                break;

            default: // 輸入選項外的數字的情況
                cout << "Wrong accept! Please enter 0~3!!\n" << "Let's try again!!" << endl;
                cout << "Which do you want to know??\n"
                    << "Enter '1' for power; '2' for Fibonacci; '3' for nrFibonacci; '0' for leave\n";
                cin >> choice;
                break;
        }
    }

    system("pause");
    return 0;
}

int Power(int base, int exponent)
{
    if(exponent>1)
    {
        return Power(base, exponent-1)*Power(base, 1); // 所求的值等於前一項再乘底數
    }

    else if(exponent == 1) // 任意數的1次方還是等於該數
        return base;
}

int Fibonacci(int n)
{
    if(n==1 || n==2) // 第1.2項的值都剛好為項數減1
        return n-1;

    else
        return Fibonacci(n-1)+Fibonacci(n-2); // 新的值等於前兩項相加
}

void nrFibonacci() // 不使用recursive
{
    int a=0, b=1,count=2; // 第1.2項的初始值已知(為0&1)，count為計算第幾項的變數
    int Fibo_num = a+b; // 先將第3項的值存入一個變數
    cout << "Fibonacci(01)" << setw(17) << a << endl;
    cout << "Fibonacci(02)" << setw(17) << b << endl;
    while(Fibo_num>=0) // 判斷當Fibo_num的值仍為正數時，還沒overflow，就繼續跑
    {
        count++;
        if(count<10) // 輸出訊息不同，只為排版較美觀
        {
            cout << "Fibonacci(0" << count << ")" << setw(17) << Fibo_num << endl;
        }
        else
            cout << "Fibonacci(" << count << ")" << setw(17) << Fibo_num << endl;
        a=b; // 將所求的值的前兩項重新設為上一個值的前一項
        b=Fibo_num; // 所求的值的前一項則為上一個求到的值
        Fibo_num = a+b; // 新的值為前兩項相加
    }

}
