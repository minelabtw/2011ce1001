#include<iostream>
#include<cstdlib>
#include<iomanip>
using namespace std;

double power(double base,int exponent)
{
    if(exponent==0)//end
    {
        return 1;
    }
    else//compute
    {
        return base*power(base,exponent-1);
    }
}

int  Fibonacci(int n)
{
    if(n==0)//initial case
    {
        return 0;
    }
    else if(n==1)//initial case
    {
        return 1;
    }
    else//compute
    {
        return Fibonacci(n-1)+Fibonacci(n-2);
    }
}

void nrFibonacci()
{
    int num1=0,num2=1,result=0,count=2;
    cout<<setw(11)<<num1<<setw(11)<<num2<<setw(11);//output initial number
    for(int loop=1;loop<2;loop++)//compute
    {
        result=num1+num2;
        num1=num2;
        num2=result;

        if(result<0)//overflow
        {
            loop=2;
        }
        else
        {
            loop=0;
            cout<<result<<setw(11);
            count++;
        }
        if(count%5==0)//arrange number
        {
            cout<<endl;
        }
    }
    cout<<endl<<"\rTotal: "<<count<<" Numbers."<<endl;
}
int main()
{
    int chos,exp,ans;
    double inputnum;
    for(int loop=1;loop<2;loop++)//execute again or not
    {
        cout<<"1.Power funtion.\n";
        cout<<"2.Fibonacci Series.\n";
        cout<<"3.Range of Fibonacci Series.\n";
        cout<<"4.Exit.\n";
        cout<<"Which service you want: ";
        cin>>chos;
        switch(chos)//choose function
        {
            case 1://power function
                for(int check=1;check<2;check++)
                {
                    cout<<"\nPlease fill in the base: ";
                    cin>>inputnum;
                    if(inputnum<=0)//incurrect case
                    {
                        cout<<"Error!";
                        check=0;
                    }
                    else
                    {
                        check=2;
                    }
                }
                for(int check=1;check<2;check++)
                {
                    cout<<"Please fill in the exponent: ";
                    cin>>exp;
                    if(exp<0)//incurrect case
                    {
                        cout<<"Error!\n";
                        check=0;
                    }
                    else
                    {
                        check=2;
                    }
                }
                cout<<"Result is: "<<power(inputnum,exp)<<endl;

                for(int checkans=1;checkans<2;checkans++)//execute again or not
                {
                    cout<<"Do you want to continue(y=1/n=2): ";
                    cin>>ans;
                    switch(ans)
                    {
                        case 1:
                            loop=0;
                            system("cls");
                            break;
                        case 2:
                            system("cls");
                            cout<<"Thanks for using!\n";
                            loop=2;
                            break;
                        default:
                            cout<<"Error!\n";
                            checkans=0;
                            break;
                    }
                }
                break;
            case 2://Fibonacci function
                for(int check=1;check<2;check++)
                {
                    cout<<"\nPlease fill in which number you want to see: ";
                    cin>>inputnum;
                    if(inputnum<=0)//incurrect case
                    {
                        cout<<"Error!";
                        check=0;
                    }
                    else
                    {
                        check=2;
                    }
                }
                cout<<"Result is: "<<Fibonacci(inputnum-1)<<endl;
                for(int checkans=1;checkans<2;checkans++)//execute again or not
                {
                    cout<<"Do you want to continue(y=1/n=2): ";
                    cin>>ans;
                    switch(ans)
                    {
                        case 1:
                            loop=0;
                            system("cls");
                            break;
                        case 2:
                            system("cls");
                            cout<<"Thanks for using!\n";
                            loop=2;
                            break;
                        default:
                            cout<<"Error!\n";
                            checkans=0;
                            break;
                    }
                }
                break;
            case 3://rage of Fibonacci
                nrFibonacci();
                for(int checkans=1;checkans<2;checkans++)//execute again or not
                {
                    cout<<"Do you want to continue(y=1/n=2): ";
                    cin>>ans;
                    switch(ans)
                    {
                        case 1:
                            loop=0;
                            system("cls");
                            break;
                        case 2:
                            system("cls");
                            cout<<"Thanks for using!\n";
                            loop=2;
                            break;
                        default:
                            cout<<"Error!\n";
                            checkans=0;
                            break;
                    }
                }
                break;
            case 4://exit
                system("cls");
                cout<<"Thanks for using!\n";
                loop=2;
                break;
            default:
                cout<<"Error!\n\n";
                loop=0;
                break;
        }
    }
    system("pause");
    return 0;
}
