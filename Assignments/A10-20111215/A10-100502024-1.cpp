#include <iostream>
#include <cstdlib>
using namespace std;

int power (int,int);
int Fibonacci (int);
int nrFibonacci (int,int);

int main ()
{
	int choose;
	int base,expo;
	int n;
	int number;
	bool loop = true; // 使用布林運算設定 loop 為 true
	while (loop)
	{
		cout << "Please choose a function to use :" << endl;  // 顯示清單
		cout << "(1) 乘方" << endl;
		cout << "(2) 費式數列 " << endl;
		cout << "(3) 記憶體中所能儲存的最大費式數列 " << endl;
		cout << "(4) 離開" << endl;
		cin >> choose;

		switch (choose)  // 判別使用者輸入的選項
		{
			case 1:
				cout << "請輸入基數 :" << endl;
				cin >> base;
				cout << "請輸入指數 :" << endl;
				cin >> expo;
				if (base == 0)  // 判別基數是否為0
				{
					if (expo == 0)
					{
						cout << "無定義" << endl;
					}
					else if (expo > 0)
					{
						cout << "0" << endl;
					}
					else 
					{
						cout << "無意義" << endl;
					}
				}
				else
				{
					cout << "答案為 :" << power (base,expo) << endl;  // 將使用者輸入的base,expo傳入power function 做運算
				}	
					break;

			case 2:
				cout << "請輸入要選擇的項數 :" << endl;
				cin >> n;
				cout << "第 " << n << " 項 : " << Fibonacci (n-1) << endl;  // 把使用者輸入的項數傳入 function 做運算
				break;

			case 3:
				cout << endl;
				cout << nrFibonacci(0,1) << endl;  // 呼叫 nrFibonacci function
				break;

			case 4:
				loop = false;  // 把loop 的布林運算值改為 false 讓使用者跳出迴圈結束
				break;

		}
	}
	system ("pause");
	return 0;
}
int power (int Base,int Expo)
{
		if (Expo == 0 || Base == 1) // 若基數為1或指數為0,則直接回傳1
		{
			return 1;
		}
		else
		{
			return Base*power (Base,Expo-1);  // 利用 recursive 算出乘方的值
		}
	
}
int Fibonacci (int number)
{
	if (number == 0  || number == 1)  // 定義第1項和第2項的值為0和1 
	{
		return number;
	}
	else 
	{
		return Fibonacci (number-1) + Fibonacci (number-2);  // 利用 recursive 算出費式數列
	}
}

int nrFibonacci(int k_1,int k_2) 
{
	int counter = 1;
	int max;
	while (true)
	{
		cout << "第" << counter << "項 : " << k_1 << endl; // 顯示第幾項
		counter++;
		if (k_2 < 0 && k_1 > 0)  // 當 overflow 時,值會變負數,就跳出迴圈
		{
			max = k_1;
			cout << "所能儲存的最大數值為 : ";
			break;
		}
		else if (k_1 < 0 && k_2 > 0)  // 當 overflow 時,值會變負數,就跳出迴圈
		{
			max = k_2;
			cout << "所能儲存的最大數值為 : ";
			break;
		}
		cout << "第" << counter << "項 : " << k_2 << endl; // 顯示第幾項
		counter++;
		k_1 = k_1 + k_2;
		k_2 = k_1 + k_2;
	}
		return max;	
}