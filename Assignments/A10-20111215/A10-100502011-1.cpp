#include<iostream>
#include<cstdlib>
using namespace std;

double power (double , int ); //運算指數的FUNCTION
int Fibonacci(int ); //運算Fibonacci 數列的FUNCTION
void nrFibonacci(); //顯示電腦能跑出的最大的Fibonacci值

int main()
{
    int choose,exponent,number; //choose是想選的FUNCTION  exponent是指數 number是想要的項數
    double base; //底數
    while (choose!=4) //當CHOOSE=4的時候跳出
    {
        cout << "Which function do you wnat to choose?" << endl
            << "1. The power function " << endl
            << "2. The Fibonacci function " << endl
            << "3. The nrFibonacci function " << endl
            << "4. Exit " << endl;
        cin >> choose;
        switch(choose) //選擇功能
        {
            case 1:
                cout <<"Enter the base number :";
                cin >> base;
                cout << "\nEnter the exponent number :";
                cin >> exponent;
                cout << "\npower(" << base << " ," << exponent  << ") = " << power(base,exponent) << endl;
                break;
            case 2:
                cout << "Please enter the number what you wnat to know : ";
                cin >> number;
                if(number<0)
                    cout << "Error!!!" << endl;
                else
                    cout << " Fibonacci(" << number << ") is " << Fibonacci(number) << endl;
                break;
            case 3:
                nrFibonacci();
                break;
            case 4:
                cout << "Thanks for your using!!";
                return 0;
                break;
            default:
                break;
        }
    system("pause");
    system("cls"); //刷新頁面
    }
}

double power(double base,int exponent) //計算出指數函數之值
{
    if(exponent==0)
        return 1;
    else
        return base*power(base,exponent-1);
}

int Fibonacci(int number) //求出需要的項數的Fibonacci數列值
{
    if(number==0 ||number==1) //遞迴
        return number;
    else
        return Fibonacci(number-1)+ Fibonacci(number- 2);
}

void nrFibonacci() //顯示電腦能跑出的最大Fibonacci數列之值的FUNCTION
{
    int y=0; //初始值
    int array[100] ={}; //陣列
    while (y!=-1)
    {
        array[0]=0;
        array[1]=1;
        array[y]=array[y-1]+array[y-2];
        if(array[y]<0)
            y = -1;
        else
        {
            if(y==0 || y==1)  //遞迴
                cout << "Fibonacci(" << y << ")  is " << y << endl;
            else
                cout << "Fibonacci(" << y << ")  is " << array[y-1]+array[y-2] << endl;
                y++;
        }
    }
}
