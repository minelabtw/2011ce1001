#include<iostream>
#include<cstdlib>
#include<iomanip>
using namespace std;

double power(double,int);//function prototype

int Fibonacci(int);//function prototype


int nrFibonacci(int);//function prototype

int main()
{
    int select,exponent,num;
    double base;
    bool key=true;// declare a bool to control while loop
    cout<<"<Assignment 10>";
    while(key)//use key to determine whether the loop continue or not
    {
        cout<<"\n1.power\n2.Fibonacci\n3.nrFibonacci\n4.quit.."<<endl;
        cout<<"choose the function: ";
        cin>>select;
        switch(select)//use Switch statement to choose which function is going to be used
        {
            case 1://to show power function
                cout<<"base: ";
                cin>>base;
                cout<<"exponent: ";
                cin>>exponent;
                if(exponent>=0)//check the exponet is more than 0
                    cout<<"result: "<<power(base,exponent)<<endl;
                else
                    cout<<"wrong exponent"<<endl;
                break;
            case 2://to show Fibonacci function
                cout<<"input the term that you want to see: ";
                cin>>num;
                if(num>=0)//check the term number is more than 0
                    cout<<"the "<<num<<" term is "<<Fibonacci(num)<<endl;
                else
                    cout<<"wrong term"<<endl;
                break;
            case 3://to show every Fibonacci number in tabular format
                cout<<"term"<<setw(16)<<"number"<<endl;
                for(int i=0;nrFibonacci(i)>=0;i++)//if overflow ,the sign would be negative
                {
                    cout<<setw(4)<<i<<setw(16)<<nrFibonacci(i)<<endl;
                }
                break;
            case 4://let key=0 and end the loop
                key=false;
                break;
            default:
                cout<<"wrong number"<<endl;
                break;
        }
    }

    system("pause");
    return 0;
}

double power(double base,int exp)//a recursive function returnsbase^exponent
{
    if(exp==0)//special case with exp=0
        return 1;
    if(exp==1)//base case
        return base;
    else//recursion step
        return base*power(base,exp-1);
}

int Fibonacci(int number1)//a recursive function returns Fibonacci number
{
    if(number1==0||number1==1)//base case
        return number1;
    else//recursion step
        return Fibonacci(number1-1)+Fibonacci(number1-2);
}

int nrFibonacci(int number2)//a non-recursive function returns Fibonacci number
{
    int ans1,ans2;//declare two integer to record the value
    for(int j=0;j<=number2;j++)
    {
        if(j==0)
            ans1=0;
        else if(j==1)
            ans2=1;
        else if(j%2==0)//if j is even ,assign a new sum to ans1
            ans1+=ans2;
        else//if j is odd ,assign a new sum to ans2
            ans2+=ans1;
    }
    if(number2%2==0)//if number2 is even ,return ans1
        return ans1;
    else//if number2 is odd ,return ans2
        return ans2;
}
