#include<iostream> //allows program to perform input and outout
#include<cstdlib> //allows program to use system pause
using namespace std; //enables program to use all the names in any standard C++ header file

double base;
int exponent;
int choose;
int number;
int former=0;
int temporary;
int later=1;

//function power to calculate base^exponent
double power(double base,int exponent)
{
    if (exponent==0) //base case
        return 1;
    else //recurison step
        return base*power(base,exponent-1);
} //end function power

//function Fibonacci to show Fibonacci series in recursive type
unsigned long Fibonacci(unsigned long number)
{
    if(number==0) //first nubmer
        return 0;
    if(number==1) //base case
        return number;
    else //recurison step
        return Fibonacci(number-1)+Fibonacci(number-2);
} //end function Fibonacci

//function nrFibonacci to show Fibonacci series in non-recursive type
int nrFibonacci()
{
    for(int i=0; later>=0; i++) //for loop to show Fibonacci series
    {
        if(i==0) //the first number
        {
            later = 0;
        }
        if(i==1) //the second number
        {
            later = 1;
        }
        else //logic arithmetic
        {
            temporary = later;
            later = later + former;
            former = temporary;
        }
        if(later>=0) //if statement to show the series and to exclude the number when it overflows
            cout << "Fibonacci(" << i+1 << ") = " << " " << later << endl;
    } //end for loop
    return later;
} //end function nrFibonacci

//function main begins program execution
int main()
{
    while(true) //while loop for runnig program again
    {
        cout << "1.A power function to calculate base^exponent\n"
             << "2.A fibonacci function to show Fibonacci series\n"
             << "3.Show the largest int Fibonacci number that can be printed on my computer\n"
             << "and list every Fibonacci number in tabular format\n"
             << "4.exit\n\n"
             << "Your choice is: ";
        cin >> choose; //prompt the user for choice
        cout << endl;
        if(choose==4) //the "exit" ability
            break; //exit while loop
        switch(choose) //switch statement for the several functions
        {
            case 1: //case for power function
                cout << "Enter the base and  the exponent to calculate power function\n"
                    << "Base: ";
                cin >> base; //prompt the user for base
                cout << "Expoent: ";
                cin >> exponent; //prompt the user for exponent
                if (exponent==0&&base==0) //if statement for incorrect condition
                {
                    cout << "It doesn't exist!!!\nNow input again.";
                    break;
                } //end if statement
                cout << "The result is: " << power(base,exponent); //call the function power
                break; //exit switch

            case 2: //case for recursive Fibonacci function
                cout << "Enter the number that the Fibonacci series contains: ";
                cin >> number; //prompt the user for the number of Fibonacci series
                if(number<0) //if statement for incorrect condition
                {
                    cout << "Wrong input!!!\nNow input again.";
                    break;
                }//end if statement
                for(int counter=1; counter<=number; counter++) //for loop to print every Fibonacci numbers
                {
                    cout << "Fibonacci(" << counter << ") = " << Fibonacci(counter-1) << endl; //call the function Fibonacci
                } //end for loop
                break; //exit switch

            case 3: //case for non-recursive Fibonacci function
                nrFibonacci(); //call the function nrFibonacci
                break; //exit switch

            default: //catch all other characters
                cout << "Wrong input!!!\nNow input again.";
                break; //exit switch

        } //end switch statement
        system("pause");
        system("cls");
    } //end while loop
    return 0; //indicate that program ended successfully
} //end function main
