#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

int power(int,int); // power function
int Fibonacci(int); // Fibonacci function
void nrFibonacci(); // caculate the limit of Fibonacci in int type

int main(void){
	int choice;
	int enter_base, enter_exponent;
	int num;
	bool run = true;
	
	while(run){
		cout << "1. Power function" << endl;
		cout << "2. Fibonacci function" << endl;
		cout << "3. The maximum in Fibonacci function" << endl;
		cout << "4. Exit" << endl;
		cout << "Please choose an action: ";
		cin >> choice;
		switch(choice){
			case 1:
				cout << "Please enter two integers for base and exponent: ";
				cin >> enter_base >> enter_exponent;
				if(enter_exponent < 0){ // test the input is right or not
					cout << "WRONG!!" << endl;
					break;
				}
				cout << power(enter_base, enter_exponent) << endl << endl; // use power function
				break;
			case 2:
				cout << "Please enter a positive integer: ";
				cin >> num;
				if(num <= 0){ // test the input is right or not
					cout << "WRONG!!" << endl;
					break;
				}
				cout << Fibonacci(num) << endl << endl; // use Fibonacci function
				break;
			case 3:
				nrFibonacci();
				cout << endl;
				break;
			case 4:
				run = false; // quit
				break;
		}
	}
	
	system("pause");
	return 0;
}

int power(int base, int exponent){ // power function
	if(exponent == 0)
		return 1;
	else
		return (base * power(base, exponent - 1));
}

int Fibonacci(int n){ // Fibonacci function
	if(n == 1)
		return 0;
	else if(n == 2)
		return 1;
	else
		return Fibonacci(n-1)+Fibonacci(n-2);
}

void nrFibonacci(){
	int one = 0, two = 1, sum;
	
	cout << setw(3) << "1" << ": " << one << endl; // 因第1 2 項無軌跡可循，因此直接輸出 
	cout << setw(3) << "2" << ": " << two << endl;
	for(int i = 3; ; i++){ // 從第3項開始，值(sum)為前兩項的和 
		sum = one + two;
		one = two;
		two = sum;
		if(sum > 0) // 若sum無溢位則輸出 
			cout << setw(3) << i << ": " << sum << endl;
		else
			break;
	}
}
