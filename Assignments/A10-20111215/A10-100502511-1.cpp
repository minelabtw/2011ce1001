#include <iostream>
#include <cstdlib>
using namespace std;

unsigned int power (unsigned int base,unsigned int exponent)  // power function
{
        if (exponent == 0)
        {
             return 1;
        }
        else if(exponent == 1)
        {
             return base;
        }
        else 
        {
             return  base*power(base,exponent-1);
        }
}
                         
int fibonacci (unsigned int number)  // fibonacci function
{
        if ((number == 0) || (number == 1))
        {
             return number;
        }
        else
        {
             return fibonacci(number-1)+fibonacci(number-2);
        }
}

int nrFibonacci()  // function that show Maximum in fibonacci
{
    int Maximum;
    for (int a=0;;a++)
    {
        if (fibonacci(a)<0)  // 如果溢位，則停止輸出數列 
        {
             break;
        }
        cout << "fibonacci" << "(" << a << ") = " << fibonacci(a) << endl;
        Maximum = fibonacci(a);
    }  
    cout << "Maximum is " << Maximum << endl;
}
// 跑到 fibonacci(46) 之後則會顯示Maximum，但後面過程速度較慢 
 
int main()
{
    int basenumber;
    int exponentnumber;
    int fibonaccinumber;
    int choosefunction1;
    int choosefunction2;
    for (;;)
    {
        cout << "1:Power 2:Fibonacci 3:End program" << endl;
        cout << "Please choose the function you want: ";
        cin >> choosefunction1;
        switch (choosefunction1)
        {
               case 1:     
                    cout << "Please input the base: ";
                    cin >> basenumber;
                    if (basenumber<0)
                    {
                        cout << "Please retype the base, base must greater than 0!!!" << endl;
                        break;
                    }
                    cout << "Please input the exponent: ";
                    cin >> exponentnumber;
                    if (exponentnumber<0)
                    {
                        cout << "Please retype the exponent , exponent must greater than 0!!!" << endl;
                        break;
                    }
                    else if(exponentnumber == 0 && basenumber == 0)  // 此為Update的部分，當base and exponent皆為0時，則出現此訊息 
                    {
                         cout << "0的0次方是懸而未決的，多數領域則不定義，部分領域定義為0 -- WIKI " << endl ;
                         break;
                    }
                    cout << "(" << basenumber << "," << exponentnumber << ")" << " = " << power(basenumber,exponentnumber) << endl;
                    break;  
               
               case 2:
                    cout << "1:Input the number you want to show in Fibonacci 2:Show the Maximun" << endl;
                    cout << "Please choose one to show: ";
                    cin >> choosefunction2;
                    switch (choosefunction2)
                    {
                           case 1:
                                cout << "Please input the fibonaccinumber: ";
                                cin >> fibonaccinumber;
                                 if (fibonaccinumber<0)  // 此為Update2的部分
                                 {
                                     cout << "Please retype the fibonaccinumber , fibonaccinumber must greater than 0!!!" << endl;
                                     break;
                                 }
                                cout << "fibonacci" << "(" << fibonaccinumber << ") = " << fibonacci(fibonaccinumber) << endl;                
                                break;
                           
                           case 2:
                                nrFibonacci();
                                break;
                    }
                    break;
                     
               case 3:
                    system("pause");
                    return 0;   
                    break; 
               
               default:  
                    cout << "Please retype the number!!!" << endl;
                    break;
        }    
    }
}
