#include<iostream>
#include<cstdlib>
using namespace std;

int power(int,int);
int Fibonacci(int);
int nFibonacci(int);
int base,exponent;
int choice,counter,counter1;
int temp;

int main()
{
    cout<<"which function do you to use:"<<endl;
    cout<<"1.power function"<<endl;
    cout<<"2.recursive function Fibonacci"<<endl;
    cout<<"3.non-recursive function nrFibonacci"<<endl;
    cin>>choice;
    cout<<endl;
    
    switch(choice)
    {
        case 1: //按1選擇使用次方之功能 
            cout<<"enter the base:"<<endl;
            cin>>base;
            cout<<"enter the exponent:"<<endl;
            cin>>exponent;
            cout<<"the answer is:"<<power(base,exponent)<<endl;
            break;
        case 2: //按2選擇使用費氏數列 (遞迴形式)選擇輸出之項數 
            cout<<"please input the argument:"<<endl;
            cin>>counter;
            cout<<"the answer is:"<<Fibonacci(counter)<<endl;
            break;
        case 3: //按3選擇使用費氏數列 (非遞迴形式)輸出至int格式可存之最大整數 
            for(counter1=1;counter1<=50;counter1++)
            {
                if(nFibonacci(counter1)>=0)
                {
                    cout<<counter1<<" "<<nFibonacci(counter1)<<endl;
                }
                else
                {
                    break;
                }
            }
            break;//輸入錯誤 
        default:    
            break;
    }
    system("pause");
    return 0;
}
int power(int base,int exponent)
{
    if(exponent==1)//次方數為1，return base 
    {
        return base;
    }
    else if(exponent==0)//次方數為0，return exponent 
    {
        return 1;
    }
    else if(exponent<0)//次方數小於0，錯誤 
    {
    }
    else
    {
        return base*power(base,exponent-1);
    }
}
int Fibonacci(int counter)//費氏數列 (recursive) 
{
    if((counter==1)||counter==0)
    {
        return counter; 
    }
    else
        return Fibonacci(counter-1)+Fibonacci(counter-2);
}
int nFibonacci(int counter1)//費氏數列 (non-recursive)
{
    int num1=0;
    int num2=1;
    static int answer=0;
    if(counter1==1)
    {
        return 0;
    }
    else if(counter1==2)
    {
        return 1;
    }
    else
    {
        for(int counter2=1;counter2<=counter1-2;counter2++)//費氏數列每一項為前兩項之合 
        {
            answer=num1+num2;
            num1=num2;
            num2=answer; 
        }
            return answer;
    }
}










