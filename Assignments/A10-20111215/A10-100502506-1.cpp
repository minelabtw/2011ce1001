#include<iostream>
#include<iomanip>
#include<cstdlib>
using namespace std;


double power(double base,int exponent)//power 函式 
{
    
    if(base==0)//底數為零 
    {
        if(exponent>=0)// 0的正數&0 次方 
        {
            return 0;
        }
        else if(exponent<0)// 0的負數次方 
        {
            cout<<"無意義\n";   
        }
    }
    
    else if(exponent>=0)//底數為正數 
    {
        if(exponent==0)
        {
            return 1; 
        } 
        else if(exponent==1) 
        {
            return base;
        }
        else if(exponent%2==0) //偶數 次方 
        {
            return power(base,exponent/2)*power(base,exponent/2);//拆成 偶數次方 * 偶數次方 
        }
        else //奇數 次方
        {
            return power(base,exponent/2)*power(base,exponent-exponent/2);//拆成 偶數次方 *奇數次方 
        }
    
    }
    else if(exponent<0)//底數為負數 
    {
        if(exponent==-1) 
        {
            return 1/base;
        }
        else if((-exponent)%2==0) 
        {
            return 1/(power(base,(-exponent)/2)*power(base,(-exponent)/2));
        }
        else if((-exponent)%2==1) 
        {
            return 1/(power(base,(-exponent)/2)*power(base,(-exponent)-(-exponent)/2));
        }
    }
}






int Fibonacci(int step)//費式數列   遞迴 
{
    if(step==0) //第1項 
    {
        return 0;
    }
    else if(step==1)//第2項 
    {
        return 1;
    }
    else
    {
        return Fibonacci(step-1)+Fibonacci(step-2);//第三項等於前兩項的合 
    }
}
int nrFibonacci()//費式數列  迴圈 
{
    int step=100;
    int fibonacci[step];
    int times=1;
    fibonacci[0]=0;//第1項 
    fibonacci[1]=1;//第2項 
    
    for(int i=2;i<step;i++)
    {
        fibonacci[i]=fibonacci[i-1]+fibonacci[i-2];//第三項等於前兩項的合
        times+=1;
        if(fibonacci[i]<fibonacci[i-1])//如果前一項比後一項大 跳出迴圈  (overflow的數字為負數) 
            break;
    }
    for(int i=0;i<times;i++)
    {
        cout<<"fibonacci["<<i<<"] = "<<fibonacci[i]<<endl;
    }
    
}


int main()
{
    bool flag=false;//EXIT flag 
    do
    {
        cout<<"請輸入要使用的函式:\n"; 
        cout<<"1.Power fuction\n";
        cout<<"2.Fibonacci Series\n";
        cout<<"3.nrFibonacci\n"; 
        cout<<"4.Exit!\n";
        
        int question1;
        double inbase;
        int inexponent;
        int inputStep;
        cin>>question1;
        
        system("cls");
        
        switch(question1)
        {
            case 1://power 函式 
                cout<<"請輸入 實數 和 整數次方 \n";
                cin>>inbase>>inexponent;
                if(inbase==0&&inexponent<0)
                {
                    power(inbase,inexponent);
                    system("pause");
                    system("cls");
                    continue;
                    
                }
                cout<<setprecision(2)<<fixed<<inbase<<"^"<<inexponent<<" = "<<setprecision(7)<<power(inbase,inexponent)<<endl;
                system("pause");
                system("cls");
                break;
                
            case 2://費式數列 遞迴 
                cout<<"請輸入要顯示到第幾階\n"; 
                cin>>inputStep;
                for(int i=0;i<=inputStep-1;i++)
                {
                    cout<<"Fibonacci("<<i<<") = "<<Fibonacci(i)<<endl;
                }
                system("pause");
                system("cls");
                break;
                
            case 3://費式數列 迴圈 
                nrFibonacci();   
                system("pause");
                system("cls");
                break;
                
            case 4://EXIT
                flag=true;
                break;
                
            default:
                cout<<"Error 請重新輸入\n";
                system("pause");
                system("cls");
                break;
        }            
    }while(flag==false);         
    system("pause");
    return 0;
}
