#include<iostream>//allow program to input and output
#include<cstdlib>
using namespace std;

int power(int,int);//power function prototype
int recursiveFi(int);//recursiveFi function prototype
void nonrecursiveFi();//nonrecursiveFi function prototype

int main()//main function to execute
{
    int judge=1,choice,BASE,EXP,number;//declare the varibles
    while(judge!=0)
    {
            cout << "Please enter your choice : \n 1:power \n 2:recursiveFi \n 3:nonrecursiveFi : \n 4:停止(否則繼續輸入) \n\n";//選擇要進行的判斷 
            cin >> choice;//輸入1~4選擇問題 
            switch(choice)
            {
                     case 1:   
                               cout << "Please enter the base and exponent to calculate the power : \n";
                               cout << "base : ";
                               cin >> BASE;//輸入底數 
                               cout << "exponent : ";
                               cin >> EXP;//輸入指數 
                               cout << BASE << "^" << EXP << " = " << power(BASE,EXP) << "\n" << endl;//輸出exp的結果 
                     break;//跳出switch，繼續while迴圈 
                     case 2:
                               cout << "Please enter the 項數 you want to know : ";
                               cin >> number;//輸入費式數列的項數 
                               if(number<=0)
                                       cout << "此項不存在喔!!" << endl;
                               else 
                                       cout << "第" << number << "項 : " << recursiveFi(number) << endl;//輸出該項的結果 
                     break;//跳出switch，繼續while迴圈 
                     case 3:
                               nonrecursiveFi();//輸出費式數列可在電腦中的各項到最大的輸出結果 
                     break;//跳出switch，繼續while迴圈 
                     case 4:
                               judge=0;// 判斷跳出迴圈 
                     break;//跳出switch，結束while迴圈 
                     default:
                               cout << "沒有該選項唷!!\n" << endl; 
                     break;//跳出switch，繼續while迴圈
            }
    }
    system("pause");
    return 0;
}

int power(int base,int exponent)
{
    if(exponent==0)// 若指數為0則回傳1的整數值 
            return 1;
    else// 若指數不為0者，繼續呼叫自己的function，直到指數計算為0，回傳一數值 
            return base*power(base,exponent-1);
            
}
int recursiveFi(int order)
{
    if(order==1)//第一項定義為0 
            return 0;
    else if(order==2)//第二項定義為1 
            return 1;
    else //其他項的數為前面兩項的數相加 ，直到計算到第1，2項才回傳值 
            return recursiveFi(order-2)+recursiveFi(order-1);
}
void nonrecursiveFi()
{
    int Fi[100]={};//先初始化費式數列的陣列 
    cout << 0 << "\n" << 1 << "\n";//先輸出第1，2項的值 
    for(int th=3;;th++)
    {
            Fi[1]=0;//第一項定義為0 
            Fi[2]=1;//第二項定義為1 
            Fi[th]=Fi[th-2]+Fi[th-1];//其他項的數為前面兩項的數相加 
            if(Fi[th]<0)//若該數為0，則代表溢位 
            {
                        cout << "電腦能跑到第 " << th-1 << " 項 \n該項為 " << Fi[th-1] << endl;//輸出最大值 
                        break;//跳出迴圈 
            }
            else
                        cout << "第" << th << "項 = " << Fi[th] << endl;//輸出費式數列中各項的數 
    }
}
