//A10-100502503-1: Power numbers & fibonacci series.
#include <iostream>
#include <cstdlib>
#include <iomanip>

using namespace std;
double power(double, int);//declare the functions.
long long fibonacci(int);
void nrfibonacci();

int main()
{
    while(1)//make a loop to control the program.
    {
        int choice;
        //choose the statement.
        cout << "Please choose the statement"
             << "\n1: caculate the power numbers."
             << "\n2: caculate the fibonacci."
             << "\n3: show the largest number of fibonacci"
             << "\n0: Quit the program." << endl;

        cin >> choice;

        switch(choice)
        {
            case 1:
                double power_base;
                int power_exponent;

                cout << "Please enter the base : ";
                cin >> power_base;
                cout << "\nPlease enter the exponent: ";
                cin >> power_exponent;
                cout << "The answer: " << power( power_base, power_exponent ) << endl;

                break;

            case 2:
                int key_in;

                cout << "Enter the number you waant to caculate: ";
                cin >> key_in;

                cout << "\nThe answer is: " << fibonacci(key_in) << endl;

                break;

            case 3:
                nrfibonacci();
                break;

            case 0://The function to quit the program.
                system("pause");
                return 0;
                break;

            default:
                cout << "Error" << endl;
                break;
        }
    }
}

double power( double base, int exponent )//caculate the power number
{
    if( exponent == 1 )
        return base;

    else
        return base*power( base,exponent-1 );//a*a^(n-1)
}

long long fibonacci( int key_in )//caculate the fibonacci series
{
    if( key_in == 1 )
        return 0;

    else if( key_in == 2 )
        return 1;

    else
        return fibonacci( key_in-1 ) + fibonacci( key_in-2 ); //n = (n-1)+(n-2)
}

void nrfibonacci()
{
    int num = 1;
    int a=0;
    int b=1;
    int c;

    cout << "th" << setw(20) << "total" << endl;

    while(b>=a)//if the number went in overflow, (n+1)th would smaller than nth, use it as the consition.
    {
        if( num == 1 )
            c = 0;

        else if( num == 2)
            c = 1;

        else        //shift the (n-2) to (n-1), (n-1) to n, n=(n-1)+(n-2).
         {
            c = a+b;
            a = b;
            b = c;
         }

        if(b>=a)//make a condition to determinate where to stop.
            cout << num << setw(20) << c << endl;
        num++;
    }

}
