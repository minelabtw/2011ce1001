#include<iostream>
#include<cstdlib>
using namespace std;

int power(int,int);//計算次方
int Fibonacci(int);//以遞迴函式計算指定的費式數列
void nrFibonacci();//計算最大的費氏

int main()
{
    int choose=0;//選擇
    int base;//基底
    int exponent;//指數
    int number;//儲存用的變數

    while(choose!=4)
    {
        cout << "Choose your action: \n" << "1.power(base,exponent)\n"
             << "2.Fibonacci(n)\n" << "3.nrFibonacci()\n"
             << "4.exit\n" << "You want ";
        cin >> choose;

        while(choose<1 || choose>4)//判斷輸入選項是否存在
        {
            cout << "Invalid choose,please try again: ";
            cin >> choose;
        }

        switch(choose)
        {
            case 1:
                cout << "\nEnter your base and exponent(exponent is an integer greater than or equal to 1): ";
                cin >> base >> exponent;

                while(exponent<1)//判斷指數是否合乎條件
                {
                    cout << "exponent is an integer greater than or equal to 1\n";
                    cin >> base >> exponent;
                }

                cout << "answer: " << power(base,exponent) << "\n\n";
                break;

            case 2:
                cout << "\nEnter the number: ";
                cin >> number;

                cout << "\nFibonacci(" << number << ") is " << Fibonacci(number) << "\n\n";
                break;

            case 3:
                nrFibonacci();
                break;

            case 4:
                break;
        }
    }

    system("pause");

    return 0;
}

int power(int base,int exponent)
{
    if(exponent==1)//一次方為基底
        return base;
    else
        return base*power(base,exponent-1);
}

int Fibonacci(int number)
{
    if(number==0 || number==1)//給前兩項值
        return number;
    else
        return Fibonacci(number-1)+Fibonacci(number-2);
}

void nrFibonacci()
{
    int number1=0;//啟始第一項
    int number2=1;//啟始第二項
    int sum;//總和

    cout << endl;

    for(int k=0;;k++)
    {
        sum=number1+number2;

        if(sum<0)//溢位時跳出迴圈
        {
            cout << "\nThe max is nrFibonacci(" << k-1 << ") = " << number2 << "\n\n";

            break;
        }

        cout << "nrFibonacci(" << k << ") = ";

        if(k==0)
            cout << number1 << endl;
        else if(k==1)
            cout << number2 << endl;
        else
        {
            number1=number2;//移位

            number2=sum;

            cout << sum << endl;
        }
    }
}
