#include <iostream>
#include <cstdlib>
using namespace std;

// 計算實數的正整數次方
double power( double base, int exponent )
{
    if ( exponent == 0 )
        return 1;
    else // recursive step
        return base * power( base, exponent -1 );
}
// 計算費式數列的某一特定項
int fibonacci( int term )
{
    if ( term == 1 )
        return 0;
    else if ( term == 2 )
        return 1;
    else // recursive step
        return fibonacci( term - 1 ) + fibonacci( term - 2 );
}
// 顯示費式數列在電腦上計算正確的最大值
int nrFibonacci()
{
    int term1 = 0, term2 = 1; // 初始前兩項的值
    for ( int counter = 1; term2 > 0 ; counter++ ) // 顯示費式數列每一項的值
    {
        if ( counter == 1 || counter == 2 )
            cout << "fibonacci( " << counter << " ) = " << counter - 1 << endl;
        else
        {
            int hold = term2;
            term2 += term1;
            term1 = hold;
            if ( term2 > 0 )
                cout << "fibonacci( " << counter << " ) = " << term2 << endl;
        }
    }
}

int main()
{
    // declare and initialize the variable
    int choose;
    bool flag = true;
    double base;
    int exponent;
    int term_n;

    while ( flag ) // while loop let program repeatedly execute
    {
        // show the functionality list
        cout << "1.Power function\n"
            << "2.Recursive Fibonacci function\n"
            << "3.Non-recursive Fibonacci function\n"
            << "4.Exit\n"
            << "Choose the function that you want to use: ";
        cin >> choose;
        switch ( choose ) // use switch case to make a choice
        {
            case 1: // calculate base^exponent
                cout << "Please enter the base: ";
                cin >> base;
                cout << "Please enter the exponent: ";
                cin >> exponent;
                if ( base == 0 && exponent == 0 )
                    cout << "0 to the power of 0 is not defined." << endl;
                else
                {
                    if ( exponent >= 0 )
                        cout << "Answer: " << power( base, exponent ) << endl;
                    else
                        cout << "Sorry,this function can't accept negative exponent." << endl;
                }
                break;
            case 2: // calculate fibonacci series
                cout << "Please enter the term of fibonacci series that you want to know: ";
                cin >> term_n;
                if ( term_n > 0 )
                    cout << "Result: " << fibonacci( term_n ) << endl;
                else
                    cout << "Error!Please enter again.\n" << endl;
                break;
            case 3: // display the largest integer Fibonacci number that can be printed on the computer
                nrFibonacci();
                break;
            case 4: // exit the program
                flag = false;
                break;
            default: // display error message
                cout << "Error!Please choose again." << endl;
                break;
        }
        system("pause");
        system("cls");
    }
}
