#include <cstdlib>
#include <iostream>

using namespace std;

int power(int base,int exp){
    int result=1;
    for (int i=0;i<exp;i++)
        result = result * base;
    return result;
}

void Fibonacci(int n){
    int front=0, back=1, temp;
    if (n==1)
        cout << front << endl;
    else if (n==2)
        cout << front << ", " << back << endl;
    else{
        cout << front << ", " << back;
        for (int i=0;i<n-2;i++){
            temp = back;
            back = front + back;
            front = temp;
            cout << ", " << back;
        }
        cout << endl;
    }
}

void nrFibonacci(){
    int front=0, back=1, temp;
    cout << front << ", " << back;
        for (int i=0;i>=0;i++){
            temp = back;
            back = front + back;
            front = temp;
            if (back>0)
                cout << ", " << back;
            else
                break;
        }
        cout << endl;
}


int main(){
    int choice, B, E, n;
    
    while(true){
        cout << "1 power" << endl;
        cout << "2 recursive Fibonacci" << endl;
        cout << "3 non-recursive Fibonacci" << endl;
        cout << "0 for exit" << endl;
        cout << "Which function do you want:";
        cin >> choice;
        switch(choice){
            case 0:
                return 0;
            case 1:
                cout << "Base:";
                cin >> B;
                cout << "Exponent:";
                cin >> E;
                cout << B << "^" << E << "=" << power(B,E) << endl;
                break;
            case 2:
                cout << "n:";
                cin >> n;
                Fibonacci(n);
                break;
            case 3:
                nrFibonacci();
                break;
        }
    }
}
