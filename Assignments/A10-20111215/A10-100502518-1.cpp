#include<iostream>
#include<cstdlib>
using namespace std;

int part1(int a,int b)//算a的b次方的函式
{
	if(b==0)//0次方是1
		return 1;
	
	else if(b==1)//1次方的話就回傳自己
		return a;
	
	else//次方漸減並把a堆疊起來
	{
		a=a*part1(a,b-1);
	}
}

int part2(int n)//費氏數列的函式
{
	if(n==0||n==1)//頭2項
		return n;
	else
		return part2(n-1)+part2(n-2);//第3項之後任一項為前2項的合
}

int main()
{
	int choice=1;
	
	while(choice!=0)
	{
		cout<<"1看part1，2看part2-1、3看part2-2、0結束: ";
		cin>>choice;

		switch(choice)
		{
		case 1:
			int base,exponent;
			cout<<"請輸入底數: ";
			cin>>base;
			cout<<"請輸入次方: ";
			cin>>exponent;

			if(exponent<1)
			{
				cout<<"輸入錯誤"<<endl;
			}
			else
			{
				cout<<"結果為: "<<part1(base,exponent)<<endl;
			}
			break;

		case 2:
			int n;
			cout<<"請輸入到第幾項: ";
			cin>>n;
			if(n<1)
			{
				cout<<"輸入錯誤"<<endl;
			}
			else
			{
				n=n-1;//0本來對到0，1本來對到1，但第1項是0所以要減1
				cout<<"結果為: "<<part2(n)<<endl;
			}
			break;

		case 3:
			int last1,last2;//依然是費氏數列
			last1=0;
			last2=1;
			
			cout<<last1<<endl;//先顯示頭2項
			cout<<last2<<endl;
			while(last1>=0&&last2>=0)
			{
				last1=last1+last2;//第3項之後任一項為前2項的合，但如果小於0代表溢位，就跳出迴圈
				if(last1<0)
					break;
				cout<<last1<<endl;
				
				last2=last1+last2;
				if(last2<0)
					break;
				cout<<last2<<endl;
			}
			break;
		case 0:
			break;
		}
	}

	system("pause");
	return 0;
}