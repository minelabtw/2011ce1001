
#include<iostream>
#include<cstdlib>

using namespace std;

//Define power function
int Power( int base , int power )
{
    if ( power == 1 )
    {
        return base ;
    }
    if ( power >= 2 )
    {
        return base * Power( base , power-1 ) ;
    }
}

//Define fibonacci
unsigned long Fibonacci( int fibonacci1 )
{
    if ( ( fibonacci1 == 1 ) || ( fibonacci1 == 0 ) )
        return fibonacci1 ;

    else
        return Fibonacci( fibonacci1-1 ) + Fibonacci( fibonacci1-2 ) ;
}

int counter1 = -1 ;
int counter2 = 1 ;
int counter3 = -2 ;
int control = 1 ;

void nrFibonacci()
{
                    while( counter3+10 >= counter1 )
                {
                    //Make fibonacci's function
                    {
                        counter3 = counter1 + counter2 ;
                        counter1 = counter2 ;
                        counter2 = counter3 ;
                    }
                    if( counter3 >= 0 )
                    {
                        cout << counter3 << "\n" << "\t"<< control << endl ;
                    }
                    control = control+1 ;
                }

}

int main()
{
    //Define variables
    int choose = 1 ;
    int base ;
    int power ;
    int number ;
    int fibonacci1 ;

    while( choose != 0 )
    {
        //Choose function
        cout << "If you want to calculate power,please key in 1.\n" ;
        cout << "If you want to calculate fibonacci with recursion,please key in 2.\n" ;
        cout << "If you want to calculate fibonacci with non-recursion,please key in 3.\n" ;
        cout << "If you want to leave,please key in 0." ;
        cin >> choose ;

        switch( choose )
        {
            case 1:
                //Use function for calculating power
                cout << "Please key in your number for base and for power.\n" ;
                cin >> base ;
                cin >> power ;
                Power( base , power ) ;
                cout << Power( base , power ) << endl ;
                break ;

            case 2:
                //Use function for calculating fibonacci with recursion
                cout << "Please key which number do you want to know.\n" ;
                cin >> fibonacci1 ;
                fibonacci1 = fibonacci1-1 ;
                Fibonacci( fibonacci1 ) ;
                cout << Fibonacci( fibonacci1 )<< endl ;
                break ;

            case 3:
                control = 1 ;
                //Define function for calculating fibonacci with non-recursion
                nrFibonacci();
                cout << "\nYour computer can show fibonacci's number till\t" << counter1 << "\t.\n" ;
                cout << "This number is\t" << control-2 << "th\tnumber of fibonaccis'.\n" ;
                break ;

            case 0:
                break ;

            default:
                cout << "There's something wrong.\n" ;
                break ;
        }
    }

    system( " pause " ) ;
    return 1 ;
}
