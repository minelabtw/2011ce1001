#include<iostream>
#include<cstdlib>
#include<iomanip>
using namespace std;

int power(int,int);
int Fibonacci(int);
void nrFibonacci();
int main()
{
	int a,b=0,num,exp,n; //宣告
	
	
	while(b==0)
	{
		cout<<"1.算次方,2.費氏數列,3.第幾個溢位,4.離開:"; //選擇
		cin>>a;
		switch(a)
		{
		case 1:
			cout<<"輸入數字及次方:";
			cin>>num>>exp;			
			power(num,exp);			
			cout<<"答案是:"<<power(num,exp)<<endl;
			break;
		case 2:
			cout<<"算幾項:";
			cin>>n;
			cout<<"答案是:"<<Fibonacci(n)<<endl;
			break;
		case 3:
			nrFibonacci();
			break;
		case 4:
			b=-1;
			break;

		}

	}

	system("pause");
	return 0;

}

int power(int num1,int exp1) //遞迴算出次方
{
	if(exp1<=1)
		return num1;
	else
	return num1*power(num1,exp1-1);
}

int Fibonacci(int n)
{
	if (n==1)
		return 0;
	else if (n==2||n==3)
		return 1;
	else
		return Fibonacci(n-1)+Fibonacci(n-2); //遞迴算出費氏數列
}

void nrFibonacci()
{
	int a=0,b=1,c=0;
	for(int i=0;i>=0;i++)
	{

		cout<<"第"<<i+1<<"項"<<a<<endl; //讓b c輪流等於前兩項
		a=b+c;
		if(i%2==0)b=a;
		if(i%2==1)c=a;

		if(a<0)
			break;		
	}
}