#include <iostream>
#include <cstdlib>
#include <string>

using namespace std;

int power ( int, int );//prototype of power function.
int rFibonacci ( int );//prototype of recursive Fibonacci Series Function.
int nrFibonacci ( int );//prototype of non-recursive Fibonacci Series Function.

int main( )
{
    int choice;//Choice to the Functions

    while ( 1 )//If it run, run it again.
    {


    cout << "1 for power, 2 for Fibonacci, 3 for Fibonacci limit, and other for exit." << endl;
    cin  >> choice;//Input your choice.

    switch ( choice )//Switch Statement. Reacting to your choice.
    {
        case 1://Power Function choosed
            int base, exponent;

            cout << "Please enter the Base: ";
            cin  >> base;
            cout << "Please enter the Exponent: ";
            cin  >> exponent;//Input the base and the exponent.
            cout << base << "^" << exponent << ": " << power ( base, exponent ) << endl;//Go to the function, and output the result.

            break;

        case 2://Recursive Fibonacci Series Function
            int term;

            cout << "Enter the Fibonacci Series you wanna show: ";
            cin  >> term;//Input the term you wanna show

            cout << rFibonacci( term ) << endl;//Go to the Function, and output the result.

            break;

        case 3://Non-recursive Fibonacci Series Function.
            for ( int terms = 0; ; terms++ )
            {

                if ( nrFibonacci ( terms ) >= 0 )//If the result is not negative.
                    cout << "Fibonacci (" << terms << "): " << nrFibonacci ( terms ) << endl;//Go to the Function, and output the result.
                else
                    break;//If the result is negative, get out of the for loop statement.
            }

            break;

        default://The exit choice.
            system ( "pause" );
            return 0;//Program ends.

    }
    system ( "pause" );
    system ( "cls" );//Clear the previous result.
    }
    system ( "pause" );
    return 0;
}

int power ( int base, int exponent )
{
    if ( exponent == 0 )//If the exponent is zero, the result is 1.
        return 1;
    else
        return base * power ( base, exponent - 1 );//If not, the program goes recursively as base * base ^ ( exponent - 1 ).
}

int rFibonacci( int number )
{
    if ( number == 0 || number == 1 )
        return number;//Return 0 or 1 if you input 0 or 1.
    else
        return rFibonacci ( number - 1 ) + rFibonacci ( number - 2 );//If not, the program goes recursively as F(n)=F(n-1)+F(n-2).
}

int nrFibonacci( int term )
{
    int element [ 50 ] = {  };//I've tried a lot of times, and the limit of the integer can let the loop run 47 times.
    //Therefore, I set an array with 50 elements.
    element [ 0 ] = 0;//F(0)= 0
    element [ 1 ] = 1;//F(1)= 1 as we know.

    if ( term == 0 )
        return element [ 0 ];//Term 0 is 0
    else if ( term == 1 )
        return element [ 1 ];//Term 1 is 1 as we know.
    else
    {
        for ( int i = 2; i <= term ; i++ )//If the the term you want is greater than 1,
        {
            element [ i ] = element [ i - 1 ] + element [ i - 2 ];//We add the last two term value to the next.
        }
        return element [ term ];//Output the result.
    }


}

