#include<cstdlib>
#include<iostream>
using namespace std;
int power(int base,int exponent)
{
    if(exponent==0)		 //當exponent被減到0時回傳1 
    {
        return 1;	
    }
    else		//指數(n)不等於0時回傳base與base^(n-1)的乘積 
    {  			//power(base,ex)=base^ex=base*(base^(ex-1))=base*(base*base^(ex-2))...
        return base*power(base,exponent-1);    
    }
}
int recursive_Fibonacci(int n)	
{
    if(n>0||n<48)
    {
		if(n==1)
    	{
        	return 0;
    	}
    	else if(n==2)
    	{	
			return 1;
		} 
    	else  //費式數列的每一項都可以拆解成前兩項的和,如此呼叫下去,直到n=0或者n=1 
    	{
        	return recursive_Fibonacci(n-1)+recursive_Fibonacci(n-2); //費氏數列的定義 
    	}
	}
    else
    {}
}

void non_recursive_Fibonacci(void)
{
    int number1=0;  //費氏數列第一項0,第二項1  
    int number2=1;
    int temp;
    for(int i=1;;i++)
    {
        if(i==1)
        {
            cout<<"Fibonacci("<<i<<")"<<":"<<number1<<endl; 
        }
        else if(i==2)
        {
            cout<<"Fibonacci("<<i<<")"<<":"<<number2<<endl;
        }
        else
        {
            temp=number2;			//前二項的值指定給前一項,前兩項值總和值指定給第三項 
            number2=number1+number2;//如此遞迴下去可完成費式數列 
            number1=temp;		
            if(number2>0)
            {
                cout<<"Fibonacci("<<i<<")"<<":"<<number2<<endl;
            }
            else 	//overflow時中斷迴圈 
            {
                break;
            }
            
        }
    }
    
}
int main()
{
    int userchoice;
    int base,exponent;
    int n;
    bool flag=1;
    while(flag)
    {
        cout<<"1.recursive function power"<<endl
            <<"2.recursive function Fibonacci"<<endl
            <<"3.non-recursive function Fibonacci"<<endl
            <<"4.exit"<<endl;
        cin>>userchoice;
        switch(userchoice)
        {
            case 1:
                cout<<"input base:";
                cin>>base;
                cout<<"input exponent:";
                cin>>exponent;
                cout<<power(base,exponent)<<endl;
                system("pause");
                system("cls");                
                break;
            case 2:
                cout<<"input a number n:";
                cin>>n;
                cout<<"Fibonacci("<<n<<")"<<":"<<recursive_Fibonacci(n)<<endl;
                system("pause");
                system("cls");
                break;
            case 3:
                non_recursive_Fibonacci();
                system("pause");
                system("cls");
                break;
            case 4:
                flag=0;
                break;
        }
    }
    system("pause");
    return 0;
}
