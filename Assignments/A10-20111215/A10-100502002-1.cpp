#include<iostream>
#include<cstdlib>
using namespace std;
double power(double base,double exponent)
{
    double result;
    if(exponent==0)
     return 1;//任何除了0的數外 指數為0的值為1
    else if(exponent<0)
    {
     exponent=-1*exponent;
     result=base*power(base,exponent-1);//a的b次方可以想成a*(a的b-1次方) 
     return 1/result;//負的指數就變成倒數囉 
    }
    else
     result=base*power(base,exponent-1);//a的b次方可以想成a*(a的b-1次方) 
     return result;
     
}//指數的function 
int Fibonacci(int number)
{
     if(number==0)
      return 0;
     else if(number==1)
      return 1;//定義第0項跟第1項的值 
     else
     number=Fibonacci(number-2)+Fibonacci(number-1);//值等於前兩項的值相加 
     return number;
     
}//用遞回寫費氏數列 
int nrFibonacci()
{
    int a=0;
    int b=1;
    int r;
    int i=2;
    cout<<"第0項  "<<0<<endl;//定義0 1項數值
    cout<<"第1項  "<<1<<endl;//定義0 1項數值 
    for(;;)
     {
          r=a+b;
          if(r<0)
          break;//如果結果<0就跳出迴圈 表示已經超過正整數範圍 
          a=b;//換來換去
          b=r;//換來換去 
          cout<<"第"<<i++<<"項  "<<r<<endl;//輸出結果 

     }
} 
           
    
int main()
{
    int choise;
    for(;;)
    {
           cout<<"請選擇功能 1.指數 2.費氏(1) 3.費氏(2) 4.離開"<<endl;//顯示功能列表 
           cin>>choise;//輸入選項 
           switch(choise)
           {
              case 1:
               double base;
               double exponent;
               cout<<"請輸入底數";
               cin>>base;
               cout<<"\n請輸入指數";
               cin>>exponent;
               if(exponent==0 && base==0)//0的0次方沒有意義
               {
                cout<<"0的0次方沒有意義"<<endl;
                break;
               } 
               cout<<base<<"的"<<exponent<<"次方:"<<power(base,exponent)<<endl;//呼叫指數function 
               break;
             case 2:
               int th;
               cout<<"請輸入您要顯示的項數";
               cin>>th;
               if(th<0)//項數怎麼可以是負的呢? 
               { 
                cout<<"請不要輸入負數好嗎?";
                break;
               } 
               cout<<"第"<<th<<"項為"<<Fibonacci(th)<<endl;//輸出要的答案 
               break;
            case 3:
               nrFibonacci();//直接呼叫function就好囉 
               break;
           }
           if(choise==4)
           break;//使用者想離開了,跳出迴圈 
    }                           
    system("pause");
    return 0;
}
