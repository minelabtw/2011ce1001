#include<iostream>
#include<cstdlib>

using namespace std;


int power(int ,int);//prototype
int Rfibonacci(int);//prototype
void NRfibonacci();//prototype



int main()
{
    int choice;
    while(choice!=4)//repeat input options
    {
        cout << "1.power\n2.inputFibonacci\n3.limitFibonacci\n4.exit\n";
        cin >> choice;
        switch(choice)
        {
            case 1://choose power function
                int Base,Expon;
                cout << "plz enter the base: ";
                cin >> Base;
                cout << "plz enter the exponent: ";
                cin >> Expon;
                if(Base==0&&Expon==0)//base0expon0 no meaning
                {
                    cout << "No such a thing!"<<endl;
                }
                else
                cout << Base << "^" << Expon << ":" << power(Base,Expon) << endl;//call power function
                break;
            case 2://choose function that find term in fibonacci
                int Num;
                cout << "plz enter the term of fibonacci: ";
                cin >> Num;
                cout << Rfibonacci(Num) << endl;//call fibonacci function
                break;
            case 3://choose function NRfibonacci
                cout << "Find the largest of Fibonacci that i can show" << endl;
                NRfibonacci();//call function
                break;
            case 4://exit
                cout << "Thx for using!! ";
                break;
            default://remove mistake input
                cout << "invalid option"<< endl;
                continue;

        }
    }

    system("pause");
    return 0;
}





int power(int base,int exponent)
{
    if(exponent==0)//if exponent=0, return value 1
    {
        return 1;
    }
    else//else base*base  for exponent-1 times
    {
        return base*power(base,exponent-1);
    }
}
int Rfibonacci(int num)
{
    if (num==0||num==1)//fibonacci 0 and 1 was defined
    {
        return num;
    }
    else
    {
        return Rfibonacci(num-1)+Rfibonacci(num-2);//recursive way to calculate fibonacci
    }
}
void NRfibonacci()
{
    int x=0;//initialize
    int array[50]={};

    array[0]=0;//define
    array[1]=1;
    for(int x=2;x<50;x++)//calculate fibonacci numbers
    {
        array[x]=array[x-1]+array[x-2];
    }

    while(array[x]!=-1)
    {
        if(array[x]<0)//if fibonacci number < 0 jump out while
        {
            array[x]=-1;
        }
        else//output results
        {
            cout << x << " term: " << array[x] << endl;
            x++;
        }
    }

}

