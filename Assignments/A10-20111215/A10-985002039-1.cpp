#include<iostream>
#include<cstdlib>

using namespace std;

int power(int a,int b){ //a^b
    if(b==1)
        return a;
    else
        return a*power(a,b-1);
}

int fibonacci(int a){   //fibonacci(n)
    if(a==1)
        return 0;
    else if(a==2||a==3)
        return 1;
    else
        return fibonacci(a-1)+fibonacci(a-2);
}

void nrFibonacci(){     //nrFibonacci()
    int n=0,m=1,r;
    cout<<n<<endl<<m<<endl;
    r=n+m;
    while(r>=0){    //當overflow 數字會變成負數 就跳出
        cout<<r<<"\n";
        n=m;
        m=r;
        r=n+m;
    }
    cout<<endl;
}

int main(){
    int n=0,a,b;
    while(n!=4){
        cout<<"請輸入選項:\n1.power(a,b)\n2.recursive function Fibonacci(n)\n3.non-recursive function nrFibonacci()\n4.exit\n";
        cin>>n;

        switch(n){
        case 1:
            system("cls");
            cout<<"請輸入底數a:";
            cin>>a;
            while(1){
                cout<<"請輸入指數b:";
                cin>>b;
                if(b>=1)
                    break;
                else
                    cout<<"指數必須大於1 請重新輸入指數b\n";
            }
            system("cls");
            cout<<"結果為:"<<power(a,b)<<endl;
            break;
        case 2:
            system("cls");
            while(1){
                cout<<"請輸入要查詢的第n項(n為正整數):";
                cin>>a;
                if(a>=1)
                    break;
                else
                    cout<<"n必為正整數 請重新輸入\n";
            }
            system("cls");
            cout<<"結果為:"<<fibonacci(a)<<endl;
            break;
        case 3:
            system("cls");
            nrFibonacci();
            break;
        case 4:
            break;
        default:
            system("cls");
            cout<<"輸入錯誤 請重新輸入\n";
            break;
        }   //switch
    }   //while
    //system("pause");
    return 0;
}
