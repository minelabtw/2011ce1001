// Fig. 2.1: fig02_01.cpp
// Text-printing program
//made by 100502027
#include <iostream> // allows program to output data to the screen

// function main begins program execution
int main()
{
	//variable declarations
	int number1; // first integer to add
	int number2; // second integer to add
	int sum; // sum of number1 and number 2
	std::cout << "Welcome to NCU CSIE~ \nLet us work hard to try it!\n"; // display message
	
	std::cout << "enter first integer:"; // prompt user for data
	std::cin  >> number1; // read first integer from user into number1

	std::cout << "enter second integer:"; // prompt user for data
	std::cin >> number2; // read second integer fron user into number2

	sum = number1 + number2 ; // add the numbers ; store result in sum

	std::cout << "sum is " << sum << std::endl; // display sum ; end line 
	system("pause");
	return 0; // indicate that program ended successfully
} // end function main