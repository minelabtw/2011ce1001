//Fig.2.1:fig02_01.cpp
//Text-printing program
#include <iostream> //allows program to output data to the screen
//function main begins program execution
int main()
{
	//ariable declarations
	int number1; //first integer to add
	int number2; //second integer to add
	int sum;// sum of number1 and 2

	std::cout << "HELLO!THIS IS MY FIRST PROGRAM.\n";//display message
	std::cout << "Enter first integer: ";//prompt user for data
	std::cin >> number1;//read first interger from user into number1

	std::cout << "Enter second inrerger: ";
	std::cin >> number2;

	sum=number1+number2;

	std::cout << "Sum is" << sum << std::endl;


	system("pause");
	return 0;//indicate that program ended successfully
}//end function main