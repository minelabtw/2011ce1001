#include <iostream>

using namespace std;

int main()
{
    int n1 = 0;
    int n2 = 0;
	int sum = 0;

    cout << "Welcome to wherever it is!\n";
    cout << "Please insert the first integer: ";
    cin >> n1;
    cout << "\nPlease insert the second integer: ";
    cin >> n2;
    cout << "\nThe summary of both integer is:\n";
    sum = n1+n2;
	cout << n1 << " + " << n2 << " = " << sum;
	
    return 0;
}
