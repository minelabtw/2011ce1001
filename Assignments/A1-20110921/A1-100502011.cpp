//Fig. 2.5;fig02_05.cpp
// Addition program that displays the sum of two integers.
#include <iostream> // allows program to perform input and output

//function main begins program execution
int main()
{
	//variable declarations
	int number1; // first integer to add
	int number2; // second integer to add
	int sum; // sum of number1 and number2

	std::cout << " yafish\n";
	std::cout << " Enter first integer: "; 
	std::cin >> number1; 

	std::cout << " Enter second integer: ";
	std::cin >> number2;

	sum = number1 + number2;

	std::cout << "Sum is " << sum << std::endl;
}