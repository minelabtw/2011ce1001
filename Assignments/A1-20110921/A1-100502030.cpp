//Fig. 2.1: fig02_01.cpp
//Text-printing program.
#include  <iostream> // allows program to output data to the screen

// function main begins program execution
int main()
{
	std::cout << " Hi! I am Lawrence \n " ; // display message

	int number1;
	int number2;
	int sum;

	std::cout << "Enter first integer:";
	std::cin >> number1;

	std::cout << "Enter secomd integer";
	std::cin >> number2;

		sum = number1 + number2;

	std::cout << "Sum is " << sum << std::endl;

	
	system("pause");
	return 0; // indicate that program ended successfully
} // end function main