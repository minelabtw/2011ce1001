#include <iostream> //allows program to output data to the screen

// function main begins program execution
int main()
{
	std::cout << "100502515 Hey this is my calculator\n";//message displaying

	int number1;//the first integer to add
    int number2;//the second integer to add
	int sum;//sun of the two numbers

    std::cout << "Enter the first integer:";//let you know where to type the first integer
	std::cin >> number1;//type the first integer here

	std::cout << "Enter the second integer:";//let you know where to type the second integer
	std::cin >> number2;//type the second integer here

	sum = number1 + number2;//add them up and show the result

	std::cout << "It equals:" << sum << std::endl;//show sum and end the line


	system("pause");//keep the message opened
	return 0;//return if the program is right
}//end main function