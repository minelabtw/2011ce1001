#include <iostream>

int main()
{
	int n1;
	int n2;
	int sum;

	std::cout << "Welcome to 100 class A!!\n";

	std::cout << "Enter first integer:";
	std::cin >> n1;
	
	std::cout << "Enter second integer:";
	std::cin >> n2;

	sum = n1 + n2;
	std::cout << "Sum is:\n" << sum << std::endl;
	
	system("pause");
	
}