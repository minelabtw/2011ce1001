#include <iostream>

int main()
{
	int num1, num2, sum;

	std::cout << "Welcome to the plus world!!!" << std::endl << "Enter first integer:";
	std::cin  >> num1;
	std::cout << "Enter second integer:";
	std::cin  >> num2;
	sum = num1 + num2;
	std::cout << "Sum is " << sum << std::endl;

	system("pause");
	return 0;
}