/*
 *A1-100502514.cpp
 *NCU CSIE Freshman Class A
 *1st C++ Assignment
 *Student name: 江衍霖
 *Student ID: 100502514
 *Date: 2011/09/21
 *Application: HelloWorld + Number Addition Extended Version
 *打字機特效為本人延伸
 */
#include <iostream>
using namespace std;
void main01(){
	//顯示訊息
	cout<<"100502514\n";
	cout<<"Please input how many seconds the TYPEWRITER TEXT shows:\n";
	cout<<"(At most 40 seconds)\n";
	unsigned int dura;
	//總時間最多40秒
	if(cin>>dura&&dura<=40){
		system("cls");
		//儲存網址字串，打字機特效程式會用到
		char address[]="100502514\nhttp://flashteens.horizon-host.com/\n";
		//休息0.5秒
		_sleep(500);
		//每打一字間隔相同秒數(each_typing的結果)，使總時間為dura秒
		int each_typing=dura*1000/strlen(address);
		for(int i=0;address[i]!='\0';i++){
			_sleep(each_typing);
			cout<<address[i];
		}
		//停頓1秒後倒數3秒
		_sleep(1000);
		for(int i=3;i>0;i--){
			cout<<"\r本程式將於 "<<i<<" 秒後退出";
			_sleep(1000);
		}
	}
}
void main02(){
	//顯示訊息
	cout<<"100502514 FlashTeens Network\n";
	//Define 2 integers and input them
	int i1,i2;

	cout<<"Please input Integer 1:\n";
	cin>>i1;
	if(cin.eof())return;

	cout<<"Please input Integer 2:\n";
	cin>>i2;
	if(cin.eof())return;

	cout<<"Integer 1 + Interger 2 = "<< (i1+i2);
	cout<<endl;
	system("pause");
}
int main(){
	system("title A1-100502514");
Begin:
	system("cls");
	cout<<"2011/09/21 計算機實習 程式作業\n";
	cout<<"100502514 江衍霖\n";
	cout<<"請輸入欲瀏覽的作業：\n";
	cout<<" 1: Hello World打字機版\n";
	cout<<" 2: 兩數相加\n";
	cout<<" 其他數字: 結束程式\n";
	int select_num;
	while(cin>>select_num){
		bool exiting=false;
		switch(select_num){
		case 1:
			system("cls");
			main01();
			if(cin.eof())cin.clear();
			break;
		case 2:
			system("cls");
			main02();
			if(cin.eof())cin.clear();
			break;
		default:
			exiting=true;
		}
		if(exiting)break;
		goto Begin;
	}
	return 0;
}